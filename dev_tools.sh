
# Author Vincent Bourgmayer
# 2023 November 22nd 

function printHelp() {
    echo "Utilisation: ./eDrive_dev_tools.sh [opt] [(optionnal:) value]
    Note: This script strongly rely on ADB, so it won't work if ADB is not installed

    Options list:
    -a : add Account into the device
    -c : clear eDrive's data
    -C : clear adb logcat
    -d : dump database (generate a database dumb on the device. The dump is reachable by the user)
    -f : Trigger fullScan work (scan cloud & device then try to sync changed files)
    -h : print this help
    -L : (required value: true|false) : true: enable/ false: disable full log
    -l : (option: --clear) : --clear: clear logcat before. Otherwise just display logcat. Default: false
    -n : Start nextcloud docker instance
    -o : (required value: true|false) true: enable File Observer / false: disable File Observer
    -p : Pull database directory from device into current computer folder
    -s : test sync : Create 10 empty files in 'Document' folder and try to upload them
    ";
}


function registerAccount() {
    echo "calling script to add account into device

    /"'!'"\\ Device need to be connecter to WI_FI

    /"'!'"\\ In order to let it work,
    check that credentials & server URL
    are set in registerAccount2.sh

    /i\\ please also consider, that the script will need to be updated if UI
    changes
    ";

    
    ./registerAccount.sh
}

function setFullLog() {
    if [ -z "$1" ]; then
        echo "The parameter -L require a value (true|false)"
    elif [ "$1" != "true" ] && [ "$1" != "false" ]; then
        echo "Invalid parameter: $1. You need to provide: (true|false)"
    else
        echo "Sending broacast with $1 as parameter"
        adb shell am broadcast -a foundation.e.drive.action.FULL_LOG_ON_PROD --receiver-include-background --ez full_log_enable "$1"
    fi
}

function displayLogcat() {
    adb logcat --pid=$(adb shell pidof -s foundation.e.drive)
}

function clearLogcat() {
    adb logcat --clear
}

function pullDatabase() {
    adb pull /data/data/foundation.e.drive/databases
}

function clearAppData() {
    echo "clear eDrive's data";
    adb shell pm clear foundation.e.drive
}

function dumpDatabase() {
    echo "Dumping database"
    adb shell am broadcast -a foundation.e.drive.action.DUMP_DATABASE --receiver-include-background
}

function forceFullScan() {
    adb shell am broadcast -a foundation.e.drive.action.FORCE_SCAN --receiver-include-background
}

function testSync() {
    adb shell am broadcast -a foundation.e.drive.action.TEST_SYNC --receiver-include-background
}

function startNCDocker() {
    docker run -d -p 8080:80 -e NEXTCLOUD_TRUSTED_DOMAINS="<your computer IP>:8080" -e SQLITE_DATABASE=nc -e NEXTCLOUD_ADMIN_USER=admin -e NEXTCLOUD_ADMIN_PASSWORD=admin nextcloud:26
}

function enableFileObserver() {
    if [ -z "$1" ]; then
        echo "The parameter -L require a value (true|false)"
    elif [ "$1" != "true" ] && [ "$1" != "false" ]; then
        echo "Invalid parameter: $1. You need to provide: (true|false)"
    else
        echo "Sending broacast with $1 as parameter"
        adb shell am broadcast -a foundation.e.drive.action.ENABLE_FILE_OBSERVER --receiver-include-background --ez file_observer_enable "$1"
    fi
}


while getopts acCdfhL:lnops flag
do
    case "${flag}" in
        a) registerAccount;;
        c) clearAppData;;
        C) clearLogcat;;
        d) dumpDatabase;;
        f) forceFullScan;;
        h) printHelp;;
        l) displayLogcat;;
        L) setFullLog ${OPTARG};;
        n) startNCDocker;;
        o) enableFileObserver ${OPTARG};;
        p) pullDatabase;;
        s) testSync;;
    esac
done

echo "|----FINISHED----|"
exit 0

