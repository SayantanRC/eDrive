/*
 * Copyright © MURENA SAS 2022-2023.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */
package foundation.e.drive.periodicScan.contentScanner;

import static org.junit.Assert.assertEquals;

import static foundation.e.drive.models.SyncedFileStateKt.SCAN_EVERYWHERE;

import android.content.Context;
import android.os.Build;

import androidx.test.core.app.ApplicationProvider;

import com.owncloud.android.lib.resources.files.model.RemoteFile;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowLog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import foundation.e.drive.TestUtils;
import foundation.e.drive.database.DbHelper;
import foundation.e.drive.models.SyncRequest;
import foundation.e.drive.models.SyncedFileState;
import foundation.e.drive.models.SyncedFolder;
import foundation.e.drive.periodicScan.contentScanner.RemoteContentScanner;

/**
 * @author vincent Bourgmayer
 */
@RunWith(RobolectricTestRunner.class)
@Config(sdk = Build.VERSION_CODES.O, manifest = Config.NONE)
public class RemoteContentScannerTest {

    private final Context context;
    private RemoteContentScanner scannerUnderTest;

    public RemoteContentScannerTest() {
        context = ApplicationProvider.getApplicationContext();
        ShadowLog.stream = System.out;
    }

    @Before
    public void setUp() {
        prepareDB();
        final List<SyncedFolder> directoryList = new ArrayList<>();
        directoryList.add(new SyncedFolder("Photos", "local/path/", "remote/path/", true, true, true, true));
        scannerUnderTest = new RemoteContentScanner(context, directoryList);
    }

    private void prepareDB() {
        DbHelper.insertSyncedFolder(createSyncedFolder("small"), context);
        DbHelper.insertSyncedFolder(createSyncedFolder("medium"), context);
        DbHelper.insertSyncedFolder(createSyncedFolder("large"), context);

        //assertion for debug purpose
        assertEquals("There isn't three folder in DB as expected", 3, DbHelper.getSyncedFolderList(context, true).size());
    }

    /**
     * Create the syncedFolder instance
     * @param name the name of the folder
     * @return SyncedFolder instance
     */
    private SyncedFolder createSyncedFolder(String name) {
        return new SyncedFolder(name,
                TestUtils.TEST_LOCAL_ROOT_FOLDER_PATH+name+"/",
                TestUtils.TEST_REMOTE_ROOT_FOLDER_PATH+name+"/",
                true, true, true, true);
    }

    @Test
    public void scanContent_withoutRemoteContent_returnListWithLocalDeleteRequest() {
        final List<RemoteFile> cloudContent = new ArrayList<>();

        final List<SyncedFileState> dbContent = new ArrayList<>();
        dbContent.add(new SyncedFileState(5, "toto", "local/path/toto", "remote/path/toto", "5555", 22222, 2, true, SCAN_EVERYWHERE));
        dbContent.add(new SyncedFileState(3, "titi", "local/path/titi", "remote/path/titi", "5556", 22322, 2, true, SCAN_EVERYWHERE));
        dbContent.add(new SyncedFileState(2, "tata", "local/path/tata", "remote/path/tata", "5557", 22232, 2, true, SCAN_EVERYWHERE));

        final HashMap<Integer, SyncRequest> scanResult = scannerUnderTest.scanContent(cloudContent, dbContent);

        Assert.assertEquals("scanResult's size doesn't match the expected result", 3, scanResult.size());

        for (SyncRequest request : scanResult.values()) {
            Assert.assertEquals("SyncRequest's type should be LOCAL_DELETE but is "+request.getOperationType(), SyncRequest.Type.DISABLE_SYNCING, request.getOperationType());
        }
    }

    private List<RemoteFile> generateListOfNewRemoteFile() {
        final ArrayList<RemoteFile> result = new ArrayList<>();

        final RemoteFile newFile1 = Mockito.mock(RemoteFile.class);
        Mockito.when(newFile1.getRemotePath()).thenReturn("remote/path/tutu");
        Mockito.when(newFile1.getEtag()).thenReturn("33323");


        final RemoteFile newFile2 = Mockito.mock(RemoteFile.class);
        Mockito.when(newFile2.getRemotePath()).thenReturn("remote/path/tete");
        Mockito.when(newFile2.getEtag()).thenReturn("33423");


        result.add(newFile1);
        result.add(newFile2);

        return result;
    }

    @Test
    public void scanContent_withUpdatedRemoteContent_returnListWithDownloadRequest() {
        final List<RemoteFile> cloudContent = new ArrayList<>();

        final RemoteFile updatedFile1 = Mockito.mock(RemoteFile.class);
        Mockito.when(updatedFile1.getRemotePath()).thenReturn("remote/path/toto");
        Mockito.when(updatedFile1.getEtag()).thenReturn("33423");
        Mockito.when(updatedFile1.getSize()).thenReturn(12L);

        final RemoteFile updatedFile2 = Mockito.mock(RemoteFile.class);
        Mockito.when(updatedFile2.getRemotePath()).thenReturn("remote/path/titi");
        Mockito.when(updatedFile2.getEtag()).thenReturn("34523");
        Mockito.when(updatedFile2.getSize()).thenReturn(14L);

        final RemoteFile upTtoDateFile = Mockito.mock(RemoteFile.class);
        Mockito.when(upTtoDateFile.getRemotePath()).thenReturn("remote/path/tata");
        Mockito.when(upTtoDateFile.getEtag()).thenReturn("5557");

        cloudContent.add(upTtoDateFile);
        cloudContent.add(updatedFile1);
        cloudContent.add(updatedFile2);

        Assert.assertEquals("Expected cloudContent size: 3 but got : " + cloudContent.size(), 3, cloudContent.size());


        final List<SyncedFileState> dbContent = new ArrayList<>();
        dbContent.add(new SyncedFileState(5, "toto", "local/path/toto", "remote/path/toto", "5555", 22222, 2, true, SCAN_EVERYWHERE));
        dbContent.add(new SyncedFileState(3, "titi", "local/path/titi", "remote/path/titi", "5556", 22322, 2, true, SCAN_EVERYWHERE));
        dbContent.add(new SyncedFileState(2, "tata", "local/path/tata", "remote/path/tata", "5557", 22232, 2, true, SCAN_EVERYWHERE));

        final HashMap<Integer, SyncRequest> scanResult = scannerUnderTest.scanContent(cloudContent, dbContent);
        Assert.assertEquals("scanResult's size doesn't match the expected result", 2, scanResult.size());

        for (SyncRequest request : scanResult.values()) {
            Assert.assertEquals("SyncRequest's type should be DOWNLOAD but is "+request.getOperationType(), SyncRequest.Type.DOWNLOAD, request.getOperationType());
        }
    }

    @Test
    public void scanContent_withNewRemoteContent_returnListWithDownloadRequest() {
        final List<RemoteFile> cloudContent = generateListOfNewRemoteFile();

        final RemoteFile updatedFile1 = Mockito.mock(RemoteFile.class);
        Mockito.when(updatedFile1.getRemotePath()).thenReturn("remote/path/toto");
        Mockito.when(updatedFile1.getEtag()).thenReturn("5555");

        final RemoteFile updatedFile2 = Mockito.mock(RemoteFile.class);
        Mockito.when(updatedFile2.getRemotePath()).thenReturn("remote/path/titi");
        Mockito.when(updatedFile2.getEtag()).thenReturn("5556");

        final RemoteFile upToDateFile = Mockito.mock(RemoteFile.class);
        Mockito.when(upToDateFile.getRemotePath()).thenReturn("remote/path/tata");
        Mockito.when(upToDateFile.getEtag()).thenReturn("5557");

        cloudContent.add(upToDateFile);
        cloudContent.add(updatedFile1);
        cloudContent.add(updatedFile2);

        Assert.assertEquals("Expected cloudContent size: 5 but got : " + cloudContent.size(), 5, cloudContent.size());


        final List<SyncedFileState> dbContent = new ArrayList<>();
        dbContent.add(new SyncedFileState(5, "toto", "local/path/toto", "remote/path/toto", "5555", 22222, 2, true, SCAN_EVERYWHERE));
        dbContent.add(new SyncedFileState(3, "titi", "local/path/titi", "remote/path/titi", "5556", 22322, 2, true, SCAN_EVERYWHERE));
        dbContent.add(new SyncedFileState(2, "tata", "local/path/tata", "remote/path/tata", "5557", 22232, 2, true, SCAN_EVERYWHERE));

        final HashMap<Integer, SyncRequest> scanResult = scannerUnderTest.scanContent(cloudContent, dbContent);
        Assert.assertEquals("scanResult's size doesn't match the expected result", 2, scanResult.size());

        for (SyncRequest request : scanResult.values()) {
            Assert.assertEquals("SyncRequest's type should be DOWNLOAD but is "+request.getOperationType(), SyncRequest.Type.DOWNLOAD, request.getOperationType());
        }
    }

    @Test
    public void scanContent_emptyLists_returnEmptyList() {
        final List<RemoteFile> cloudContent = new ArrayList<>();
        final List<SyncedFileState> dbContent = new ArrayList<>();

        final HashMap<Integer, SyncRequest> scanResult = scannerUnderTest.scanContent(cloudContent, dbContent);
        Assert.assertEquals("scanResult's size doesn't match the expected result", 0, scanResult.size());

        for (SyncRequest request : scanResult.values()) {
            System.out.println(request.getOperationType() + "for " +request.getSyncedFileState().getRemotePath());
        }
    }
}