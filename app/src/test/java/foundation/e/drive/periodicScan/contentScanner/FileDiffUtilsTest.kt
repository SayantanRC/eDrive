/*
 * Copyright © MURENA SAS 2023.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */
package foundation.e.drive.periodicScan.contentScanner

import com.owncloud.android.lib.resources.files.model.RemoteFile
import foundation.e.drive.models.SyncedFileState
import foundation.e.drive.periodicScan.contentScanner.FileDiffUtils
import foundation.e.drive.utils.AppConstants
import org.junit.Assert
import org.junit.Test
import org.mockito.Mockito
import java.io.File

/**
 * Unit for test FileDiffUtils's method
 *
 * note: when test method name mention 'DB' it refers to SyncedFileState, because those object
 * represent data fetched from DB in the scope of the class under test.
 *
 * 'mTime' means modification time, it is a shortcut for 'lastModified'
 *
 * @author Vincent Bourgmayer
 */
internal class FileDiffUtilsTest {
    private val currentTimestampInSecond = System.currentTimeMillis()/1000

    /* isRemoteAndLocalSizeEqual() */
    @Test
    fun `isRemoteAndLocalSizeEqual() return false when local file's is size smaller`() {
        val remoteSize: Long = 12346789
        val localSize: Long = 1234

        Assert.assertFalse("isRemoteAndLocalSizeEqual($remoteSize, $localSize) returned true instead of false",
            FileDiffUtils.isRemoteAndLocalSizeEqual(remoteSize, localSize))
    }

    @Test
    fun `isRemoteAndLocalSizeEqual() return false when local file's size is bigger`() {
        val remoteSize: Long = 1234
        val localSize: Long = 12346789

        Assert.assertFalse("isRemoteAndLocalSizeEqual($remoteSize, $localSize) returned true instead of false",
            FileDiffUtils.isRemoteAndLocalSizeEqual(remoteSize, localSize))
    }

    @Test
    fun `isRemoteAndLocalSizeEqual() return true when remote & local file size are equal`() {
        val remoteSize: Long = 12346789
        val localSize: Long = 12346789

        Assert.assertTrue("isRemoteAndLocalSizeEqual($remoteSize, $localSize) returned false instead of true",
            FileDiffUtils.isRemoteAndLocalSizeEqual(remoteSize, localSize))
    }

    /* hasAlreadyBeenDownloaded() */
    @Test
    fun `hasAlreadyBeenDownloaded() return true when last modified is bigger than 0`() {
        val fileState = Mockito.mock(SyncedFileState::class.java)
        Mockito.`when`(fileState.lastModified).thenReturn(1)

        Assert.assertTrue("hasAlreadyBeenDownloaded() returned false instead of true",
            FileDiffUtils.hasAlreadyBeenDownloaded(fileState))
    }

    @Test
    fun `hasAlreadyBeenDownloaded() return false when last modified equal to 0`() {
        val fileState = Mockito.mock(SyncedFileState::class.java)
        Mockito.`when`(fileState.lastModified).thenReturn(0)

        Assert.assertFalse("hasAlreadyBeenDownloaded() returned true instead of false",
            FileDiffUtils.hasAlreadyBeenDownloaded(fileState))
    }

    /* hasEtagChanged() */
    @Test
    fun `hasEtagChanged() return false when DB doesn't have eTag return false`() {
        val remoteFile = Mockito.mock(RemoteFile::class.java)

        val fileState = Mockito.mock(SyncedFileState::class.java)
        Mockito.`when`(fileState.isLastEtagStored()).thenReturn(false)

        Assert.assertFalse("hasEtagChanged() returned true instead of false",
            FileDiffUtils.hasEtagChanged(remoteFile, fileState))
    }

    @Test
    fun `hasEtagChanged() return false when eTag from remoteFile & DB are the same`() {
        val remoteFile = Mockito.mock(RemoteFile::class.java)
        Mockito.`when`(remoteFile.etag).thenReturn("123456789")

        val fileState = Mockito.mock(SyncedFileState::class.java)
        Mockito.`when`(fileState.lastEtag).thenReturn("123456789")
        Mockito.`when`(fileState.isLastEtagStored()).thenReturn(true)

        Assert.assertFalse("hasEtagChanged() returned true instead of false",
            FileDiffUtils.hasEtagChanged(remoteFile, fileState))
    }

    @Test
    fun `hasEtagChanged() return true when Remote eTag is empty while DB is not`() {
        val remoteFile = Mockito.mock(RemoteFile::class.java)
        Mockito.`when`(remoteFile.etag).thenReturn("")

        val fileState = Mockito.mock(SyncedFileState::class.java)
        Mockito.`when`(fileState.lastEtag).thenReturn("123456789")
        Mockito.`when`(fileState.isLastEtagStored()).thenReturn(true)

        Assert.assertTrue("hasEtagChanged() returned false instead of true",
            FileDiffUtils.hasEtagChanged(remoteFile, fileState))
    }

    @Test
    fun `hasEtagChanged() return true when eTag from DB & Remote are different`() {
        val remoteFile = Mockito.mock(RemoteFile::class.java)
        //Mockito.`when`(remoteFile.etag).thenReturn("123456789")
        remoteFile.etag = "123456789"
        val fileState = Mockito.mock(SyncedFileState::class.java)
        Mockito.`when`(fileState.lastEtag).thenReturn("987654321")
        Mockito.`when`(fileState.isLastEtagStored()).thenReturn(true)

        Assert.assertTrue("hasEtagChanged() returned false instead of true",
            FileDiffUtils.hasEtagChanged(remoteFile, fileState))
    }


    /* getActionForFileDiff  for localFile */

    @Test
    fun `getActionForFileDiff(File) return Upload when local file mTime is smaller & no eTag in DB`() {
        val mockedLastModified: Long = 1
        val mockedFile = Mockito.spy(File("/local/path.txt"))
        Mockito.`when`(mockedFile.lastModified()).thenReturn(mockedLastModified)

        val mockedLastModifiedFromDB: Long = 10
        val fileState = Mockito.mock(SyncedFileState::class.java)
        Mockito.`when`(fileState.isLastEtagStored()).thenReturn(false)
        Mockito.`when`(fileState.lastModified).thenReturn(mockedLastModifiedFromDB)

        val resultUnderTest = FileDiffUtils.getActionForFileDiff(mockedFile, fileState)
        Assert.assertEquals("getActionForFileDiff() returned $resultUnderTest instead of Action.Upload", FileDiffUtils.Action.Upload, resultUnderTest)
    }

    @Test
    fun `getActionForFileDiff(File) return Skip when local file mTime is smaller & eTag is in DB`() {
        val mockedLastModified: Long = 1
        val mockedFile = Mockito.spy(File("/local/path.txt"))
        Mockito.`when`(mockedFile.lastModified()).thenReturn(mockedLastModified)

        val mockedLastModifiedFromDB: Long = 10
        val fileState = Mockito.mock(SyncedFileState::class.java)
        Mockito.`when`(fileState.isLastEtagStored()).thenReturn(true)
        Mockito.`when`(fileState.lastModified).thenReturn(mockedLastModifiedFromDB)

        val resultUnderTest = FileDiffUtils.getActionForFileDiff(mockedFile, fileState)
        Assert.assertEquals("getActionForFileDiff() returned $resultUnderTest instead of Action.Skip", FileDiffUtils.Action.Skip, resultUnderTest)
    }

    @Test
    fun `getActionForFileDiff(File) return Upload when local file mTime is equal to DB & eTag not in DB`() {
        val mockedLastModified: Long = 10
        val mockedFile = Mockito.spy(File("/local/path.txt"))
        Mockito.`when`(mockedFile.lastModified()).thenReturn(mockedLastModified)

        val mockedLastModifiedFromDB: Long = 10
        val fileState = Mockito.mock(SyncedFileState::class.java)
        Mockito.`when`(fileState.isLastEtagStored()).thenReturn(false)
        Mockito.`when`(fileState.lastModified).thenReturn(mockedLastModifiedFromDB)

        val resultUnderTest = FileDiffUtils.getActionForFileDiff(mockedFile, fileState)
        Assert.assertEquals("getActionForFileDiff() returned $resultUnderTest instead of Action.Upload", FileDiffUtils.Action.Upload, resultUnderTest)
    }

    @Test
    fun `getActionForFileDiff(File) return Skip when local file mTime is equal to DB & eTag is in DB`() {
        val mockedLastModified: Long = 10
        val mockedFile = Mockito.spy(File("/local/path.txt"))
        Mockito.`when`(mockedFile.lastModified()).thenReturn(mockedLastModified)

        val mockedLastModifiedFromDB: Long = 10
        val fileState = Mockito.mock(SyncedFileState::class.java)
        Mockito.`when`(fileState.isLastEtagStored()).thenReturn(true)
        Mockito.`when`(fileState.lastModified).thenReturn(mockedLastModifiedFromDB)

        val resultUnderTest = FileDiffUtils.getActionForFileDiff(mockedFile, fileState)
        Assert.assertEquals("getActionForFileDiff() returned $resultUnderTest instead of Action.Skip", FileDiffUtils.Action.Skip, resultUnderTest)
    }

    @Test
    fun `getActionForFileDiff(File) return Upload when local file mTime is bigger & eTag not in DB`() {
        val mockedLastModified: Long = 10
        val mockedFile = Mockito.spy(File("/local/path.txt"))
        Mockito.`when`(mockedFile.lastModified()).thenReturn(mockedLastModified)

        val mockedLastModifiedFromDB: Long = 1
        val fileState = Mockito.mock(SyncedFileState::class.java)
        Mockito.`when`(fileState.isLastEtagStored()).thenReturn(false)
        Mockito.`when`(fileState.lastModified).thenReturn(mockedLastModifiedFromDB)

        val resultUnderTest = FileDiffUtils.getActionForFileDiff(mockedFile, fileState)
        Assert.assertEquals("getActionForFileDiff() returned $resultUnderTest instead of Action.Upload", FileDiffUtils.Action.Upload, resultUnderTest)
    }

    @Test
    fun `getActionForFileDiff(File) return Upload when local file mTime is bigger & eTag is in DB`() {
        val mockedLastModified: Long = 10
        val mockedFile = Mockito.spy(File("/local/path.txt"))
        Mockito.`when`(mockedFile.lastModified()).thenReturn(mockedLastModified)

        val mockedLastModifiedFromDB: Long = 1
        val fileState = Mockito.mock(SyncedFileState::class.java)
        Mockito.`when`(fileState.isLastEtagStored()).thenReturn(true)
        Mockito.`when`(fileState.lastModified).thenReturn(mockedLastModifiedFromDB)

        val resultUnderTest = FileDiffUtils.getActionForFileDiff(mockedFile, fileState)
        Assert.assertEquals("getActionForFileDiff() returned $resultUnderTest instead of Action.Upload", FileDiffUtils.Action.Upload, resultUnderTest)
    }

    /* getActionForFileDiff  for remoteFile */
     @Test
    fun `getActionForFileDiff(RemoteFile) return Skip when file is up to date`() {
        val mockedRemoteFile = Mockito.mock(RemoteFile::class.java)
         Mockito.`when`(mockedRemoteFile.modifiedTimestamp).thenReturn(currentTimestampInSecond)
        Mockito.`when`(mockedRemoteFile.etag).thenReturn("123456")
        val mockedFileState = Mockito.mock(SyncedFileState::class.java)
        Mockito.`when`(mockedFileState.lastModified).thenReturn(1L)
        Mockito.`when`(mockedFileState.lastEtag).thenReturn("123456")
        Mockito.`when`(mockedFileState.isLastEtagStored()).thenReturn(true)

        val resultUnderTest = FileDiffUtils.getActionForFileDiff(mockedRemoteFile, mockedFileState)
        Assert.assertEquals("getActionForFileDiff(RemoteFile) returned $resultUnderTest instead of Action.Skip", FileDiffUtils.Action.Skip, resultUnderTest)
    }

    @Test
    fun `getActionForFileDiff(RemoteFile) return UpdateDB when file is up to date but not DB`() {
        val mockedRemoteFile = Mockito.mock(RemoteFile::class.java)
        Mockito.`when`(mockedRemoteFile.modifiedTimestamp).thenReturn(currentTimestampInSecond)
        Mockito.`when`(mockedRemoteFile.etag).thenReturn("123456")
        Mockito.`when`(mockedRemoteFile.size).thenReturn(0) //local file doesn't exist for test, so its size will be 0
        val mockedFileState = Mockito.mock(SyncedFileState::class.java)
        Mockito.`when`(mockedFileState.lastModified).thenReturn(1L)
        Mockito.`when`(mockedFileState.localPath).thenReturn("/local/file.txt")
        Mockito.`when`(mockedFileState.lastEtag).thenReturn("12")
        Mockito.`when`(mockedFileState.isLastEtagStored()).thenReturn(true)

        val resultUnderTest = FileDiffUtils.getActionForFileDiff(mockedRemoteFile, mockedFileState)
        Assert.assertEquals("getActionForFileDiff(RemoteFile) returned $resultUnderTest instead of Action.UpdateDB", FileDiffUtils.Action.UpdateDB, resultUnderTest)
    }


    @Test
    fun `getActionForFileDiff(RemoteFile) return Download when file is not up to date`() {
        val mockedRemoteFile = Mockito.mock(RemoteFile::class.java)
        Mockito.`when`(mockedRemoteFile.modifiedTimestamp).thenReturn(currentTimestampInSecond)
        Mockito.`when`(mockedRemoteFile.etag).thenReturn("123456")
        Mockito.`when`(mockedRemoteFile.size).thenReturn(1) //local file doesn't exist for test, so its size will be 0
        val mockedFileState = Mockito.mock(SyncedFileState::class.java)
        Mockito.`when`(mockedFileState.lastModified).thenReturn(1L)
        Mockito.`when`(mockedFileState.localPath).thenReturn("/local/file.txt")
        Mockito.`when`(mockedFileState.lastEtag).thenReturn("12")
        Mockito.`when`(mockedFileState.isLastEtagStored()).thenReturn(true)

        val resultUnderTest = FileDiffUtils.getActionForFileDiff(mockedRemoteFile, mockedFileState)
        Assert.assertEquals("getActionForFileDiff(RemoteFile) returned $resultUnderTest instead of Action.Download", FileDiffUtils.Action.Download, resultUnderTest)
    }

    @Test
    fun `getActionForFileDiff(RemoteFile) return Upload when remote modifiedTimestamp is corrupted and eTag unchanged`() {
        val mockedRemoteFile = Mockito.mock(RemoteFile::class.java)
        Mockito.`when`(mockedRemoteFile.modifiedTimestamp).thenReturn(4294967295000L)
        Mockito.`when`(mockedRemoteFile.etag).thenReturn("123456")
        val mockedFileState = Mockito.mock(SyncedFileState::class.java)
        Mockito.`when`(mockedFileState.lastEtag).thenReturn("123456")
        val resultUnderTest = FileDiffUtils.getActionForFileDiff(mockedRemoteFile, mockedFileState)
        Assert.assertEquals("getActionForFileDiff(RemoteFile) returned $resultUnderTest instead of Action.Upload", FileDiffUtils.Action.Upload, resultUnderTest)
    }

    @Test
    fun `getActionForFileDiff(RemoteFile) don't return Upload when remote modifiedTimestamp is corrupted but eTag changed`() {
        val mockedRemoteFile = Mockito.mock(RemoteFile::class.java)
        Mockito.`when`(mockedRemoteFile.modifiedTimestamp).thenReturn(4294967295000L)
        Mockito.`when`(mockedRemoteFile.etag).thenReturn("654321")
        val mockedFileState = Mockito.mock(SyncedFileState::class.java)
        Mockito.`when`(mockedFileState.lastEtag).thenReturn("123456789")
        Mockito.`when`(mockedFileState.isLastEtagStored()).thenReturn(true)
        Mockito.`when`(mockedFileState.localPath).thenReturn("/local/file.txt")
        val resultUnderTest = FileDiffUtils.getActionForFileDiff(mockedRemoteFile, mockedFileState)
        Assert.assertNotEquals("getActionForFileDiff(RemoteFile) returned $resultUnderTest while it should not", FileDiffUtils.Action.Upload, resultUnderTest)
    }

    /* isCorruptedTimestamp  for localFile */
    @Test
    fun `isCorruptedTimestamp() return true with timestamp equal to max of Int32 `() {
        val corruptedTimestamp = AppConstants.CORRUPTED_TIMESTAMP_IN_MILLISECOND
        val resultUnderTest = FileDiffUtils.isCorruptedTimestamp(corruptedTimestamp)
        Assert.assertTrue("isCorruptedTimestamp(4294967295000L) returned $resultUnderTest instead of true", resultUnderTest)
    }

    @Test
    fun `isCorruptedTimestamp() return true with timestamp bigger than max of Int32 `() {
        val corruptedTimestamp = AppConstants.CORRUPTED_TIMESTAMP_IN_MILLISECOND + 1
        val resultUnderTest = FileDiffUtils.isCorruptedTimestamp(corruptedTimestamp)
        Assert.assertTrue("isCorruptedTimestamp(4294967295000L) returned $resultUnderTest instead of true", resultUnderTest)
    }

    @Test
    fun `isCorruptedTimestamp() return false with timestamp smaller than max of Int32 `() {
        val corruptedTimestamp = AppConstants.CORRUPTED_TIMESTAMP_IN_MILLISECOND - 1
        val resultUnderTest = FileDiffUtils.isCorruptedTimestamp(corruptedTimestamp)
        Assert.assertFalse("isCorruptedTimestamp(4294967295000L) returned $resultUnderTest instead of false", resultUnderTest)
    }
}