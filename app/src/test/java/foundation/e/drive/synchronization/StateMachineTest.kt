/*
 * Copyright © MURENA SAS 2023.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */
package foundation.e.drive.synchronization

import foundation.e.drive.synchronization.SyncState.IDLE
import foundation.e.drive.synchronization.SyncState.LISTENING_FILES
import foundation.e.drive.synchronization.SyncState.PERIODIC_SCAN
import foundation.e.drive.synchronization.SyncState.SYNCHRONIZING
import org.junit.Assert
import org.junit.Test

class StateMachineTest {

    @Test
    fun `Changing State from PERIODIC_SCAN to LISTENING_FILES should return true`() {
        StateMachine.currentState = PERIODIC_SCAN

        val result = StateMachine.changeState(LISTENING_FILES)
        Assert.assertTrue("Changing state from PERIODIC_SCAN to LISTENING_FILES returned false", result)

        val currentState = StateMachine.currentState
        Assert.assertEquals("New current state should be LISTENING_FILES but is $currentState", LISTENING_FILES, currentState)
    }

    @Test
    fun `Changing State from PERIODIC_SCAN to PERIODIC_SCAN should return false`() {
        StateMachine.currentState = PERIODIC_SCAN

        val result = StateMachine.changeState(PERIODIC_SCAN)
        Assert.assertFalse("Changing state from PERIODIC_SCAN to PERIODIC_SCAN returned true", result)

        val currentState = StateMachine.currentState
        Assert.assertEquals("Current state should remain PERIODIC_SCAN but is $currentState", PERIODIC_SCAN, currentState)
    }

    @Test
    fun `Changing State from PERIODIC_SCAN to SYNCHRONIZING should return true`() {
        StateMachine.currentState = PERIODIC_SCAN

        val result = StateMachine.changeState(SYNCHRONIZING)
        Assert.assertTrue("Changing state from PERIODIC_SCAN to SYNCHRONIZING returned false", result)

        val currentState = StateMachine.currentState
        Assert.assertEquals("New current state should be SYNCHRONIZING but is $currentState", SYNCHRONIZING, currentState)
    }

    @Test
    fun `Changing State from PERIODIC_SCAN to IDLE should return true`() {
        StateMachine.currentState = PERIODIC_SCAN

        val result = StateMachine.changeState(IDLE)
        Assert.assertTrue("Changing state from PERIODIC_SCAN to IDLE returned false", result)

        val currentState = StateMachine.currentState
        Assert.assertEquals("Current state should be IDLE but is $currentState", IDLE, currentState)
    }

    @Test
    fun `Changing State from LISTENING_FILES to PERIODIC_SCAN should return true`() {
        StateMachine.currentState = LISTENING_FILES

        val result = StateMachine.changeState(PERIODIC_SCAN)
        Assert.assertTrue("Changing state from LISTENING_FILES to PERIODIC_SCAN returned false", result)

        val currentState = StateMachine.currentState
        Assert.assertEquals("New current state should be PERIODIC_SCAN but is $currentState", PERIODIC_SCAN, currentState)
    }

    @Test
    fun `Changing State from LISTENING_FILE to SYNCHRONIZING should return true`() {
        StateMachine.currentState = LISTENING_FILES

        val result = StateMachine.changeState(SYNCHRONIZING)
        Assert.assertTrue("Changing state from LISTENING_FILES to SYNCHRONIZING returned false", result)

        val currentState = StateMachine.currentState
        Assert.assertEquals("New current state should be SYNCHRONIZING but is $currentState", SYNCHRONIZING, currentState)
    }

    @Test
    fun `Changing State from LISTENING_FILE to IDLE should return true`() {
        StateMachine.currentState = LISTENING_FILES

        val result = StateMachine.changeState(IDLE)
        Assert.assertTrue("Changing state from LISTENING_FILES to IDLE returned false", result)

        val currentState = StateMachine.currentState
        Assert.assertEquals("Current state should be IDLE but is $currentState", IDLE, currentState)
    }

    @Test
    fun `Changing State from LISTENING_FILES to LISTENING_FILES should return true`() {
        StateMachine.currentState = LISTENING_FILES

        val result = StateMachine.changeState(LISTENING_FILES)
        Assert.assertTrue("Changing state from LISTENING_FILES to LISTENING_FILES returned false", result)

        val currentState = StateMachine.currentState
        Assert.assertEquals("New current state should be LISTENING_FILES but is $currentState", LISTENING_FILES, currentState)
    }

    @Test
    fun `Changing State from SYNCHRONIZING to LISTENING_FILE should return true`() {
        StateMachine.currentState = SYNCHRONIZING

        val result = StateMachine.changeState(LISTENING_FILES)
        Assert.assertTrue("Changing state from SYNCHRONIZING to LISTENING_FILES returned false", result)

        val currentState = StateMachine.currentState
        Assert.assertEquals("New current state should be LISTENING_FILES but is $currentState", LISTENING_FILES, currentState)
    }

    @Test
    fun `Changing State from SYNCHRONIZING to PERIODIC_SCAN should return false`() {
        StateMachine.currentState = SYNCHRONIZING

        val result = StateMachine.changeState(PERIODIC_SCAN)
        Assert.assertFalse("Changing state from SYNCHRONIZING to PERIODIC_SCAN returned true", result)

        val currentState = StateMachine.currentState
        Assert.assertEquals("Current state should remain SYNCHRONIZING but is $currentState", SYNCHRONIZING, currentState)
    }

    @Test
    fun `Changing State from SYNCHRONIZING to SYNCHRONIZING should return true`() {
        StateMachine.currentState = SYNCHRONIZING

        val result = StateMachine.changeState(SYNCHRONIZING)
        Assert.assertTrue("Changing state from SYNCHRONIZING to SYNCHRONIZING returned false", result)

        val currentState = StateMachine.currentState
        Assert.assertEquals("New Current state should be SYNCHRONIZING but is $currentState", SYNCHRONIZING, currentState)
    }

    @Test
    fun `Changing State from SYNCHRONIZING to IDLE should return true`() {
        StateMachine.currentState = SYNCHRONIZING

        val result = StateMachine.changeState(IDLE)
        Assert.assertTrue("Changing state from SYNCHRONIZING to IDLE returned false", result)

        val currentState = StateMachine.currentState
        Assert.assertEquals("Current state should be IDLE but is $currentState", IDLE, currentState)
    }

    @Test
    fun `Changing State from IDLE to LISTENING_FILES should return true`() {
        StateMachine.currentState = IDLE

        val result = StateMachine.changeState(LISTENING_FILES)
        Assert.assertTrue("Changing state from IDLE to LISTENING_FILES returned false", result)

        val currentState = StateMachine.currentState
        Assert.assertEquals("New current state should be LISTENING_FILES but is $currentState", LISTENING_FILES, currentState)
    }

    @Test
    fun `Changing State from IDLE to SYNCHRONIZING should return true`() {
        StateMachine.currentState = IDLE

        val result = StateMachine.changeState(SYNCHRONIZING)
        Assert.assertTrue("Changing state from IDLE to SYNCHRONIZING returned false", result)

        val currentState = StateMachine.currentState
        Assert.assertEquals("New current state should be SYNCHRONIZING but is $currentState", SYNCHRONIZING, currentState)
    }

    @Test
    fun `Changing State from IDLE to PERIODIC_SCAN should return true`() {
        StateMachine.currentState = IDLE

        val result = StateMachine.changeState(PERIODIC_SCAN)
        Assert.assertTrue("Changing state from IDLE to PERIODIC_SCAN returned false", result)

        val currentState = StateMachine.currentState
        Assert.assertEquals("New current state should be PERIODIC_SCAN but is $currentState", PERIODIC_SCAN, currentState)
    }
}