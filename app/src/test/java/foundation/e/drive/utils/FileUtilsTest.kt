/*
 * Copyright © MURENA SAS 2023.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */
package foundation.e.drive.utils

import org.junit.Assert
import org.junit.Test
import java.io.File

internal class FileUtilsTest {

    @Test
    fun `get file name from path with invalid path should return null`() {
        val invalidPath = "photo.jpg"
        val result = FileUtils.getFileNameFromPath(invalidPath)
        Assert.assertNull("Null was expected but got ${result.toString()}", result)
    }

    @Test
    fun `get file name from path with valid path should return the file name`() {
        val expectedResult = "photo.jpg"
        val validPath = "folder/photo.jpg"
        val result = FileUtils.getFileNameFromPath(validPath)
        Assert.assertEquals("Got $result instead of $expectedResult", expectedResult, result)

        val validPath2 = "/folder0/folder1/folder2/photo.jpg"
        val result2 = FileUtils.getFileNameFromPath(validPath2)
        Assert.assertEquals("Got $result2 instead of $expectedResult", expectedResult, result2)
    }

    @Test
    fun `get file name from path with wrong path separator should return null`() {
        val invalidPath = "\\folder\\photo.jpg"
        val result = FileUtils.getFileNameFromPath(invalidPath)
        Assert.assertNull("Got $result instead of null", result)
    }

    @Test
    fun `getMimeType from file with unusual extension `() {
        val expected = "*/*"
        val file = File("dummy.tmo")
        val result = FileUtils.getMimeType(file)

        Assert.assertEquals("Expected $expected but got $result", expected, result)
    }

    @Test
    fun `getMimeType from file without extension `() {
        val expected = "*/*"
        val file = File("dummy")
        val result = FileUtils.getMimeType(file)

        Assert.assertEquals("Expected $expected but got $result", expected, result)
    }

    @Test
    fun `getMimeType for picture file`() {
        val expected = "image/jpeg"
        val file = File("../files-for-test/picture.jpeg")
        val result = FileUtils.getMimeType(file)
        Assert.assertEquals("Expected $expected but got $result", expected, result)
    }
}