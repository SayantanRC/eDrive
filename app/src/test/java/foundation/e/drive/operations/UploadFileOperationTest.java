package foundation.e.drive.operations;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;


import static foundation.e.drive.models.SyncedFileStateKt.SCAN_EVERYWHERE;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.os.Build;

import androidx.test.core.app.ApplicationProvider;

import com.nextcloud.common.NextcloudClient;
import com.owncloud.android.lib.common.OwnCloudClient;
import com.owncloud.android.lib.common.Quota;
import com.owncloud.android.lib.common.UserInfo;
import com.owncloud.android.lib.common.operations.RemoteOperationResult;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowLog;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import foundation.e.drive.database.DbHelper;
import foundation.e.drive.models.SyncedFileState;
import foundation.e.drive.models.SyncedFolder;
import foundation.e.drive.TestUtils;
import foundation.e.drive.synchronization.tasks.UploadFileOperation;
import foundation.e.drive.utils.DavClientProvider;

@SuppressWarnings("rawtypes")
@RunWith(RobolectricTestRunner.class)
@Config(sdk = Build.VERSION_CODES.O, manifest = Config.NONE)
public class UploadFileOperationTest {
    private final List<SyncedFileState> syncedFileStates= new ArrayList<>();
    private final OwnCloudClient ocClient;
    private final NextcloudClient ncClient;
    private final Context context;
    private final long userFreeQuota = 50;
    private final Account account;

    public UploadFileOperationTest() {
        context = ApplicationProvider.getApplicationContext();
        final AccountManager accountManager = AccountManager.get(context);
        ShadowLog.stream = System.out;
        TestUtils.loadServerCredentials();
        TestUtils.prepareValidAccount(accountManager);
        account = TestUtils.getValidAccount();
        ocClient = DavClientProvider.getInstance().getClientInstance(account, context);
        ncClient = TestUtils.getNcClient(context);
    }

    @Before
    public void setUp() {
        prepareDB();
        assertNotNull("Client is null. unexpected!", ocClient);
    }

    /**
     * Prepare content of database for test
     * Insert three test folder: small, medium, large
     */
    private void prepareDB() {
        DbHelper.insertSyncedFolder(createSyncedFolder("small"), context);
        DbHelper.insertSyncedFolder(createSyncedFolder("medium"), context);
        DbHelper.insertSyncedFolder(createSyncedFolder("large"), context);

        assertEquals("There isn't three folder in DB as expected", 3, DbHelper.getSyncedFolderList(context, true).size());
    }

    /**
     * Create the syncedFolder instance
     * @param name the name of the folder
     * @return SyncedFolder instance
     */
    private SyncedFolder createSyncedFolder(String name) {
        return new SyncedFolder(name,
                TestUtils.TEST_LOCAL_ROOT_FOLDER_PATH + name + "/",
                TestUtils.TEST_REMOTE_ROOT_FOLDER_PATH + name + "/",
                true, true, true, true);
    }

    /**
     * Create a local small file to use for upload test
     */
    private File createSmallFile() {
        final String smallDummyFilePath = TestUtils.TEST_LOCAL_ROOT_FOLDER_PATH + "small/dummy.txt";
        File file = null;
        try {
            file = TestUtils.createFile(smallDummyFilePath, 2);
        } catch (IOException | SecurityException e ) {
            fail(e.getMessage());
        }

        final SyncedFileState sfs = new SyncedFileState(-1,
                "dummy.txt",
                TestUtils.TEST_LOCAL_ROOT_FOLDER_PATH + "small/dummy.txt",
                TestUtils.TEST_REMOTE_ROOT_FOLDER_PATH + "small/dummy.txt",
                "", 0L, 0, true, SCAN_EVERYWHERE);
        sfs.setId(DbHelper.manageSyncedFileStateDB(sfs, "INSERT", context));

        syncedFileStates.add(sfs);
        return file;
    }

    /**
     * Remove SmallFile locally if it exist
     * This method has just been made to make basic test to work
     * todo: refactor for next state, and test improvement
     * @return true if file deleted or if it doesn't exist
     */
    private boolean removeSmallFile() {
        if (!syncedFileStates.isEmpty()) {
            final SyncedFileState sfs = syncedFileStates.get(0);
            if (sfs != null) {
                syncedFileStates.remove(sfs);
            }
        }

        final String smallDummyFilePath = TestUtils.TEST_LOCAL_ROOT_FOLDER_PATH + "small/dummy.txt";
        final File smallFile = new File(smallDummyFilePath);
        if (smallFile.exists()) {
            return smallFile.delete();
        } else return true;
    }

    /**
     * test upload of a file and check that it's ok
     */
    @Ignore("Fail on gitlab, reason not found yet")
    @Test
    public void uploadNewSmallFile_shouldwork() {
        removeSmallFile(); //clean the environment
        final File file = createSmallFile(); //preparation

        final SyncedFileState sfs_fromDB = DbHelper.loadSyncedFile(context, syncedFileStates.get(0).getLocalPath(), true);
        assertTrue("SyncedFileState loaded from DB must have an empty ETag", sfs_fromDB.getLastEtag().isEmpty());
        final UploadFileOperation operation = Mockito.spy(new UploadFileOperation(syncedFileStates.get(0), account, context));
        mockCreateRemoteDir(true, operation, sfs_fromDB.getRemotePath());
        mockUserInfoReading(operation);
        mockFileHeadOperation(true, operation, sfs_fromDB.getRemotePath());
        mockSuccessfulFileUpload(operation, file, false);

        //noinspection deprecation
        final RemoteOperationResult result = operation.execute(ocClient);
        String errorMsg = "The upload failed:\n http code: " + result.getHttpCode()
                + "\n, is success ?" + result.isSuccess()
                + "\n, log msg: " + result.getLogMessage()
                + "\n, is server fail ? " + result.isServerFail();
        if (result.getException() != null) {
            errorMsg += "\n, exception msg: " + result.getException().getMessage();
        }
        assertTrue( errorMsg, result.isSuccess());

        final SyncedFileState sfs_fromDBAfterUpload = DbHelper.loadSyncedFile(context, syncedFileStates.get(0).getLocalPath(), true);
        assertFalse("After upload, the database must store the etag of the syncedFileState. But here it is empty", sfs_fromDBAfterUpload.getLastEtag().isEmpty());
    }

    /**
     * Try to upload local file that has been removed between uploadOperation's creation and
     *  ploadOperation execution
     */
    @Test
    public void localFileRemovedBeforeUpload_shouldFail() {
        removeSmallFile(); //clean the environment
        createSmallFile(); //preparation

        final SyncedFileState sfs_fromDB = DbHelper.loadSyncedFile(context, syncedFileStates.get(0).getLocalPath(), true);
        assertTrue("SyncedFileState loaded from DB must have an empty Etag", sfs_fromDB.getLastEtag().isEmpty());

        final UploadFileOperation testOperation = new UploadFileOperation(syncedFileStates.get(0), account, context);

        final File smallFile = new File(sfs_fromDB.getLocalPath());
        assertTrue("Local file deletion return false instead of true", smallFile.delete());

        //noinspection deprecation
        final RemoteOperationResult result = testOperation.execute(ocClient);
        assertEquals("Expected result code was FORBIDDEN but got: " + result.getCode().name(), RemoteOperationResult.ResultCode.FORBIDDEN, result.getCode());
    }

    /**
     * Assert that uploading a file that is bigger than free quotas isn't allowed
     */
    @Test
    public void fileSizeBiggerThanFreeQuota_shouldnotBeAllowed() {
        assertNotEquals("Reading remote free quota fails" + userFreeQuota, -1, userFreeQuota);

        final UploadFileOperation operation = Mockito.spy(new UploadFileOperation(Mockito.mock(SyncedFileState.class), account, context));
        mockUserInfoReading(operation);
        final RemoteOperationResult actualResult = operation.checkAvailableSpace(ncClient, (userFreeQuota+1));
        assertEquals("Quota check (" + userFreeQuota + "vs" + (userFreeQuota+1) + ") failed", RemoteOperationResult.ResultCode.QUOTA_EXCEEDED, actualResult.getCode());
    }

    /**
     * Assert that uploading a file which size is exactlyF the amount of free quota isn't allowed
     */
    @Test
    public void fileSizeEqualToFreeQuota_shouldBeAllowed() {
        assertNotEquals("Reading remote free quota fails" + userFreeQuota, -1, userFreeQuota);

        final UploadFileOperation operation = Mockito.spy(new UploadFileOperation(Mockito.mock(SyncedFileState.class), account, context));
        mockUserInfoReading(operation);

        final RemoteOperationResult actualResult = operation.checkAvailableSpace(ncClient, userFreeQuota);
        assertNotEquals("Quota check is Quota Exceeded (" + userFreeQuota + " vs " + userFreeQuota + ")",
                RemoteOperationResult.ResultCode.QUOTA_EXCEEDED,
                actualResult.getCode());
        assertEquals("Quota Check is not OK",
                RemoteOperationResult.ResultCode.OK,
                actualResult.getCode());
    }

    /**
     * Assert that uploading a file which size is lower than the amount of free quota is allowed
     *
     */
    @Test
    public void fileSizeSmallerThanFreeQuota_shouldBeAllowed() {
        assertNotEquals("Reading remote free quota fails " + userFreeQuota, -1, userFreeQuota);

        final UploadFileOperation operation = Mockito.spy(new UploadFileOperation(Mockito.mock(SyncedFileState.class), account, context));
        mockUserInfoReading(operation);
        final RemoteOperationResult actualResult = operation.checkAvailableSpace(ncClient, (userFreeQuota-1));
        assertEquals("Quota check (" + userFreeQuota + " vs " + (userFreeQuota-1) + ") failed",
                RemoteOperationResult.ResultCode.OK,
                actualResult.getCode());
    }

    @Test
    public void ifMatchEtagRequired_notMediaType_expectedFalse() {
        final SyncedFileState testSyncedState = new SyncedFileState(1,
                "test.jpg",
                "pictures/toto.jpg",
                "pictures/test.jpg",
                "123456",
                11111220,
                1,
                false,
                SCAN_EVERYWHERE);

        assertFalse("SyncedFileState should not have a media type but it has", testSyncedState.isMediaType());

        final UploadFileOperation operationUnderTest = new UploadFileOperation(testSyncedState, account, context);
        final boolean addIfMatchHeader =operationUnderTest.ifMatchETagRequired(ocClient);
        assertFalse("Expected result for ifMatchETagRequired(client) was false but got: true", addIfMatchHeader);
    }

    @Test
    public void ifMatchETagRequired_empyEtag_expectedFalse() {
        final SyncedFileState testSyncedState = new SyncedFileState(1,
                "test.jpg",
                "pictures/toto.jpg",
                "pictures/test.jpg",
                "",
                11111220,
                1,
                true,
                SCAN_EVERYWHERE);

        assertTrue("SyncedFileState's ETag should be empty but isn't", testSyncedState.getLastEtag().isEmpty());
        assertTrue("SyncedFileState is not a media type but it should", testSyncedState.isMediaType());
        final UploadFileOperation operationUnderTest = new UploadFileOperation(testSyncedState, account, context);
        final boolean addIfMatchHeader =operationUnderTest.ifMatchETagRequired(ocClient);
        assertFalse("Expected result for ifMatchETagRequired(client) was false but got: true", addIfMatchHeader);
    }

    @Test
    public void ifMatchETagRequired_missingRemoteFile_expectedFalse() {
        final SyncedFileState testSyncedState = new SyncedFileState(1,
                "test.jpg",
                "pictures/toto.jpg",
                "pictures/test.jpg",
                "123456",
                11111220,
                1,
                true,
                SCAN_EVERYWHERE);

        assertFalse("SyncedFileState's ETag should not be empty but isn't", testSyncedState.getLastEtag().isEmpty());
        assertTrue("SyncedFileState is not a media type but it should", testSyncedState.isMediaType());
        final UploadFileOperation operationUnderTest = Mockito.spy(new UploadFileOperation(testSyncedState, account, context));
        mockFileHeadOperation(false, operationUnderTest, testSyncedState.getRemotePath());
        final boolean addIfMatchHeader =operationUnderTest.ifMatchETagRequired(ocClient);
        assertFalse("Expected result for ifMatchETagRequired(client) is false but got: true", addIfMatchHeader);
    }

    @Test
    public void ifMatchETagRequired_expectedTrue() {
        final SyncedFileState testSyncedState = new SyncedFileState(1,
                "test.jpg",
                "pictures/toto.jpg",
                "pictures/test.jpg",
                "123456",
                11111220,
                1,
                true,
                SCAN_EVERYWHERE);

        assertFalse("SyncedFileState's ETag should not be empty but isn't", testSyncedState.getLastEtag().isEmpty());
        assertTrue("SyncedFileState is not a media type but it should", testSyncedState.isMediaType());
        final UploadFileOperation operationUnderTest = Mockito.spy(new UploadFileOperation(testSyncedState, account, context));
        mockFileHeadOperation(true, operationUnderTest, testSyncedState.getRemotePath());
        final boolean addIfMatchHeader =operationUnderTest.ifMatchETagRequired(ocClient);
        assertTrue("Expected result for ifMatchETagRequired(client) is true but got: false", addIfMatchHeader);
    }

    @Test
    public void formatTimeStampToMatchCloud_validTimestamp() {
        final long initialTimestamp = 1683017095074L;
        final long expectedOutput = 1683017095L;
        final SyncedFileState mockedSyncedState = Mockito.mock(SyncedFileState.class);
        final UploadFileOperation operationUnderTest = new UploadFileOperation(mockedSyncedState, account, context);

        final long output = operationUnderTest.formatTimestampToMatchCloud(initialTimestamp);
        assertEquals("Output doesn't match expectation.", expectedOutput, output);
    }

    @Test
    public void formatTimeStampToMatchCloud_invalidTimestamp() {
        final long initialTimestamp = 0L;
        final long expectedOutput = 0L;
        final SyncedFileState mockedSyncedState = Mockito.mock(SyncedFileState.class);
        final UploadFileOperation operationUnderTest = new UploadFileOperation(mockedSyncedState, account, context);

        final long output = operationUnderTest.formatTimestampToMatchCloud(initialTimestamp);
        assertEquals("Output doesn't match expectation.", expectedOutput, output);
    }

    private void mockUserInfoReading(UploadFileOperation instanceUnderTest) {
        final long userTotalQuota = 100;
        final long userUsedQuota = 50;
        final double userRelativeQuota = 50;
        final Quota quota = new Quota(userFreeQuota, userUsedQuota, userTotalQuota, userRelativeQuota, 100);
        final UserInfo uInfo = new UserInfo("id", true, "toto", "toto@toto.com", "+123456789", "adress", "", "", quota, null);
        final RemoteOperationResult<UserInfo> mockResponse = new RemoteOperationResult<>(RemoteOperationResult.ResultCode.OK);
        mockResponse.setResultData(uInfo);
        Mockito.doReturn(mockResponse).when(instanceUnderTest).readUserInfo(ncClient);
    }

    @SuppressWarnings("SameParameterValue")
    private void mockSuccessfulFileUpload(final UploadFileOperation instanceUnderTest, final File file, final boolean checkETag) {
        final String eTag = "123456789";
        final RemoteOperationResult<String> mockResponse = new RemoteOperationResult<>(RemoteOperationResult.ResultCode.OK);
        mockResponse.setResultData(eTag);
        Mockito.doReturn(mockResponse).when(instanceUnderTest).uploadFile(file, ocClient, checkETag);
    }

    private void mockFileHeadOperation(boolean expectedResult, final UploadFileOperation instanceUdnerTest, final String remotePath) {
        Mockito.doReturn(expectedResult).when(instanceUdnerTest).remoteFileExist(remotePath, ocClient);
    }

    @SuppressWarnings("SameParameterValue")
    private void mockCreateRemoteDir(boolean successExpected, final UploadFileOperation instanceUnderTest, final String remotePath) {
        Mockito.doReturn(successExpected).when(instanceUnderTest).createRemoteFolder(remotePath, ocClient);
    }
}