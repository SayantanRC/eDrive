/*
 * Copyright © MURENA SAS 2022-2023.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */

package foundation.e.drive.database;
import static foundation.e.drive.models.SyncedFileStateKt.SCAN_EVERYWHERE;

import android.content.Context;
import android.os.Build;

import androidx.test.core.app.ApplicationProvider;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.util.ArrayList;
import java.util.List;

import foundation.e.drive.models.SyncedFileState;
import foundation.e.drive.models.SyncedFolder;

/**
 * @author vincent Bourgmayer
 */
@RunWith(RobolectricTestRunner.class)
@Config(sdk = Build.VERSION_CODES.O, manifest = Config.NONE)
public class SyncedFileStateDAOTest {

    private SyncedFileStateDAO daoUnderTest;
    private final Context context;

    public SyncedFileStateDAOTest() {
        context = ApplicationProvider.getApplicationContext();
    }


    @Before
    public void prepareDB() {
        final SyncedFolder dummyFolder = new SyncedFolder("Picture", "local/", "remote/", true, true, true, true);
        dummyFolder.setId(12);
        DbHelper.insertSyncedFolder(dummyFolder, context);

        final SyncedFileState dummy = new SyncedFileState(6, "foo.jpg", "local/foo.jpg", "remote/foo.jpg", "7777", 0L, 12, true, SCAN_EVERYWHERE);
        DbHelper.manageSyncedFileStateDB(dummy, "INSERT", context);

        daoUnderTest = DbHelper.openSyncedFileStateDAO(context, false);
    }

    @After
    public void closeDB() {
        daoUnderTest.close();
    }

    @Test
    public void getBySyncedFolderIds_emptyIdList_returnEmptyList() {
        final List<Long> folderIds = new ArrayList<>();
        final List<SyncedFileState> result = daoUnderTest.getBySyncedFolderIds(folderIds);
        Assert.assertTrue(result.isEmpty());
    }


    @Test
    public void getBySyncedFolderIds_notEmpty_returnOneFileState() {
        final List<Long> folderIds = new ArrayList<>();
        folderIds.add(new Long(12));
        final List<SyncedFileState> result = daoUnderTest.getBySyncedFolderIds(folderIds);
        Assert.assertFalse("result should contain 1 SyncedFileState but was empty", result.isEmpty());
    }
}
