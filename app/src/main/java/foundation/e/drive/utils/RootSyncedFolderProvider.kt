/*
 * Copyright © MURENA SAS 2023.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */
package foundation.e.drive.utils

import android.content.Context
import android.os.Build
import android.os.Build.BRAND
import android.os.Build.MODEL
import android.os.Environment
import android.os.Environment.DIRECTORY_DCIM
import android.os.Environment.DIRECTORY_DOCUMENTS
import android.os.Environment.DIRECTORY_MOVIES
import android.os.Environment.DIRECTORY_MUSIC
import android.os.Environment.DIRECTORY_PICTURES
import android.os.Environment.DIRECTORY_PODCASTS
import android.os.Environment.DIRECTORY_RECORDINGS
import android.os.Environment.DIRECTORY_RINGTONES
import androidx.annotation.RequiresApi
import com.owncloud.android.lib.resources.files.FileUtils.PATH_SEPARATOR
import foundation.e.drive.models.SyncedFolder

object RootSyncedFolderProvider {
    private const val CATEGORY_IMAGES = "Images"
    private const val CATEGORY_MOVIES = "Movies"
    private const val CATEGORY_MUSIC = "Music"
    private const val CATEGORY_RECORDINGS = "Recordings"
    private const val CATEGORY_RINGTONES = "Ringtones"
    private const val CATEGORY_DOCUMENTS = "Documents"
    private const val CATEGORY_PODCASTS = "Podcasts"
    private const val CATEGORY_ROM_SETTINGS = "Rom settings"

    private const val LOCAL_ROM_SETTINGS_PATH = "/data/system/users/0/"
    private val REMOTE_ROM_SETTINGS_PATH = PATH_SEPARATOR + "Devices" + PATH_SEPARATOR + BRAND + "_" + MODEL + "_" + Build.getSerial() + "/rom_settings/"
    private val REMOTE_APP_LIST_PATH = REMOTE_ROM_SETTINGS_PATH + "app_list/"

    fun getSyncedFolderRoots(context : Context): List<SyncedFolder> {
        val syncedFolders = ArrayList<SyncedFolder>()
        val categories = getSyncableCategories()

        categories.forEach {
            when (it) {
                CATEGORY_IMAGES -> {
                    syncedFolders.add(createPhotosSyncedFolder())
                    syncedFolders.add(createPicturesSyncedFolder())
                }
                CATEGORY_MOVIES -> syncedFolders.add(createMovieSyncedFolder())
                CATEGORY_DOCUMENTS -> syncedFolders.add(createDocumentsSyncedFolder())
                CATEGORY_MUSIC -> syncedFolders.add(createMusicsSyncedFolder())
                CATEGORY_RECORDINGS -> {
                    if (isAboveA12()) {
                        syncedFolders.add(createRecordingsSyncedFolder())
                    }
                }
                CATEGORY_PODCASTS -> syncedFolders.add(createPodcastsSyncedFolder())
                CATEGORY_RINGTONES -> syncedFolders.add(createRingtonesSyncedFolder())
                CATEGORY_ROM_SETTINGS -> {
                    syncedFolders.add(createRomSettingsSyncedFolder())
                    syncedFolders.add(createAppListSyncedFolder(context))
                }
            }
        }
        return syncedFolders
    }

    private fun getSyncableCategories(): List<String> {
        return listOf(
            CATEGORY_IMAGES,
            CATEGORY_MOVIES,
            CATEGORY_MUSIC,
            CATEGORY_RECORDINGS,
            CATEGORY_RINGTONES,
            CATEGORY_DOCUMENTS,
            CATEGORY_PODCASTS,
            CATEGORY_ROM_SETTINGS,
        )
    }

    private fun createPhotosSyncedFolder(): SyncedFolder {
        return createMediaSyncedFolder(CATEGORY_IMAGES, DIRECTORY_DCIM, "/Photos/")
    }

    private fun createPicturesSyncedFolder(): SyncedFolder {
        return  createMediaSyncedFolder(CATEGORY_IMAGES, DIRECTORY_PICTURES, "/Pictures/")
    }

    private fun createMovieSyncedFolder(): SyncedFolder {
        return createMediaSyncedFolder(CATEGORY_MOVIES, DIRECTORY_MOVIES, "/Movies/")
    }

    private fun createDocumentsSyncedFolder(): SyncedFolder {
        return createMediaSyncedFolder(CATEGORY_DOCUMENTS, DIRECTORY_DOCUMENTS, "/Documents/")
    }

    private fun createMusicsSyncedFolder(): SyncedFolder {
        return createMediaSyncedFolder(CATEGORY_MUSIC, DIRECTORY_MUSIC, "/Music/")
    }

    @RequiresApi(Build.VERSION_CODES.S)
    private fun createRecordingsSyncedFolder(): SyncedFolder {
        return createMediaSyncedFolder(CATEGORY_RECORDINGS, DIRECTORY_RECORDINGS, "/Recordings/")
    }

    private fun createRingtonesSyncedFolder(): SyncedFolder {
        return createMediaSyncedFolder(CATEGORY_RINGTONES, DIRECTORY_RINGTONES, "/Ringtones/")
    }

    private fun createPodcastsSyncedFolder(): SyncedFolder {
        return createMediaSyncedFolder(CATEGORY_PODCASTS, DIRECTORY_PODCASTS, "/Podcasts/")
    }

    private fun createRomSettingsSyncedFolder(): SyncedFolder {
        return createSettingsSyncedFolder(CATEGORY_ROM_SETTINGS, LOCAL_ROM_SETTINGS_PATH, REMOTE_ROM_SETTINGS_PATH)
    }

    private fun createAppListSyncedFolder(context: Context): SyncedFolder {
        val localPath = context.filesDir.absolutePath + PATH_SEPARATOR
        return createSettingsSyncedFolder(CATEGORY_ROM_SETTINGS, localPath, REMOTE_APP_LIST_PATH)
    }

    private fun createMediaSyncedFolder(category: String, publicDirectoryType: String, remotePath: String): SyncedFolder {
        val dirPath = FileUtils.getLocalPath(Environment.getExternalStoragePublicDirectory(publicDirectoryType))
        val localPath = dirPath + PATH_SEPARATOR
        return SyncedFolder(category, localPath, remotePath, true)
    }

    private fun createSettingsSyncedFolder(category: String, localPath: String, remotePath: String): SyncedFolder {
        return SyncedFolder(category, localPath, remotePath, true, false, true, false)
    }

    /**
     * Function to check if device android version is above Android 12, SDK 31
     *
     * @return true or false
     */
    private fun isAboveA12(): Boolean {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.S
    }
}