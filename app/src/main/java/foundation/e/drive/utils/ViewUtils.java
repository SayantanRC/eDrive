/*
 * Copyright © MURENA SAS 2023.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */

package foundation.e.drive.utils;

import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;

import foundation.e.drive.widgets.EDriveWidget;

public class ViewUtils {

    public static void updateWidgetView(@NonNull Context context) {
        final Intent updateIntent = new Intent(context, EDriveWidget.class);
        updateIntent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
        context.sendBroadcast(updateIntent);
    }
}
