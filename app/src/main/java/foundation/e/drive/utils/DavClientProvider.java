/*
 * Copyright © CLEUS SAS 2019.
 * Copyright © MURENA SAS 2023
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */

package foundation.e.drive.utils;

import android.accounts.Account;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.nextcloud.common.NextcloudClient;
import com.owncloud.android.lib.common.OwnCloudAccount;
import com.owncloud.android.lib.common.OwnCloudClient;
import com.owncloud.android.lib.common.OwnCloudClientManager;
import com.owncloud.android.lib.common.OwnCloudClientManagerFactory;
import com.owncloud.android.lib.common.accounts.AccountUtils;

import java.io.IOException;

import foundation.e.drive.R;
import timber.log.Timber;

/**
 * @author Vincent Bourgmayer
 */
@SuppressWarnings({"deprecation"})
public class DavClientProvider {

    private final static DavClientProvider instance = new DavClientProvider();

    private DavClientProvider() {
    }

    @Nullable
    public OwnCloudClient getClientInstance(@Nullable final Account account, @NonNull final Context context) {
        if (account == null) {
            return null;
        }

        try {
            final OwnCloudClientManager ownCloudClientManager = OwnCloudClientManagerFactory.getDefaultSingleton();
            final OwnCloudAccount ocAccount = new OwnCloudAccount(account, context);
            return ownCloudClientManager.getClientFor(ocAccount, context);
        } catch (AccountUtils.AccountNotFoundException | OperationCanceledException |
                 AuthenticatorException | IOException exception) {
            Timber.e(exception);
            return null;
        }
    }

    @Nullable
    public NextcloudClient getNcClientInstance(@Nullable final Account account, @NonNull final Context context) {
        if (account == null) {
            return null;
        }

        try {
            final OwnCloudClientManager ownCloudClientManager = OwnCloudClientManagerFactory.getDefaultSingleton();
            final OwnCloudAccount ocAccount = new OwnCloudAccount(account, context);
            return ownCloudClientManager.getNextcloudClientFor(ocAccount, context);
        } catch (AccountUtils.AccountNotFoundException | OperationCanceledException |
                 AuthenticatorException | IOException exception) {
            Timber.e(exception);
            return null;
        }

    }

    public void cleanUp() {
        final OwnCloudClientManager ownCloudClientManager = OwnCloudClientManagerFactory.getDefaultSingleton();
        ownCloudClientManager.removeClientForByName(null);
    }

    public void saveAccounts(@NonNull Context context) {
        OwnCloudClientManagerFactory.getDefaultSingleton().saveAllClients(context, context.getString(R.string.eelo_account_type));
    }

    @NonNull
    public static DavClientProvider getInstance() {
        return instance;
    }
}
