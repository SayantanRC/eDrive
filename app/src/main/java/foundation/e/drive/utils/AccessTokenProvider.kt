/*
 * Copyright MURENA SAS 2024
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.drive.utils

import android.accounts.Account
import android.accounts.AccountManager
import com.owncloud.android.lib.common.accounts.AccountUtils
import net.openid.appauth.AuthState
import org.json.JSONException
import timber.log.Timber

object AccessTokenProvider {

    private const val PLACEHOLDER_TOKEN = "placeholder"

    @JvmStatic
    fun getToken(accountManager: AccountManager, account: Account?): String {
        getAuthState(accountManager, account)?.let { authState ->
            try {
                return AuthState.jsonDeserialize(authState).accessToken ?: PLACEHOLDER_TOKEN
            } catch (e: JSONException) {
                Timber.e(e)
            }
        }

        return PLACEHOLDER_TOKEN
    }

    private fun getAuthState(accountManager: AccountManager, account: Account?): String? {
        var authState: String? = null

        if (account != null) {
            authState = accountManager.getUserData(account, AccountUtils.Constants.KEY_AUTH_STATE)
        }

        return if (authState.isNullOrBlank()) null else authState
    }
}
