/*
 * Copyright © CLEUS SAS 2018-2019.
 * Copyright © MURENA SAS 2022-2023.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */
package foundation.e.drive.utils

/**
 * @author Vincent Bourgmayer
 * @author Abhishek Aggarwal
 */
object AppConstants {
    const val MEDIA_SYNC_PROVIDER_AUTHORITY = "foundation.e.drive.providers.MediasSyncProvider"
    const val SETTINGS_SYNC_PROVIDER_AUTHORITY = "foundation.e.drive.providers.SettingsSyncProvider"
    const val METERED_NETWORK_ALLOWED_AUTHORITY =
        "foundation.e.drive.providers.MeteredConnectionAllowedProvider"
    const val SETUP_COMPLETED = "setup_completed"
    const val KEY_LAST_SCAN_TIME = "lastScanTimestamp"
    const val INITIAL_FOLDER_NUMBER = "initial_folder_number"
    const val APPLICATIONS_LIST_FILE_NAME = "packages_list.csv"
    const val SHARED_PREFERENCE_NAME = "preferences"
    const val ACCOUNT_DATA_NAME = "display_name"
    const val ACCOUNT_DATA_USED_QUOTA_KEY = "used_quota"
    const val ACCOUNT_DATA_TOTAL_QUOTA_KEY = "total_quota"
    const val ACCOUNT_DATA_RELATIVE_QUOTA_KEY = "relative_quota"
    const val ACCOUNT_DATA_GROUPS = "group"
    const val ACCOUNT_DATA_ALIAS_KEY = "alias"
    const val ACCOUNT_DATA_EMAIL = "email"
    const val ACCOUNT_USER_ID_KEY = "USERID"
    const val notificationChannelID = "foundation.e.drive"
    const val WORK_GENERIC_TAG = "eDrive"
    const val WORK_SETUP_TAG = "eDrive-init"
    const val CORRUPTED_TIMESTAMP_IN_MILLISECOND = 4294967295000L
}
