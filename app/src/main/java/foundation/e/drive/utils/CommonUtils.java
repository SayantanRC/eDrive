/*
 * Copyright © CLEUS SAS 2018-2019.
 * Copyright © MURENA SAS 2022-2023.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */

package foundation.e.drive.utils;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.annotation.SuppressLint;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ContentResolver;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkCapabilities;

import android.net.Uri;
import android.widget.Toast;

import java.lang.reflect.Method;
import java.text.CharacterIterator;
import java.text.StringCharacterIterator;
import java.util.Locale;

import foundation.e.drive.R;
import timber.log.Timber;

import static foundation.e.drive.utils.AppConstants.MEDIA_SYNC_PROVIDER_AUTHORITY;
import static foundation.e.drive.utils.AppConstants.METERED_NETWORK_ALLOWED_AUTHORITY;
import static foundation.e.drive.utils.AppConstants.SETTINGS_SYNC_PROVIDER_AUTHORITY;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * @author Vincent Bourgmayer
 * @author Abhishek Aggarwal
 * @author Jonathan Klee
 * @author Mohit Mali
 */
public abstract class CommonUtils {

    /**
     * This method retrieve Account corresponding to account's type
     *
     * @param accountType account type
     * @param am          Account Manager
     * @return Account or null if not found
     */
    @Nullable
    public static Account getAccount(@Nullable String accountType, @NonNull AccountManager am) {
        Account[] accounts = am.getAccounts();
        for (int i = -1, size = accounts.length; ++i < size; ) {
            if (accounts[i].type.equals(accountType)) {
                return accounts[i];
            }
        }
        return null;
    }

    /**
     * Say if synchronisation is allowed
     *
     * @param account                Account used for synchronisation
     * @param syncedFileStateIsMedia true if the concerned syncedFileState is a media's type element, false if it is a settings's type element
     * @return true if sync is allowed in given condition
     */
    public static boolean isThisSyncAllowed(@NonNull Account account, boolean syncedFileStateIsMedia) {
        return (syncedFileStateIsMedia && isMediaSyncEnabled(account))
                || (!syncedFileStateIsMedia && isSettingsSyncEnabled(account));
    }

    /**
     * Say if Media Sync is enabled in account
     *
     * @param account Concerned account
     * @return true if media sync enabled
     */
    public static boolean isMediaSyncEnabled(@NonNull Account account) {
        return ContentResolver.getSyncAutomatically(account, MEDIA_SYNC_PROVIDER_AUTHORITY);
    }

    /**
     * Say if Settings Sync is enabled in account
     *
     * @param account Concerned account
     * @return true if enabled
     */
    public static boolean isSettingsSyncEnabled(@NonNull Account account) {
        return ContentResolver.getSyncAutomatically(account, SETTINGS_SYNC_PROVIDER_AUTHORITY);
    }

    /**
     * Read accountManager settings
     * to check if user can sync on metered network
     * @param account account used to check settings
     * @return true if usage of metered connection is allowed
     */
    public static boolean isMeteredNetworkAllowed(@NonNull Account account) {
        return ContentResolver.getSyncAutomatically(account, METERED_NETWORK_ALLOWED_AUTHORITY);
    }
    
    /**
     * Tell if there is internet connection
     *
     * @param context               Activity or service which are calling this method
     * @param meteredNetworkAllowed true if service can use metered network / false either
     * @return True if there is connection, false either
     */
    public static boolean haveNetworkConnection(@NonNull Context context, boolean meteredNetworkAllowed) {

        final ConnectivityManager cm = context.getSystemService(ConnectivityManager.class);
        final NetworkCapabilities capabilities = cm.getNetworkCapabilities(cm.getActiveNetwork());

        return capabilities != null
                && capabilities.hasCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
                && capabilities.hasCapability(NetworkCapabilities.NET_CAPABILITY_VALIDATED)
                && (capabilities.hasCapability(NetworkCapabilities.NET_CAPABILITY_NOT_METERED)
                || meteredNetworkAllowed);
    }

    /**
     * Formatter class is not used since bytes passed by server are in SI unit aka 1kb = 1024byte
     * <a href="https://stackoverflow.com/questions/3758606/how-can-i-convert-byte-size-into-a-human-readable-format-in-java/3758880#3758880">...</a>
     *
     * @param bytes file/data size in bytes
     * @return String in human readable format
     */
    @NonNull
    public static String humanReadableByteCountBin(long bytes) {
        long absB = bytes == Long.MIN_VALUE ? Long.MAX_VALUE : Math.abs(bytes);
        if (absB < 1024) {
            return bytes + " B";
        }
        long value = absB;
        CharacterIterator ci = new StringCharacterIterator("KMGTPE");
        for (int i = 40; i >= 0 && absB > 0xfffccccccccccccL >> i; i -= 10) {
            value >>= 10;
            ci.next();
        }
        value *= Long.signum(bytes);
        return String.format(Locale.ENGLISH, "%.1f %cB", value / 1024.0, ci.current());
    }

    public static void createNotificationChannel(@NonNull Context context) {
        final CharSequence name = context.getString(R.string.notif_channel_name);
        final String description = context.getString(R.string.notif_channel_description);
        int importance = NotificationManager.IMPORTANCE_DEFAULT;
        final NotificationChannel channel = new NotificationChannel(AppConstants.notificationChannelID, name, importance);
        channel.setDescription(description);
        // Register the channel with the system; you can't change the importance
        // or other notification behaviors after this
        final NotificationManager notificationManager = context.getSystemService(NotificationManager.class);
        notificationManager.createNotificationChannel(channel);
    }

    public static void copyToClipboard(@NonNull Uri data, @NonNull Context context, @NonNull String label) {
        final ClipboardManager clipboard =
                (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        final ClipData clip = ClipData.newPlainText(label, String.valueOf(data));
        clipboard.setPrimaryClip(clip);
        Toast.makeText(context, context.getString(R.string.copied_to_clipboard, label),
                Toast.LENGTH_SHORT).show();
    }

    /**
     * Function for get build props through reflection
     *
     * @param prop key that you want to get value for
     * @return value of the prop, returns empty if not found
     */
    @SuppressLint("PrivateApi")
    @NonNull
    public static String getProp(@NonNull String prop) {
        String value = "";
        try {
            Class<?> c = Class.forName("android.os.SystemProperties");
            Method get = c.getMethod("get", String.class);
            value = (String) get.invoke(c, prop);
        } catch (Exception e) {
            Timber.e(e);
        }
        return value == null ? "" : value;
    }
}