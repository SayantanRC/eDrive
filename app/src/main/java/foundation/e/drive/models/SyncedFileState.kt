/*
 * Copyright © MURENA SAS 2022-2023.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */
package foundation.e.drive.models

import android.os.Parcel
import android.os.Parcelable


const val DO_NOT_SCAN = 0
const val SCAN_ON_CLOUD = 1
const val SCAN_ON_DEVICE = 2
const val SCAN_EVERYWHERE = 3

/**
 * Class to hold data about a synchronized file
 *
 *  1. file state: no etag & mTime <=0 : new local file discovered
 * 2. filestate: no etag but mTime > 0 : ???
 * When does it happens ?, and what does it correspond to ? what side effect of such case ?
 * 3. fileState: etag but mTime <= 0 : new remote file discovered
 * 4. fileState: etag & mTime > 0: file synchronized once
 *
 * @param id Id of syncedFileState in database
 * @param name Name of the file
 * @param localPath path to the file on the device
 * @param remotePath path to the file on the cloud
 * @param lastEtag Last known value of eTag, from cloud
 * @param lastModified Last known value of "lastModified", from a file on device
 * @param syncedFolderId Id of SyncedFolder (parent) from database
 * @param scanScope define on which mount point the file should be scanned for change
 *
 * @author Vincent Bourgmayer
 */
data class SyncedFileState (var id: Int,
                            val name: String,
                            val localPath: String,
                            val remotePath: String,
                            var lastEtag: String,
                            var lastModified: Long,
                            val syncedFolderId: Long,
                            val isMediaType: Boolean,
                            var scanScope: Int) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readLong(),
        parcel.readLong(),
        parcel.readByte() != 0.toByte(),
        parcel.readInt()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(name)
        parcel.writeString(localPath)
        parcel.writeString(remotePath)
        parcel.writeString(lastEtag)
        parcel.writeLong(lastModified)
        parcel.writeLong(syncedFolderId)
        parcel.writeByte(if (isMediaType) 1 else 0)
        parcel.writeInt(scanScope)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<SyncedFileState> {
        override fun createFromParcel(parcel: Parcel): SyncedFileState {
            return SyncedFileState(parcel)
        }

        override fun newArray(size: Int): Array<SyncedFileState?> {
            return arrayOfNulls(size)
        }
    }

    fun isLastEtagStored() : Boolean {
        return !this.lastEtag.isNullOrEmpty()
    }

    fun hasBeenSynchronizedOnce(): Boolean {
        return isLastEtagStored() && this.lastModified > 0L
    }

    fun disableScanning() {
        this.scanScope = DO_NOT_SCAN
    }

    override fun toString(): String {
        return """
             SyncedFileState :
             Id: ${id}
             Name: ${name}
             LocalPath: ${localPath}
             RemotePath: ${remotePath}
             Last Etag: ${lastEtag}
             Local last modified: ${lastModified}
             SyncedFolderId: ${syncedFolderId}
             isMediaType: ${isMediaType}
             scannable: ${scanScope}
             """.trimIndent()
    }
}