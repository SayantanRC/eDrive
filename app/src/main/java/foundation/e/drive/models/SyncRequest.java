/*
 * Copyright © MURENA SAS 2022-2023.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */
package foundation.e.drive.models;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class SyncRequest {
    public enum Type { UPLOAD, DOWNLOAD, DISABLE_SYNCING};

    private final SyncedFileState syncedFileState;

    private final Type operationType;

    public SyncRequest(@NonNull SyncedFileState syncedFileState, @NonNull Type operationType) {
        this.syncedFileState = syncedFileState;
        this.operationType = operationType;
    }

    @NonNull
    public Type getOperationType() {
        return operationType;
    }

    @NonNull
    public SyncedFileState getSyncedFileState() {
        return syncedFileState;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (obj instanceof SyncRequest) {
            final SyncedFileState objFileState = ((SyncRequest) obj).syncedFileState;
            return (syncedFileState.getLocalPath().equals(objFileState.getLocalPath()) ||
                    syncedFileState.getRemotePath().equals(objFileState.getRemotePath()));
        }
        return super.equals(obj);
    }
}
