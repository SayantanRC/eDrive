/*
 * Copyright (C) 2024 MURENA SAS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
package foundation.e.drive.models;

import android.accounts.Account;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.owncloud.android.lib.common.operations.RemoteOperation;

import foundation.e.drive.synchronization.tasks.DownloadFileOperation;
import foundation.e.drive.synchronization.tasks.UploadFileOperation;

/**
 * This class encapsulates data, for SynchronizationService, about a thread which run a RemoteOperation
 *
 * @author Vincent Bourgmayer
 */
public class SyncWrapper {
    private final SyncRequest request;
    private final RemoteOperation remoteOperation;

    /**
     * Build an instance of SyncThreadHolder for a file transfer
     * @param request SyncRequest at origin of the file transfer
     * @param context Application context, used to create RemoteOperation to run
     */
    public SyncWrapper(@NonNull final SyncRequest request, @Nullable final Account account, @NonNull final Context context) {
        this.request = request;
        remoteOperation = createRemoteOperation(request, account, context);
    }

    @Nullable
    public RemoteOperation getRemoteOperation() {
        return remoteOperation;
    }

    /**
     * Create the RemoteOperation (to perform file transfer) based on SyncRequest
     * @param request syncRequest for the file
     * @param context App context to be passed to RemoteOperation's contructor
     * @return RemoteOperation for Upload/Download request or null
     */
    private static RemoteOperation createRemoteOperation(final SyncRequest request, final Account account, final Context context) {
        final RemoteOperation operation;
        switch (request.getOperationType()) {
            case UPLOAD:
                final SyncedFileState sfs = request.getSyncedFileState();
                operation = new UploadFileOperation(sfs, account, context);
                break;
            case DOWNLOAD:
                final DownloadRequest downloadRequest = (DownloadRequest) request;
                operation = new DownloadFileOperation(downloadRequest.getRemoteFile(),
                        downloadRequest.getSyncedFileState(),
                        context);
                break;
            default:
                operation = null;
                break;
        }
        return operation;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (obj instanceof SyncRequest) {
            final SyncRequest objRequest = (SyncRequest) obj;
            return (objRequest.equals(this.request));
        }
        return super.equals(obj);
    }

    @NonNull
    public SyncRequest getRequest() {
        return request;
    }
}
