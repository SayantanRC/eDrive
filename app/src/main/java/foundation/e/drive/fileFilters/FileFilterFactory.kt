/*
 * Copyright MURENA SAS 2024
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.drive.fileFilters;

import foundation.e.drive.utils.AppConstants
import foundation.e.drive.utils.FileUtils
import java.io.File
import java.io.FileFilter

/**
 * @author Vincent Bourgmayer
 * @author Fahim Salam Chowdhury
 */
object FileFilterFactory {

    fun buildFileFilter(category: String?): FileFilter {
        return when (category) {
            "Rom settings" -> buildSettingsFilter()
            "Applications" -> buildAppSettingsFilter()
            "media" -> BasicFileFilter()
            else -> buildNoCacheFileFilter()
        }
    }

    private fun buildSettingsFilter(): FileFilter {
        return object : BasicFileFilter() {
            override fun accept(pathname: File?): Boolean {
                return super.accept(pathname) && pathname!!.isFile &&
                        (pathname.name.startsWith("settings_") && pathname.name.endsWith(".xml")
                                || pathname.name == AppConstants.APPLICATIONS_LIST_FILE_NAME)
            }
        }
    }

    private fun buildAppSettingsFilter(): FileFilter {
        return object : BasicFileFilter() {
            override fun accept(pathname: File?): Boolean {
                return (super.accept(pathname)
                        && (pathname!!.isFile || (pathname.isDirectory
                        && !pathname.name.lowercase().contains("cache"))))
            }
        }
    }

    private fun buildNoCacheFileFilter(): FileFilter {
        return object : BasicFileFilter() {
            override fun accept(pathname: File?): Boolean {
                return super.accept(pathname) &&
                        !pathname!!.name.lowercase().contains("cache")
            }
        }
    }
}

private open class BasicFileFilter : FileFilter {
    override fun accept(pathname: File?): Boolean {
        return pathname != null && !pathname.isHidden && FileUtils.isNotPartFile(pathname)
    }
}
