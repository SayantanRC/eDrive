/*
 * Copyright © MURENA SAS 2023.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */
package foundation.e.drive.synchronization

import android.accounts.Account
import android.content.Context

import androidx.work.Worker
import androidx.work.WorkerParameters
import com.owncloud.android.lib.common.OwnCloudClient
import foundation.e.drive.EdriveApplication
import foundation.e.drive.account.AccountUtils
import foundation.e.drive.utils.DavClientProvider
import timber.log.Timber
import java.util.concurrent.Executors
import java.util.concurrent.Future
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicInteger

class SyncWorker(
    context: Context,
    params: WorkerParameters
) : Worker(context, params) {

    companion object {
        const val UNIQUE_WORK_NAME = "syncWorker"
        internal lateinit var pendingTaskCounter: AtomicInteger
        private const val threadAmount = 2
        private val syncManager = SyncProxy as SyncManager
    }

    private var account: Account? = null
    private var ocClient: OwnCloudClient? = null
    private var executor = Executors.newFixedThreadPool(threadAmount)
    private val syncNotifier = SyncProgressNotifier(applicationContext)

    override fun onStopped() {
        Timber.d("SyncWorker has been stopped")
        syncNotifier.cancelAllSyncNotifications()
        executor.shutdownNow()

        super.onStopped()
    }

    override fun doWork(): Result {
        try {
            account = AccountUtils.getAccount(applicationContext)
            if (account == null) {
                Timber.d("Warning : account is null")
                syncManager.startListeningFiles(applicationContext as EdriveApplication)
                return Result.failure()
            }

            ocClient = getOcClient(account!!)
            if (ocClient == null) {
                Timber.d("Warning : ocClient is null")
                syncManager.startListeningFiles(applicationContext as EdriveApplication)
                return Result.failure()
            }
            syncNotifier.createNotificationChannel()

            while (!syncManager.isQueueEmpty()) {
                val requestCount = syncManager.getQueueSize()
                pendingTaskCounter = AtomicInteger(requestCount)
                setForegroundAsync(syncNotifier.createForegroundInfo(requestCount))
                executeRequests()
            }

            syncNotifier.cancelAllSyncNotifications()
            syncManager.startListeningFiles(applicationContext as EdriveApplication)

            DavClientProvider.getInstance().saveAccounts(applicationContext)
        } catch (exception: Exception) {
            Timber.w(exception)
        }

        return Result.success()
    }

    private fun getOcClient(account: Account): OwnCloudClient? {
        return DavClientProvider.getInstance().getClientInstance(
            account,
            applicationContext
        )
    }

    private fun executeRequests() {
        val futureList = arrayListOf<Future<*>>()
        if (executor.isShutdown || executor.isTerminated) {
            executor = Executors.newFixedThreadPool(threadAmount)
        }

        while (!syncManager.isQueueEmpty()) {
            val request = syncManager.pollSyncRequest()?: break
            val task = SyncTask(request, ocClient!!, account!!, applicationContext)
            futureList.add(executor!!.submit(task))
        }

        for (future: Future<*> in futureList) {
            if (this.isStopped) {
                Timber.d("Cancel task before execution")
                future.cancel(true)
            } else {
                future.get()
            }
        }

        executor.shutdown()
        while (!executor.isTerminated && !executor.isShutdown) {
            executor.awaitTermination(30, TimeUnit.SECONDS)
        }
    }
}