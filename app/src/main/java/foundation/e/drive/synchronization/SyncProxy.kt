/*
 * Copyright © MURENA SAS 2023.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */
package foundation.e.drive.synchronization

import android.app.Application
import android.content.Context
import foundation.e.drive.EdriveApplication
import foundation.e.drive.database.FailedSyncPrefsManager
import foundation.e.drive.models.SyncRequest
import foundation.e.drive.models.SyncWrapper
import foundation.e.drive.work.WorkLauncher
import timber.log.Timber
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.ConcurrentLinkedQueue

/**
 * This interface is used to restrict access to a limited set of method
 * @author Vincent Bourgmayer
 */
interface SyncRequestCollector {
    fun queueSyncRequest(request: SyncRequest, context: Context): Boolean
    fun queueSyncRequests(requests: MutableCollection<SyncRequest>, context: Context)
    fun startSynchronization(context: Context)
    fun onPeriodicScanStart(application: Application): Boolean
    fun startListeningFiles(application: Application)
    fun moveToIdle(application: Application)
}

/**
 * This interface is used to restrict access to a limited set of method
 * @author Vincent Bourgmayer
 */
interface SyncManager {
    fun pollSyncRequest(): SyncRequest?
    fun addStartedRequest(fileLocalPath: String, syncWrapper: SyncWrapper)
    fun isQueueEmpty(): Boolean
    fun getQueueSize(): Int
    fun clearRequestQueue()
    fun removeStartedRequest(fileLocalPath: String)
    fun startListeningFiles(application: Application)
}

/**
 * This class goals is to act as a proxy between file's change detection & performing synchronization
 *
 * This object must allow concurrent access between (periodic | instant) file's change detection and synchronization
 * it holds the SyncRequest Queue and a list of running sync
 * @author Vincent Bourgmayer
 */
object SyncProxy: SyncRequestCollector, SyncManager {
    private val syncRequestQueue: ConcurrentLinkedQueue<SyncRequest> = ConcurrentLinkedQueue() //could we use channel instead ?
    private val startedRequests: ConcurrentHashMap<String, SyncWrapper> = ConcurrentHashMap()

    /**
     * Add a SyncRequest into waiting queue if it matches some conditions:
     * - an equivalent sync Request (same file & same operation type) isn't already running
     * - failure limit isn't reach for this file
     *
     * It also remove already existing request for the same file from the waiting queue
     * @param request request to add to waiting queue
     * @param context used to check previous failure of file sync
     */
    override fun queueSyncRequest(request: SyncRequest, context: Context): Boolean {
        if (startedRequests.containsKey(request.syncedFileState.localPath)) {
            Timber.d("A request is already performing for ${request.syncedFileState.name}")
            return false
        }

        if (skipBecauseOfPreviousFail(request, context)) return false

        syncRequestQueue.remove(request)
        syncRequestQueue.add(request)
        return true
    }

    /**
     * Add a collection of SyncRequest into waiting queue if they met some conditions:
     * - an equivalent sync Request (same file & same operation type) isn't already running
     * - failure limit isn't reach for this file
     *
     * It also remove already existing request for the same file from the waiting queue
     * @param requests collections of requests to add
     * @param context used to check previous failure of file sync
     */
    override fun queueSyncRequests(requests: MutableCollection<SyncRequest>, context: Context) {
        for (syncWrapper in startedRequests.values) {
            requests.removeIf { obj: SyncRequest ->
                startedRequests.containsKey(obj.syncedFileState.localPath)
            }
        }

        requests.removeIf { request: SyncRequest? ->
            skipBecauseOfPreviousFail(request!!, context)
        }

        syncRequestQueue.removeAll(requests.toSet())
        syncRequestQueue.addAll(requests)
    }

    /**
     * Try to start synchronization
     *
     * some rules:
     * - Restart FileEventListener if previous state what Periodic Scan
     * - Start synchronization
     * @param context use ApplicationContext
     */
    override fun startSynchronization(context: Context) {
        if (context !is EdriveApplication) {
            Timber.d("Invalid parameter: startSynchronization(context)")
            StateMachine.changeState(SyncState.LISTENING_FILES)
            return
        }

        if (syncRequestQueue.isEmpty()) {
            Timber.d("Request queue is empty")
            startListeningFiles(context)
            return
        }

        val previousSyncState = StateMachine.currentState
        val isStateChanged = StateMachine.changeState(SyncState.SYNCHRONIZING)

        if (!isStateChanged) return

        context.startRecursiveFileObserver()

        if (previousSyncState != SyncState.SYNCHRONIZING) {
            WorkLauncher.getInstance(context).enqueueOneTimeSync();
        }
    }

    /**
     * Called when periodic scan try to start
     * @param application need EdriveApplication instance or will return false
     * @return true if the periodic scan is allowed
     */
    override fun onPeriodicScanStart(application: Application): Boolean {
        if (application !is EdriveApplication) {
            Timber.d("Invalid parameter: : startPeriodicScan(application)")
            return false
        }

        val isStateChanged = StateMachine.changeState(SyncState.PERIODIC_SCAN)
        if (!isStateChanged) return false

        return true
    }

    /**
     * File synchronization is finished
     * restart FileObserver if required
     * @param application need EdriveApplication instance or will exit
     */
    override fun startListeningFiles(application: Application) {
        if (application !is EdriveApplication) {
            Timber.d("Invalid parameter: startListeningFiles(application)")
            return
        }

        StateMachine.changeState(SyncState.LISTENING_FILES)
        application.startRecursiveFileObserver()
    }

    /*
     * called after account logged out
     * update the stateMachine's state & stop recursive fileObserver
     */
    override fun moveToIdle(application: Application) {
        if (application !is EdriveApplication) {
            Timber.d("Invalid parameter: : moveToIdle(application)")
            return
        }

        val isStateChanged = StateMachine.changeState(SyncState.IDLE)
        if (!isStateChanged) {
            Timber.d("failed to change state. moveToIdle")
            return
        }

        application.stopRecursiveFileObserver()
    }


    /**
     * Progressively delay synchronization of a file in case of failure
     * @param request SyncRequest for the given file
     * @return false if file can be sync or true if it must wait
     */
    private fun skipBecauseOfPreviousFail(request: SyncRequest, context: Context): Boolean {
        val failedSyncPref = FailedSyncPrefsManager.getInstance(context)
        val fileStateId = request.syncedFileState.id
        val delay: Long = when (failedSyncPref.getFailureCounterForFile(fileStateId)) {
            0 -> return false
            1 -> 3600 //1h
            2 -> 7200 //2h
            3 -> 18000 // 5h
            else -> return true
        }

        val timeStamp = System.currentTimeMillis() / 1000
        return timeStamp < failedSyncPref.getLastFailureTimeForFile(fileStateId) + delay
    }

    override fun pollSyncRequest(): SyncRequest? {
        return syncRequestQueue.poll()
    }

    override fun addStartedRequest(fileLocalPath: String, syncWrapper: SyncWrapper) {
        startedRequests[fileLocalPath] = syncWrapper
    }

    override fun isQueueEmpty(): Boolean {
        return syncRequestQueue.isEmpty()
    }

    override fun getQueueSize(): Int {
        return syncRequestQueue.size
    }

    override fun clearRequestQueue() {
        syncRequestQueue.clear()
    }

    override fun removeStartedRequest(fileLocalPath: String) {
        if (startedRequests.containsKey(fileLocalPath)) {
            startedRequests.remove(fileLocalPath)
        }
    }
}