/*
 * Copyright © MURENA SAS 2023.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */
package foundation.e.drive.synchronization

import androidx.annotation.VisibleForTesting
import timber.log.Timber

enum class SyncState {
    PERIODIC_SCAN,
    SYNCHRONIZING,
    LISTENING_FILES,
    IDLE
}

/**
 * This class handle Synchronization state of the app
 * It can be synchronizing (transfering file)
 * It can be listening for file event
 * It can be Scanning cloud & device for missed files
 * It can be Idle (i.e: after boot, before restart, after logout)
 * @author Vincent Bourgmayer
 */
object StateMachine {


    var currentState: SyncState = SyncState.IDLE
        @VisibleForTesting
        set

    private var lastUpdateTimeInMs = 0L //todo use it to prevent apps to be blocked in one state

    @Synchronized 
    fun changeState(newState: SyncState): Boolean {
        val previousState = currentState
        val isStateChanged = when (newState) {
            SyncState.PERIODIC_SCAN -> setPeriodicScanState()
            SyncState.SYNCHRONIZING -> setSynchronizing()
            SyncState.LISTENING_FILES -> setListeningFilesState()
            SyncState.IDLE -> setIdleFilesState()
        }

        if (!isStateChanged) {
            Timber.d("Failed to change state to %s, from %s", newState, previousState)
            return false
        }

        Timber.i("Change Sync state to %s from %s", newState, previousState)
        lastUpdateTimeInMs = System.currentTimeMillis()
        return true
    }

    private fun setListeningFilesState(): Boolean {
        currentState = SyncState.LISTENING_FILES
        return true
    }

    private fun setIdleFilesState(): Boolean {
        currentState = SyncState.IDLE
        return true
    }

    private fun setPeriodicScanState(): Boolean {
        if (currentState == SyncState.SYNCHRONIZING) {
            Timber.d("Cannot change state: files sync is running")
            return false
        }

        if (currentState == SyncState.PERIODIC_SCAN) {
            Timber.d("Cannot change state: Periodic scan is already running")
            return false
        }

        currentState = SyncState.PERIODIC_SCAN
        return true
    }

    private fun setSynchronizing(): Boolean {
        currentState = SyncState.SYNCHRONIZING
        return true
    }
}