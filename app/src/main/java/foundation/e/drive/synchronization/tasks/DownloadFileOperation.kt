/*
 * Copyright (C) 2024 MURENA SAS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
package foundation.e.drive.synchronization.tasks

import android.content.Context
import android.media.MediaScannerConnection

import com.owncloud.android.lib.common.OwnCloudClient
import com.owncloud.android.lib.common.operations.RemoteOperation
import com.owncloud.android.lib.common.operations.RemoteOperationResult
import com.owncloud.android.lib.common.operations.RemoteOperationResult.ResultCode
import com.owncloud.android.lib.resources.files.DownloadFileRemoteOperation
import com.owncloud.android.lib.resources.files.model.RemoteFile
import foundation.e.drive.database.DbHelper
import foundation.e.drive.models.SyncedFileState
import foundation.e.drive.utils.AppConstants.CORRUPTED_TIMESTAMP_IN_MILLISECOND
import foundation.e.drive.utils.FileUtils
import timber.log.Timber
import java.io.File
import java.util.stream.IntStream.range

/**
 * @author Vincent Bourgmayer
 * Encapsulates a global download process for a remote file  ⚠️ Ignores
 * deprecation about download with OwncloudClient because NC lib doesn't
 * implement it with NextcloudClient
 */
class DownloadFileOperation(private val remoteFile: RemoteFile,
                            private val fileState: SyncedFileState,
                            private val context: Context): RemoteOperation<Any>() {

    private val previousEtag: String = fileState.lastEtag
    private val targetFilePath = fileState.localPath

    companion object {
        private const val MAX_DOWNLOAD_TRIAL = 3
    }

    @Suppress("OVERRIDE_DEPRECATION")
    override fun run(client: OwnCloudClient): RemoteOperationResult<Any> {
        Timber.v("DownloadFileOperation.run() for ${remoteFile.remotePath} ")
        persistIfNewSyncedFileState()

        val canStartResultCode = checkStartCondition()
        if (canStartResultCode != ResultCode.OK) {
            return RemoteOperationResult(canStartResultCode)
        }

        val tmpDirectoryPath = context.externalCacheDir!!.absolutePath
        val tmpFile = File(tmpDirectoryPath + remoteFile.remotePath)

        val downloadResult = downloadFile(tmpDirectoryPath, tmpFile, client)

        if (downloadResult == ResultCode.OK) {
            val targetFile = File(targetFilePath)
            setRemoteTimestampOnFile(targetFile)

            makeGalleryDetectTheFile(context, targetFilePath)
            updateSyncedFileStateInDb(targetFile.lastModified())
        }

        return RemoteOperationResult(downloadResult)
    }

    private fun checkStartCondition(): ResultCode {
        var resultCode = ResultCode.OK

        if (isFileUpToDate()) {
            Timber.d( "${remoteFile.remotePath} already up-to-date")
            resultCode =  ResultCode.ETAG_UNCHANGED
        } else if (context.externalCacheDir == null) {
            Timber.d( "context.externalCacheDir is null")
            resultCode =  ResultCode.FORBIDDEN
        }

        return resultCode
    }

    /**
     * Makes device be aware of this file instantly
     *
     * @param context  Calling service context
     * @param filePath String containing path the file to update by mediaScanner
     */
    private fun makeGalleryDetectTheFile(context: Context, filePath: String) {
        Timber.v("makeGalleryDetectTheFile( $filePath )" )

        MediaScannerConnection.scanFile(context,
            arrayOf(filePath),
            arrayOf(remoteFile.mimeType)) {
                path, uri -> Timber
                .v("file %s was scanned with success: %s ", path, uri)
        }
    }

    private fun setRemoteTimestampOnFile(targetFile: File) {
        if (remoteFile.modifiedTimestamp < CORRUPTED_TIMESTAMP_IN_MILLISECOND) {
            try {
                targetFile.setLastModified(remoteFile.modifiedTimestamp)
            } catch(securityException: SecurityException) {
                Timber.w(securityException, "Can't update last modified timestamp")
            } catch (illegalArgumentException: IllegalArgumentException) {
                Timber.w(illegalArgumentException,
                    "remoteFile's lastModifiedTimestamp : ${remoteFile.modifiedTimestamp}")
            }
        }
    }

    private fun updateSyncedFileStateInDb(fileLastModifiedTimestamp: Long) {
        fileState.lastModified = fileLastModifiedTimestamp
        fileState.lastEtag = remoteFile.etag!! //already checked in "isFileUpToDate()"

        DbHelper.manageSyncedFileStateDB(fileState, "UPDATE", context)
        // todo : define what to do in this case. Is this test even relevant ?
        // replace the above line with the following
        //
        // val result = DbHelper.manageSyncedFileStateDB(fileState, "UPDATE", context)
        // if (result <= 0) {
        // ....
        // }
    }

    @Suppress("DEPRECATION")
    private fun downloadFile(tmpDirPath: String, tmpFile: File, client: OwnCloudClient): ResultCode {
        var lastResultCode: ResultCode = ResultCode.UNKNOWN_ERROR

        for (trialCount in range(1, MAX_DOWNLOAD_TRIAL)) {
            fileState.lastEtag = previousEtag

            val downloadFileOperation = DownloadFileRemoteOperation(remoteFile.remotePath, tmpDirPath)
            val downloadResult = downloadFileOperation.execute(client)

            lastResultCode = if (downloadResult.isSuccess) {
                isFileValid(tmpFile)
            } else {
                downloadResult.code
            }

            if (lastResultCode != ResultCode.OK) {
                Timber.d("Download trial: $trialCount failed: ${downloadResult.code}, ${downloadResult.logMessage}")
            }

            if (!deserveRetry(lastResultCode)) {
                break
            }
        }

        if (lastResultCode == ResultCode.OK
            && !moveTmpFileToRealLocation(tmpFile)) {
            return ResultCode.FORBIDDEN
        }

        return lastResultCode
    }
    
    private fun deserveRetry(resultCode: ResultCode?): Boolean {
        return (resultCode == null
                || (resultCode != ResultCode.OK && !isNetworkFailure(resultCode)))
    }

    private fun isFileValid(tmpFile: File): ResultCode {
        if (!tmpFile.exists()) {
            Timber.d("Missing downloaded temporary file for ${remoteFile.remotePath}")
            return ResultCode.FILE_NOT_FOUND
        }

        if (!hasExpectedFileSize(tmpFile)) {
            tmpFile.delete()
            return ResultCode.INVALID_OVERWRITE
        }
        return ResultCode.OK
    }

    /**
     * Moves the temporary file downloaded to its final location
     * @param tmpFile Temporary file that has just been downloaded
     * @return true if success, false otherwise
     */
    private fun moveTmpFileToRealLocation(tmpFile: File): Boolean {
        val targetFile = File(targetFilePath)
        val targetDir = targetFile.parentFile ?: return false

        return if (FileUtils.ensureDirectoryExists(targetDir)) {
            FileUtils.copyFile(tmpFile, targetFile)
        } else {
            Timber.d("Failed to move ${tmpFile.absolutePath} to $targetFilePath")
            false
        }
    }

    private fun hasExpectedFileSize(tmpFile: File): Boolean {
        val remoteFileSize = remoteFile.length
        val tmpFileSize = tmpFile.length()
        if (tmpFileSize != remoteFileSize) {
            Timber.d("Local's size $tmpFileSize and remote's size $remoteFileSize " +
                    "of ${remoteFile.remotePath} don't match")
            return false
        }
        return true
    }

    /**
     * Checks if failed download is due to network issue
     */
    private fun isNetworkFailure(resultCode: ResultCode): Boolean {
        return resultCode in listOf(ResultCode.NO_NETWORK_CONNECTION,
            ResultCode.HOST_NOT_AVAILABLE,
            ResultCode.WRONG_CONNECTION)
    }

    private fun isFileUpToDate(): Boolean {
        return !remoteFile.etag.isNullOrEmpty()
            && fileState.lastEtag == remoteFile.etag
            && fileState.lastModified > 0L
    }

    private fun persistIfNewSyncedFileState() {
        if (fileState.id == -1) {
            val id = DbHelper.manageSyncedFileStateDB(fileState, "INSERT", context)
            fileState.id = id
        }
    }
}
