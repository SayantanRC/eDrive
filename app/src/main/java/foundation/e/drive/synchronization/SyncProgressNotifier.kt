/*
 * Copyright © MURENA SAS 2023.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */

package foundation.e.drive.synchronization

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.pm.ServiceInfo
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.work.ForegroundInfo
import foundation.e.drive.R

class SyncProgressNotifier(private val context: Context) {
    companion object {
        const val NOTIFICATION_ID = 2003004
        const val NOTIF_CHANNEL_ID = "syncChannelId"
    }

    private val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

    private fun createNotification(requestCounter: Int): Notification {
        val text = getNotificationText(requestCounter)
        val title = context.getString(R.string.notif_sync_is_running_title)

        return NotificationCompat.Builder(context, NOTIF_CHANNEL_ID)
            .setOngoing(true)
            .setContentTitle(title)
            .setContentText(text)
            .setSmallIcon(R.drawable.ic_synchronization)
            .build()
    }

    fun notifyTaskFinished(requestCounter: Int) {
        notificationManager.notify(NOTIFICATION_ID, createNotification(requestCounter))
    }

    fun cancelAllSyncNotifications() {
        notificationManager.cancel(NOTIFICATION_ID)
    }

    private fun getNotificationText(requestCount: Int): String {
        return context.resources
            .getQuantityString(R.plurals.notif_sync_is_running_txt, requestCount, requestCount)
    }

    fun createNotificationChannel() {
        val channelName = context.getString(R.string.notif_sync_channel_name)
        val importance = NotificationManager.IMPORTANCE_MIN
        val channel = NotificationChannel(NOTIF_CHANNEL_ID, channelName, importance)
        val channelDescription = context.getString(R.string.notif_sync_channel_description)
        channel.description = channelDescription

        notificationManager.createNotificationChannel(channel)
    }

    internal fun createForegroundInfo(requestCount: Int): ForegroundInfo {
        val notification = createNotification(requestCount)

        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            ForegroundInfo(NOTIFICATION_ID, notification, ServiceInfo.FOREGROUND_SERVICE_TYPE_DATA_SYNC)
        } else {
            ForegroundInfo(NOTIFICATION_ID, notification)
        }
    }
}
