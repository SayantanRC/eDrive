/*
 * Copyright © CLEUS SAS 2018-2019.
 * Copyright © ECORP SAS 2022.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */

package foundation.e.drive.database;

import static foundation.e.drive.database.SyncedFolderContract.CATEGORIE_LABEL;
import static foundation.e.drive.database.SyncedFolderContract.ENABLED;
import static foundation.e.drive.database.SyncedFolderContract.IS_MEDIA_TYPE;
import static foundation.e.drive.database.SyncedFolderContract.LAST_ETAG;
import static foundation.e.drive.database.SyncedFolderContract.LOCAL_LAST_MODIFIED;
import static foundation.e.drive.database.SyncedFolderContract.LOCAL_PATH;
import static foundation.e.drive.database.SyncedFolderContract.REMOTE_PATH;
import static foundation.e.drive.database.SyncedFolderContract.SCANLOCAL;
import static foundation.e.drive.database.SyncedFolderContract.SCANREMOTE;
import static foundation.e.drive.database.SyncedFolderContract.TABLE_NAME;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import androidx.annotation.NonNull;

import com.owncloud.android.lib.resources.files.FileUtils;

import java.util.ArrayList;
import java.util.List;

import foundation.e.drive.models.SyncedFolder;
import timber.log.Timber;

/**
 * @author Vincent Bourgmayer
 * Source: https://vogella.developpez.com/tutoriels/android/utilisation-base-donnees-sqlite/
 */
/* package */ class SyncedFolderDAO {
    private SQLiteDatabase mDB;
    private final DbHelper mHelper;
    private static final String[] allColumns = { SyncedFolderContract._ID,
        CATEGORIE_LABEL,
        LOCAL_PATH,
        REMOTE_PATH,
        LAST_ETAG,
        LOCAL_LAST_MODIFIED,
        SCANLOCAL,
        SCANREMOTE,
        ENABLED,
        IS_MEDIA_TYPE
    };

    /* package */ SyncedFolderDAO(@NonNull Context context){
        Timber.tag(SyncedFolderDAO.class.getSimpleName());
        this.mHelper = new DbHelper(context);
    }

    /**
     * Open Database connexion
     * @param writeMod Connexion mod. If set to true, database is open in writable mod, else it is opened in read mod.
     * @throws SQLException SqlException
     */
    /* package */ void open(Boolean writeMod) throws SQLException {
        mDB = (writeMod)? mHelper.getWritableDatabase() : mHelper.getReadableDatabase();
    }

    /**
     * Close Database connexion
     */
    /* package */ void close() {
        mHelper.close();
    }

    /**
     * Convert a syncedFolder to ContentValues
     * @param syncedFolder SyncedFolder to convert
     * @return ContentValues object
     */
    private ContentValues toContentValues(SyncedFolder syncedFolder){
        final ContentValues values = new ContentValues();
        values.put( CATEGORIE_LABEL, syncedFolder.getLibelle() );
        values.put( LOCAL_PATH, syncedFolder.getLocalFolder() );
        values.put( REMOTE_PATH, syncedFolder.getRemoteFolder() );
        values.put( LAST_ETAG, syncedFolder.getLastEtag() );
        values.put( LOCAL_LAST_MODIFIED, syncedFolder.getLastModified() );
        values.put( SCANLOCAL, syncedFolder.isScanLocal() ? 1 : 0 );
        values.put( SCANREMOTE, syncedFolder.isScanRemote() ? 1 : 0 );
        values.put( ENABLED, syncedFolder.isEnabled() ? 1 : 0 );
        values.put( IS_MEDIA_TYPE, syncedFolder.isMediaType() ? 1 : 0 );
        return values;
    }

    /**
    * Insert new syncedFolder
    * @param syncedFolder element to register
    * @return Id of the last inserted value
    */
    /* package */ long insert(SyncedFolder syncedFolder){
        return mDB.insertWithOnConflict(TABLE_NAME,
                null,
                toContentValues(syncedFolder), SQLiteDatabase.CONFLICT_IGNORE);
    }

    /**
     * Delete all syncedFolder with the same categorie's label
     * @param id syncedFolder's id
     */
    /* package */ int delete(long id) {
        return mDB.delete(TABLE_NAME, SyncedFolderContract._ID
                + " = " + id, null);
    }

    /**
     * Update a specific syncedFolder
     * @param syncedFolder syncedFolder to update
     * @return Number of row affected
     */
    /* package */ int update(SyncedFolder syncedFolder){
       return mDB.update(TABLE_NAME,
                toContentValues(syncedFolder),
                SyncedFolderContract._ID+" = "+ syncedFolder.getId(),
                null);
    }

    /**
    * Get list of syncedFolder
    * @param selection if true, return all enabled syncedFolder, if false return all disabled syncedFolder
    * todo rewrite this method and at least its description because it's not clear...
    * @return List of all syncedFolder
    */
    /* package */ List<SyncedFolder> getSyncedFolderList(String selection, String[] args) {
        final List<SyncedFolder> syncedFolders = new ArrayList<>();
        final Cursor cursor = mDB.query(TABLE_NAME, allColumns, selection,args, null, null,
                null);

        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            final SyncedFolder syncedFolder = cursorToSyncedFolder(cursor);
            if (syncedFolder == null ) {
                Timber.v("getSyncedFolderList : current Folder ( %s ) is null", syncedFolder.getLocalFolder());
            } else {
                syncedFolders.add(syncedFolder);
            }
            cursor.moveToNext();
        }

        cursor.close();
        return syncedFolders;
    }

    /**
     * reduce to 1 the value stored in lastModified of this syncFolder . The goal is to allow app to resync this folder next time
     * @param syncFolderID id of the synced folder to update
     * @return number of row affected
     */
    /* package */ int reduceLastModifiedValue(int syncFolderID) {
        final ContentValues values = new ContentValues();
        values.put( LOCAL_LAST_MODIFIED, 1 );

        return mDB.update(TABLE_NAME, values,
                    SyncedFolderContract._ID+" = "+ syncFolderID,
                    null);
    }

    /* package */ SyncedFolder getSyncedFolderByLocalPath(String localPath){
        final String whereClause = LOCAL_PATH + " LIKE ?";
        final String[] whereValue = new String[] { localPath + FileUtils.PATH_SEPARATOR };
        final Cursor cursor = mDB.query(TABLE_NAME, allColumns, whereClause, whereValue, null, null, null);
        cursor.moveToFirst();
        SyncedFolder result = null;
        if (!cursor.isAfterLast()) {
            result = cursorToSyncedFolder(cursor);
        }
        cursor.close();
        return result;
    }

    /**
    * Create a syncedFolder object from data in cursor.
    * @param cursor cursor containing data to build syncedFolder
    * @return syncedFolder instance build with data in the cursor
    */
    private SyncedFolder cursorToSyncedFolder(Cursor cursor) {
        return new SyncedFolder(cursor.getString(1), //libelle
                cursor.getString(2), //localFOlder
                cursor.getString(3), //remoteFolder
                ( cursor.getInt(6) == 1 ),//scanLocal
                ( cursor.getInt(7) == 1 ),//scanRemote
                ( cursor.getInt(8) == 1 ), //isEnabled
                ( cursor.getInt(9) == 1 ) //is Media Type
                )//active
            .setId(cursor.getInt(0) )
            .setLastEtag(cursor.getString( 4 ) )
            .setLastModified(cursor.getLong( 5 ) );
    }
}
