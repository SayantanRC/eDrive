/*
 * Copyright © ECORP SAS 2022.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */

package foundation.e.drive.database;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;

/**
 * @author vincent Bourgmayer
 */
public class FailedSyncPrefsManager {
    public final static String PREF_NAME = "failed_transfer";
    private final static String FAILURE_COUNTER_KEY = "C";
    private final static String FAILURE_TIME_KEY = "T";

    private static FailedSyncPrefsManager instance = null;

    private SharedPreferences preferences;
    private FailedSyncPrefsManager() {};

    @NonNull
    public static FailedSyncPrefsManager getInstance(@NonNull Context context) {
        if (instance == null) {
            instance = new FailedSyncPrefsManager();
            instance.preferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        }

        return instance;
    }


    public int getFailureCounterForFile(int fileStateId) {
        return preferences.getInt(getFailureCounterKey(fileStateId), 0);
    }

    public long getLastFailureTimeForFile(int fileStateId) {
        return preferences.getLong(getFailureTimeKey(fileStateId), 0);
    }

    public void saveFileSyncFailure(int fileStateId) {
        final int failure_counter = getFailureCounterForFile(fileStateId)+1;
        final long timestamp = System.currentTimeMillis() / 1000;

        preferences.edit()
            .putInt(getFailureCounterKey(fileStateId), failure_counter)
            .putLong(getFailureTimeKey(fileStateId), timestamp)
            .apply();
    }

    public void clearPreferences() {
        preferences.edit().clear().apply();
    }

    public void removeDataForFile(int fileStateId) {
        preferences.edit()
                .remove(getFailureCounterKey(fileStateId))
                .remove(getFailureTimeKey(fileStateId))
                .apply();
    }

    @NonNull
    private String getFailureCounterKey(int fileStateId) {
        return fileStateId + FAILURE_COUNTER_KEY;
    }

    @NonNull
    private String getFailureTimeKey(int fileStateId) {
        return fileStateId + FAILURE_TIME_KEY;
    }
}
