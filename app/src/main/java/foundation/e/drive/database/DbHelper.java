/*
 * Copyright © CLEUS SAS 2018-2019.
 * Copyright © ECORP SAS 2022.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */

package foundation.e.drive.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import foundation.e.drive.BuildConfig;
import foundation.e.drive.models.SyncedFolder;
import foundation.e.drive.models.SyncedFileState;
import timber.log.Timber;

/**
 * @author Vincent Bourgmayer
 * source: https://developer.android.com/training/data-storage/sqlite.html#java
 * https://vogella.developpez.com/tutoriels/android/utilisation-base-donnees-sqlite/
 */
public final class DbHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 20; //16/03/2022
    public static final String DATABASE_NAME = "eelo_drive.db";

    /**
     * Constructor of the helper
     */
    public DbHelper(@NonNull Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    /**
     * Function to create a new DB.
     * @param db SQLiteDatabase object
     */
    @Override
    public void onCreate(@NonNull SQLiteDatabase db) {
        db.execSQL(SyncedFolderContract.SQL_CREATE_TABLE_SYNCEDFOLDER);
        db.execSQL(SyncedFileStateContract.SQL_CREATE_TABLE_SYNCEDFILESTATE);
    }

    /**
     * Function to upgrade a DB.
     * @param db SQLiteDatabase object
     * @param oldVersion Version number of the db to delete
     * @param newVersion Version number of the db to create
     */
    @Override
    public void onUpgrade(@NonNull SQLiteDatabase db, int oldVersion, int newVersion) {
        Timber.i("onUpgrade(db, %s, %s)",oldVersion, newVersion);
        try {
            if (oldVersion < 19) {
                db.execSQL(SyncedFolderContract.UPDATE_TABLE_TO_VERSION_19);
                db.execSQL(SyncedFileStateContract.UPDATE_TABLE_TO_VERSION_19);
                db.execSQL(SyncedFileStateContract.UPDATE_MEDIA_DATA_TO_VERSION_19);
                db.execSQL(SyncedFileStateContract.UPDATE_SETTINGS_DATA_TO_VERSION_19);
                db.execSQL(SyncedFolderContract.UPDATE_MEDIA_DATA_TO_VERSION_19);
                db.execSQL(SyncedFolderContract.UPDATE_SETTINGS_DATA_TO_VERSION_19);
            }
            if (oldVersion < 20) {
                db.execSQL(SyncedFileStateContract.UPDATE_TABLE_TO_VERSION_20);
                db.execSQL(SyncedFileStateContract.UPDATE_MEDIA_DATA_TO_VERSION_20);
                db.execSQL(SyncedFileStateContract.UPDATE_SETTINGS_DATA_TO_VERSION_20);
            }
        } catch(Exception exception) {
            Timber.e(exception);
        }
    }

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    @Nullable
    public static SyncedFileStateDAO openSyncedFileStateDAO(@NonNull Context context, boolean writeMod) {
        final SyncedFileStateDAO dao = new SyncedFileStateDAO(context);
        try {
            dao.open(writeMod);
        } catch (Exception exception) { //todo catch SQLiteException instead or throw it and handle it in higher stack
            Timber.e(exception);
            return null;
        }
        return dao;
    }

    @Nullable
    private static SyncedFolderDAO openSyncedFolderDAO(@NonNull Context context, boolean writeMod){
        final SyncedFolderDAO dao = new SyncedFolderDAO(context);
        try {
            dao.open(writeMod);
        } catch(Exception exception) { //todo catch SQLiteException instead or throw it and handle it in higher stack
            Timber.e(exception);
            return null;
        }
        return dao;
    }

    /**
     * Insert, update or delete a new SyncedFileState in DB.
     * @param syncedFileState SyncedFileState to update, insert or delete
     * @param action What to do with synced syncedFileState; possible values or "UPDATE", "INSERT", "DELETE"
     * @return int If it is  an update or a delete then the function return the number of row affected
     * but if it is an insert then it return the id of the inserted data.
     */
    public static int manageSyncedFileStateDB(@NonNull SyncedFileState syncedFileState, @NonNull String action, @NonNull Context context){
        Timber.d("manageSyncedFileStateDB( %s, %s )", syncedFileState.getName(), action);
        int result = -1;
        final SyncedFileStateDAO dao = openSyncedFileStateDAO(context, true);
        if (dao != null) {
            if (action.equals( "UPDATE")) {
                result = dao.update(syncedFileState);
            } else if (action.equals("INSERT")) {
                result = (int) dao.insert( syncedFileState );
            }
            else if (action.equals("DELETE")) {
                result = dao.delete(syncedFileState.getId());
            }
            dao.close();
        }
        return result;
    }

    /**
     * get one synced file filtered on remote or local path
     * @param context calling context to get DAO
     * @param Path full path to filter syncedFileState.
     * @param useLocalPath True if path is a local path, if it's false use remote path
     * @return instance of SyncedFileState or null if it can't open DB
     */
    @Nullable
    public static SyncedFileState loadSyncedFile(@NonNull Context context, @NonNull String Path, boolean useLocalPath) {
        final SyncedFileStateDAO dao = openSyncedFileStateDAO(context, false);

        if (dao == null) {
            return null;
        }
        SyncedFileState syncedFileState = dao.getByPath(Path, useLocalPath);
        dao.close();
        return syncedFileState;
    }

    /**
     * get many synced file filtered on remote or local path
     * @param context calling context to get DAO
     * @param ids List<String> id of SyncedFolders

     * @return empty list if DB opening failed, either return the list of SyncedFileState
     */
    @NonNull
    public static List<SyncedFileState> getSyncedFileStatesByFolders(@NonNull Context context, @NonNull List<Long> ids) {
        final SyncedFileStateDAO dao = openSyncedFileStateDAO(context, false);
        if (dao == null) {
            return new ArrayList<>();
        }
        final List<SyncedFileState> result = dao.getBySyncedFolderIds(ids);
        dao.close();
        return result;
    }

    /**
     * Update array of SyncedFolder's row in DB
     * @param syncedFolders SyncedFolder array to update
     * @param context appContext
     * @return number of affected row. Return -1 if failed
     */
    public static int updateSyncedFolders(@NonNull List<SyncedFolder> syncedFolders, @NonNull Context context){
        int result = -1;

        final SyncedFolderDAO dao = openSyncedFolderDAO(context, true);
        if (dao == null) {
            return result;
        }

        final int SyncedFoldersSize = syncedFolders.size();
        for(int i = 0; i < SyncedFoldersSize; ++i ){
            result += dao.update(syncedFolders.get(i));
        }
        dao.close();
        return result;
    }


    public static int updateSyncedFolder(@NonNull SyncedFolder syncedFolder, @NonNull Context context) {
        int result = -1;

        final SyncedFolderDAO dao = openSyncedFolderDAO(context, true);
        if (dao == null) {
            return result;
        }
        result = dao.update(syncedFolder);
        dao.close();
        return result;
    }

    /**
     * Load SyncedFolder's from DB
     * @param context app or service activity
     * @return List<Folder> a list of SyncedFolder from DB
     */
    @NonNull
    public static List<SyncedFolder> getAllSyncedFolders(@NonNull Context context) {
        final SyncedFolderDAO dao = openSyncedFolderDAO(context, false);
        List<SyncedFolder> mSyncedFolder = new ArrayList<SyncedFolder>();
        if (dao == null) {
            return mSyncedFolder;
        } else {
            mSyncedFolder = dao.getSyncedFolderList(SyncedFolderContract.ENABLED+" = 1 ", null );
            dao.close();
            return mSyncedFolder;
        }
    }

    @NonNull
    public static List<SyncedFolder> getSyncedFolderList(@NonNull Context context, boolean isMediaType) {
        final SyncedFolderDAO dao = openSyncedFolderDAO(context, false);
        List<SyncedFolder> mSyncedFolder = new ArrayList<>();
        if (dao == null) {
            return mSyncedFolder;
        } else {
            mSyncedFolder = dao.getSyncedFolderList(SyncedFolderContract.ENABLED+" = 1 AND "
                +SyncedFolderContract.IS_MEDIA_TYPE+" = ?", new String[] {(isMediaType)? "1":"0"} );
            dao.close();
            return mSyncedFolder;
        }
    }

    /**
     * Save new SyncedFolder into DB
     * @param mSyncedFolder The SyncedFolder to persist
     * @param context The context of the app
     * @return the id of the  Synced folder or -1 if it hasn't been inserted
     */
    public static long insertSyncedFolder(@NonNull SyncedFolder mSyncedFolder, @NonNull Context context){
        final SyncedFolderDAO dao = openSyncedFolderDAO(context, true);
        if (dao == null) {
            return -2;
        }
        final long id = dao.insert(mSyncedFolder);
        dao.close();
        return id;
    }

    @Nullable
    public static SyncedFolder getSyncedFolderByLocalPath(@NonNull String localPath, @NonNull Context context){
        final SyncedFolderDAO dao = openSyncedFolderDAO(context, true);
        if (dao == null) {
            return null;
        }
        final SyncedFolder syncedFolder = dao.getSyncedFolderByLocalPath(localPath);
        dao.close();
        return syncedFolder;
    }

    /**
     * Set the lastModified value of SyncedFolder to 1.
     * The goal is to force to rescan it next time.
     * @param syncedFolderId syncedFolder to update
     * @param context context of the app
     * @return number of row affected
     */
    public static int forceFoldertoBeRescan(int syncedFolderId, @NonNull Context context){
        final SyncedFolderDAO dao = openSyncedFolderDAO(context, true);
        if (dao == null) {
            return -1;
        }

        final int result = dao.reduceLastModifiedValue(syncedFolderId);
        dao.close();
        return result;
    }

    /**
     * Look if there is some known file that need to be uploaded for the given syncedFolder
     * @param syncedFolderID id of the syncedFolder
     * @param context context
     * @return true if there is at least one file that need to be uploaded
     */
    public static boolean syncedFolderHasContentToUpload(long syncedFolderID, @NonNull Context context) {
        boolean result = false;
        try {
            final SyncedFileStateDAO dao = openSyncedFileStateDAO(context, false);
            result = dao.countFileWaitingForUploadForSyncedFolder(syncedFolderID) > 0;
            dao.close();
        } catch (SQLiteException exception) {
            Timber.e(exception);
        }
        return result;
    }

    /**
     * Look if there us some known file that need to be downloaded for the given syncedFolder
     * @param syncedFolderId SyncedFolderId
     * @param context context
     * @return true if there is at least one file to download
     */
    public static boolean syncedFolderHasContentToDownload(int syncedFolderId, @NonNull Context context) {
        boolean result = false;
        try {
            final SyncedFileStateDAO dao = openSyncedFileStateDAO(context, false);
            result = dao.countFileWaitingForDownloadForSyncedFolder(syncedFolderId) > 0;
            dao.close();
        } catch (SQLiteException exception) {
            Timber.e(exception);
        }
        return result;
    }

    /**
     * Copy database file into user accessible directory for debuging purpose
     * @return path to the dump or null if failure
     */
    public static void dumpDatabase(@NonNull Context context) {
        final File database = context.getDatabasePath(DbHelper.DATABASE_NAME);
        final File dstDir = context.getExternalFilesDir("DataBaseDump");
        if (!dstDir.exists()) dstDir.mkdir();
        final File dbDump = new File(dstDir, "dump-" + BuildConfig.VERSION_NAME + "-" + System.currentTimeMillis() + ".db");

        if (database.exists()) {
            try (final InputStream src = new FileInputStream(database);
                 final OutputStream dst = new FileOutputStream(dbDump)) {
                byte[] buffer = new byte[1024];
                int read;
                while ((read = src.read(buffer)) != -1) {
                    dst.write(buffer, 0, read);
                }
                Timber.d("Database has been dump at %s", dbDump.getAbsolutePath());
                return;
            } catch (IOException exception) {
                Timber.d(exception);
            }
        }
    }

    /**
     * Remove syncedFileState for hidden file inserted in previous version
     * @param context
     * @throws SQLiteException if database can't be open
     */
    public static void cleanSyncedFileStateTableAfterUpdate(@NonNull Context context) throws SQLiteException {
        final SyncedFileStateDAO dao = openSyncedFileStateDAO(context, true);
        dao.deleteHiddenFileStates();
        dao.close();
    }
}
