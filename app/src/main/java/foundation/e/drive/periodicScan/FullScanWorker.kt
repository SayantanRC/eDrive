/*
 * Copyright © MURENA SAS 2023.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */
package foundation.e.drive.periodicScan

import android.accounts.Account
import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.owncloud.android.lib.common.operations.RemoteOperationResult
import com.owncloud.android.lib.resources.files.model.RemoteFile
import foundation.e.drive.account.AccountUtils
import foundation.e.drive.database.DbHelper
import foundation.e.drive.models.SyncRequest
import foundation.e.drive.models.SyncedFolder
import foundation.e.drive.periodicScan.contentScanner.LocalContentScanner
import foundation.e.drive.periodicScan.contentScanner.LocalFileLister
import foundation.e.drive.periodicScan.contentScanner.RemoteContentScanner
import foundation.e.drive.synchronization.SyncProxy
import foundation.e.drive.synchronization.SyncRequestCollector
import foundation.e.drive.utils.AppConstants
import foundation.e.drive.utils.AppConstants.KEY_LAST_SCAN_TIME
import foundation.e.drive.utils.CommonUtils
import foundation.e.drive.utils.DavClientProvider
import timber.log.Timber

class FullScanWorker(private val context: Context, private val workerParams: WorkerParameters) :
    Worker(context, workerParams)
{

    private val syncRequests = HashMap<Int, SyncRequest>()

    companion object {
        private const val SYNC_MINIMUM_DELAY = 900000 // min delay between two run in ms.
        const val ACTION_FORCED_SYNC_KEY = "forced_sync"
        const val UNIQUE_WORK_NAME = "fullScan"
    }

    override fun doWork(): Result {
        try {
            val requestCollector: SyncRequestCollector = SyncProxy

            val prefs = context.getSharedPreferences(
                AppConstants.SHARED_PREFERENCE_NAME,
                Context.MODE_PRIVATE
            )
            val account = AccountUtils.getAccount(applicationContext)

            val startAllowed = checkStartConditions(account, prefs, requestCollector)
            if (!startAllowed) {
                Timber.d("Start Periodic Scan is not allowed")
                return Result.failure()
            }

            val syncFolders = loadSyncedFolders(account!!)
            if (syncFolders.isEmpty()) {
                requestCollector.startListeningFiles(applicationContext as Application)
                return Result.success()
            }

            val remoteSyncRequests = scanRemoteFiles(account, syncFolders.toMutableList())
            syncRequests.putAll(remoteSyncRequests)

            Timber.d("${remoteSyncRequests.size} request collected from cloud")

            val localSyncRequests = scanLocalFiles(syncFolders.toMutableList())
            syncRequests.putAll(localSyncRequests)
            Timber.d("${localSyncRequests.size} request collected from device")

            prefs.edit()
                .putLong(KEY_LAST_SCAN_TIME, System.currentTimeMillis())
                .apply();

            if (syncRequests.isEmpty()) {
                Timber.d("Nothing to sync")
                requestCollector.startListeningFiles(applicationContext as Application)
                return Result.success()
            }

            Timber.d("${syncRequests.size} files to sync")
            requestCollector.queueSyncRequests(syncRequests.values, context)
            requestCollector.startSynchronization(applicationContext)
            return Result.success()

        } catch (exception: Exception) {
            Timber.e(exception)
            return Result.failure()
        }
    }

    private fun checkStartConditions(account: Account?, prefs : SharedPreferences, requestCollector: SyncRequestCollector): Boolean {
        Timber.d("FullScanWorker.checkStartConditions()")

        if (account == null) return false

        if (!isSetupDone(prefs)) return false   

        if (isFileSyncDisabled(account)) return false //@todo could be replaced by checking list of sync folders not empty after loading them

        val forcedSync = workerParams.inputData.getBoolean(ACTION_FORCED_SYNC_KEY, false)

        if (!forcedSync && !isMinimumDelayRespected(prefs)) return false

        if (!isConnectedToAllowedNetwork(account)) return false

        if (!requestCollector.onPeriodicScanStart(applicationContext as Application)) return false

        return true
    }

    /**
     * indicate if minimum delay between two periodic scan is respected
     */
    private fun isMinimumDelayRespected(prefs: SharedPreferences): Boolean {
        val lastSyncTime = prefs.getLong(KEY_LAST_SCAN_TIME, 0L)
        val currentTime = System.currentTimeMillis()
        val deltaTime = currentTime - lastSyncTime

        return deltaTime >= SYNC_MINIMUM_DELAY
    }

    /**
     * Indicates if user has disable both media and settings files synchronization
     */
    private fun isFileSyncDisabled(account: Account) : Boolean {
        return !CommonUtils.isMediaSyncEnabled(account) && !CommonUtils.isSettingsSyncEnabled(account)
    }

    /**
     * Indicates if account has been set up and Remote root folders created
     */
    private fun isSetupDone(prefs: SharedPreferences): Boolean {
        return prefs.getBoolean(AppConstants.SETUP_COMPLETED, false)
    }

    /**
     * Check if device is connected to an allowed network type for synchronization
     * note: because user can disable sync over metered network
     */
    private fun isConnectedToAllowedNetwork(account: Account): Boolean {
        val isMeteredNetworkAllowed = CommonUtils.isMeteredNetworkAllowed(account)
        return CommonUtils.haveNetworkConnection(context, isMeteredNetworkAllowed)
    }

    /**
     * Fetch SyncedFolders list from Database
     */
    private fun loadSyncedFolders(account: Account): List<SyncedFolder> {
        val isMediaSyncEnabled = CommonUtils.isMediaSyncEnabled(account)
        val isSettingsSyncEnabled = CommonUtils.isSettingsSyncEnabled(account)

        return if (isMediaSyncEnabled && isSettingsSyncEnabled) DbHelper.getAllSyncedFolders(context)
        else if (isMediaSyncEnabled) DbHelper.getSyncedFolderList(context, true)
        else if (isSettingsSyncEnabled) DbHelper.getSyncedFolderList(context, false)
        else ArrayList()
    }

    /**
     * generate SyncRequest for files on the cloud
     */
    private fun scanRemoteFiles(account: Account, syncedFolders: List<SyncedFolder>): HashMap<Int, SyncRequest> {
        val ocClient = DavClientProvider.getInstance().getClientInstance(account, context)?: return HashMap()
        val listRemoteFilesOperation =
            ListFileRemoteOperation(
                syncedFolders,
                context
            )

        try {
            @Suppress("DEPRECATION")
            val result = listRemoteFilesOperation.execute(ocClient) as RemoteOperationResult<ArrayList<RemoteFile>>
            if (!result.isSuccess) {
                Timber.d("Fails to check remote files")
            }
            val remoteFiles = result.resultData as List<RemoteFile>

            val syncedFoldersIds = listRemoteFilesOperation.syncedFoldersId
            val syncedFileStates = DbHelper.getSyncedFileStatesByFolders(context, syncedFoldersIds)

            DavClientProvider.getInstance().saveAccounts(applicationContext)

            if (remoteFiles.isNotEmpty() || syncedFileStates.isNotEmpty()) {
                val scanner = RemoteContentScanner(context, syncedFolders)
                return scanner.scanContent(remoteFiles, syncedFileStates)
            }

        } catch(exception: IllegalArgumentException) {
            Timber.e(exception)
        }
        return HashMap()
    }

    /**
     * generate SyncRequest for files on the device
     */
    private fun scanLocalFiles(syncedFolders: List<SyncedFolder>): HashMap<Int, SyncRequest> {
        val localFileLister = LocalFileLister(syncedFolders)
        val isContentToScan = localFileLister.listContentToScan(context)

        if (!isContentToScan) {
            return HashMap()
        }

        val localFiles = localFileLister.contentToScan
        val syncedFoldersIds = localFileLister.syncedFoldersId
        val syncedFileStates = DbHelper.getSyncedFileStatesByFolders(context, syncedFoldersIds)

        if (localFiles.isNotEmpty() || syncedFileStates.isNotEmpty()) {
            val scanner = LocalContentScanner(context, syncedFolders)
            return scanner.scanContent(localFiles, syncedFileStates)
        }
        return HashMap()
    }
}