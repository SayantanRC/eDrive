/*
 * Copyright © ECORP SAS 2022.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */
package foundation.e.drive.periodicScan.contentScanner;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.owncloud.android.lib.resources.files.FileUtils;

import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;

import foundation.e.drive.models.SyncRequest;
import foundation.e.drive.models.SyncedFileState;
import foundation.e.drive.models.SyncedFileStateKt;
import foundation.e.drive.models.SyncedFolder;

/**
 * Class encapsulating common code and references for RemoteContentScanner & LocalContentScanner
 * The goal is to generate SyncRequest for file that need to be synchronized
 * @author vincent  Bourgmayer
 */
public abstract class AbstractContentScanner<T> {
    protected final Context context;
    protected final HashMap<Integer, SyncRequest> syncRequests;
    protected final List<SyncedFolder> syncedFolders;

    /**
     * @param context Context used to access Database, etc.
     * @param syncedFolders List of SyncedFolders
     */
    protected AbstractContentScanner(@NonNull Context context, @NonNull List<SyncedFolder> syncedFolders) {
        syncRequests = new HashMap<>();
        this.context = context;
        this.syncedFolders = syncedFolders;
    }

    /**
     * Method to look for file to synchronize into a given list of files.
     * The main logic is fixed but some part depend of the implementation
     * @param fileList List of file, instances of T class
     * @param fileStates SyncedFileState representing already known files
     * @return HashMap<Integer, SyncRequest> with SynceFileState ID as the key and SyncRequest instance as the value
     */
    @NonNull
    public final HashMap<Integer, SyncRequest> scanContent(@NonNull List<T> fileList, @NonNull List<SyncedFileState> fileStates) {
        fileStates.removeIf(p -> p.isMediaType() && p.getName().startsWith(".")); //ignore hidden medias from db

        FileLoop: for (final T file : fileList) {
            final ListIterator<SyncedFileState> iterator = fileStates.listIterator();

            while (iterator.hasNext()) {
                final SyncedFileState fileState = iterator.next();
                if (isFileMatchingSyncedFileState(file, fileState)) {
                    onKnownFileFound(file, fileState);
                    iterator.remove();
                    continue FileLoop;
                }
            }
            onNewFileFound(file);
        }
        
        for (SyncedFileState remainingFileState : fileStates) {
            if (remainingFileState.getScanScope() == SyncedFileStateKt.DO_NOT_SCAN) {
                continue;
            }
            onMissingFile(remainingFileState);
        }
        return syncRequests;
    }

    /**
     * Obtain the SyncedFolder for parent of file denoted by the given path
     * The method to obtain syncedFolder depend of implementation of ContentScanner
     * @param filePath path of the file for which we want a syncFolder
     * @return SyncedFolder instance if found or null
     */
    @Nullable
    protected SyncedFolder getParentSyncedFolder(@NonNull String filePath) {
        final String dirPath = filePath.substring(0, filePath.lastIndexOf(FileUtils.PATH_SEPARATOR) + 1);

        for (SyncedFolder syncedFolder : syncedFolders) {
            if (isSyncedFolderParentOfFile(syncedFolder, dirPath)) {
                return syncedFolder;
            }
        }
        return null;
    }

    /**
     * When a file doesn't exist anymore we remove it from device/cloud (depending of implementation) & from Database
     * @param fileState SyncedFileState for which we lack remote file
     */
    protected abstract void onMissingFile(@NonNull SyncedFileState fileState);

    /**
     * A new file has been found
     * - Create SyncedFileState for it and insert in DB
     * - Create a syncRequest for it
     * @param file The new remote file
     */
    protected abstract void onNewFileFound(@NonNull T file);

    /**
     * A known file has been found
     * Check what to do: ignore, update Database with missing input or create a new syncRequest
     * @param file The remote file
     * @param fileState file's latest known state
     */
    protected abstract void onKnownFileFound(@NonNull T file, @NonNull SyncedFileState fileState);
    protected abstract boolean isFileMatchingSyncedFileState(@NonNull T file, @NonNull SyncedFileState fileState);
    protected abstract boolean isSyncedFolderParentOfFile(@NonNull SyncedFolder syncedFolder, @NonNull String dirPath);
}