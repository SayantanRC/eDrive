/*
 * Copyright © MURENA SAS 2022-2023.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */
package foundation.e.drive.periodicScan.contentScanner;

import static foundation.e.drive.models.SyncRequest.Type.DISABLE_SYNCING;
import static foundation.e.drive.models.SyncedFileStateKt.DO_NOT_SCAN;
import static foundation.e.drive.models.SyncedFileStateKt.SCAN_ON_CLOUD;
import static foundation.e.drive.models.SyncedFileStateKt.SCAN_ON_DEVICE;
import static foundation.e.drive.periodicScan.contentScanner.FileDiffUtils.getActionForFileDiff;

import android.content.Context;

import androidx.annotation.NonNull;

import com.owncloud.android.lib.resources.files.model.RemoteFile;

import java.util.List;

import foundation.e.drive.database.DbHelper;
import foundation.e.drive.models.DownloadRequest;
import foundation.e.drive.models.SyncRequest;
import foundation.e.drive.models.SyncedFileState;
import foundation.e.drive.models.SyncedFolder;
import foundation.e.drive.utils.FileUtils;
import timber.log.Timber;

/**
 * Implementation of AbstractContentScanner for RemoteFile
 * @author vincent Bourgmayer
 */
public class RemoteContentScanner extends AbstractContentScanner<RemoteFile> {

    /**
     * @param context Context used to access Database, etc.
     */
    public RemoteContentScanner(@NonNull Context context, @NonNull List<SyncedFolder> syncedFolders) {
        super(context, syncedFolders);
        Timber.tag(RemoteContentScanner.class.getSimpleName());
    }

    @Override
    protected void onKnownFileFound(@NonNull RemoteFile file, @NonNull SyncedFileState fileState) {
        if (fileState.getScanScope() == DO_NOT_SCAN) return;
        final FileDiffUtils.Action action = getActionForFileDiff(file, fileState);

        switch (action) {
            case Upload:
                addUploadRequest(fileState);
                break;
            case Download:
                addDownloadRequest(fileState, file);
                break;
            case UpdateDB:
                addUpdateDBRequest(fileState, file);
                break;
        }
    }

    @Override
    protected void onNewFileFound(@NonNull RemoteFile file) {
        final String remoteFilePath = file.getRemotePath();
        final SyncedFolder parentDir = getParentSyncedFolder(remoteFilePath);
        if (parentDir == null) return;

        final String fileName = FileUtils.getFileNameFromPath(remoteFilePath);
        if (fileName == null) return;

        int scanScope = DO_NOT_SCAN;
        if (parentDir.isEnabled()) {
            if (parentDir.isScanRemote()) scanScope += SCAN_ON_CLOUD;
            if (parentDir.isScanLocal()) scanScope += SCAN_ON_DEVICE;
        }

        final SyncedFileState newFileState = new SyncedFileState(-1, fileName, parentDir.getLocalFolder() + fileName, remoteFilePath, file.getEtag(), 0, parentDir.getId(), parentDir.isMediaType(), scanScope);

        final int storedId = DbHelper.manageSyncedFileStateDB(newFileState, "INSERT", context);
        if (storedId > 0) {
            newFileState.setId(storedId);
            Timber.d("Add downloadSyncRequest for new remote file: %s", remoteFilePath);
            this.syncRequests.put(storedId, new DownloadRequest(file, newFileState));
        }
    }

    @Override
    protected void onMissingFile(@NonNull SyncedFileState fileState) {
        if (!fileState.hasBeenSynchronizedOnce()) {
            return;
        }

        Timber.d("Add 'Disable syncing' request for file: %s", fileState.getLocalPath());
        this.syncRequests.put(fileState.getId(), new SyncRequest(fileState, DISABLE_SYNCING));
    }

    @Override
    protected boolean isFileMatchingSyncedFileState(@NonNull RemoteFile file, @NonNull SyncedFileState fileState) {
        final String remotePath = fileState.getRemotePath();
        return remotePath.equals(file.getRemotePath());
    }

    @Override
    protected boolean isSyncedFolderParentOfFile(@NonNull SyncedFolder syncedFolder, @NonNull String dirPath) {
        return syncedFolder.getRemoteFolder().equals(dirPath);
    }

    private void addUploadRequest(SyncedFileState fileState) {
        fileState.setLastEtag(""); //Force fake lastEtag value to bypass condition check on UploadFileOperation
        syncRequests.put(fileState.getId(), new SyncRequest(fileState, SyncRequest.Type.UPLOAD));
    }

    private void addUpdateDBRequest(SyncedFileState fileState, RemoteFile file) {
        fileState.setLastEtag(file.getEtag());
        final int affectedRows = DbHelper.manageSyncedFileStateDB(fileState, "UPDATE", context);
        if (affectedRows == 0) Timber.d("Error while updating eTag in DB for: %s", file.getRemotePath());
    }

    private void addDownloadRequest(SyncedFileState fileState, RemoteFile file) {
        Timber.d("Add download SyncRequest for %s", file.getRemotePath());
        syncRequests.put(fileState.getId(), new DownloadRequest(file, fileState));
    }
}