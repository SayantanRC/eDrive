/*
 * Copyright © ECORP SAS 2022.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */
package foundation.e.drive.periodicScan.contentScanner;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

/**
 * This wrapper is used to contains either a File/RemoteFile instance with its content
 * or a "missing file" information. The explanation of this, is because scanning remote file
 * may result in different type of error while loading content (network issues, etc.)
 * but we want to handle 404 error (= missing resources) has to be considered like a
 * potential file deletions. There is probably something better to do for that
 * @author vincent Bourgmayer
 * @param <T> File or RemoteFile expected
 */
public class FolderWrapper<T> {

    private final T folder;
    private final List<T> content;

    protected FolderWrapper(@NonNull T folder) {
        content = new ArrayList<>();
        this.folder = folder;
    }

    protected FolderWrapper() {
        folder = null;
        content = null;
    }

    /**
     * Get the folder (should be RemoteFile or File instance ) represented by this abstraction
     * @return null if it is missing
     */
    public T getFolder() {
        return folder;
    }

    /**
     * Get subfiles (including sub folder) of the current folder
     * @return Null if the directory is missing
     */
    @NonNull
    public List<T> getContent() {
        return content;
    }

    public void addContent(@NonNull List<T> newContent) {
        content.addAll(newContent);
    }

    /**
     * Should return true if local File for directory doesn't exists, or if ReadFolderRemoteOperation return 404
     * for a remote file
     * @return
     */
    public boolean isMissing() {
        return folder == null;
    }
}
