/*
 * Copyright © MURENA SAS 2023.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */

package foundation.e.drive.periodicScan;

import static com.owncloud.android.lib.resources.files.FileUtils.PATH_SEPARATOR;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.database.Cursor;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import foundation.e.drive.utils.AppConstants;
import timber.log.Timber;

/**
 * Class responsible for building a list of installed app and save that in a file that must be synchronized
 * @author vincent Bourgmayer
 */
public class ListAppsWorker extends Worker {
    private static final String PWA_PLAYER = "content://foundation.e.pwaplayer.provider/pwa";
    private static final String SEPARATOR = ",";
    private static final String PWA_SECTION_SEPARATOR = "\n---- PWAs ----\n";
    private static final String APPLICATIONS_LIST_FILE_NAME_TMP = ".tmp_packages_list.csv";

    public ListAppsWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
    }

    @NonNull
    @Override
    public Result doWork() {
        Timber.d("generateAppListFile()");
        try {
            final Context context = getApplicationContext();
            final StringBuilder fileContents = listRegularApps(context);
            listPWAs(context, fileContents);
            if (fileContents.length() == 0) return Result.success();

            final boolean success = writeToFile(fileContents);
            return success ? Result.success() : Result.failure();
        } catch (Exception exception) {
            Timber.e(exception);
            return Result.failure();
        }
    }

    private StringBuilder listRegularApps(@NonNull final Context context) {
        final List<PackageInfo> packagesInfo = context.getPackageManager().getInstalledPackages(0);
        final StringBuilder result = new StringBuilder();
        for (PackageInfo currentPkg : packagesInfo) {
            result.append(currentPkg.packageName)
                  .append(SEPARATOR)
                  .append(currentPkg.versionName)
                  .append("\n");
        }
        return result;
    }

    private void listPWAs(@NonNull final Context context, @NonNull final StringBuilder stringBuilder) {
        Timber.v("ListPWAs");
        final Cursor cursor = context.getContentResolver().query(
                Uri.parse(PWA_PLAYER), null, null, null, null);

        if (cursor.getCount() <= 0) {
            cursor.close();
            return;
        }

        stringBuilder.append(PWA_SECTION_SEPARATOR);
        cursor.moveToFirst();
        Timber.v("Writing list of PWA");
        do {
            try {
                final String pwaTitle = cursor.getString(cursor.getColumnIndexOrThrow("title"));
                final String pwaUrl = cursor.getString(cursor.getColumnIndexOrThrow("url"));
                final Long pwaDbId = cursor.getLong(cursor.getColumnIndexOrThrow("_id"));
                stringBuilder.append(pwaDbId)
                             .append(SEPARATOR)
                             .append(pwaTitle)
                             .append(SEPARATOR)
                             .append(pwaUrl)
                             .append("\n");

            } catch (IllegalArgumentException exception) {
                Timber.e(exception, "Caught exception: invalid column names for cursor");
            }
        } while (cursor.moveToNext());

        cursor.close();
    }

    private boolean writeToFile(@NonNull final StringBuilder fileContents) {
        try (final FileOutputStream tmp = getApplicationContext().openFileOutput(APPLICATIONS_LIST_FILE_NAME_TMP, Context.MODE_PRIVATE)
        ) {
            tmp.write(fileContents.toString().getBytes());

            final String filesDir = getApplicationContext().getFilesDir().getCanonicalPath() + PATH_SEPARATOR;
            final File tmp_file = new File(filesDir + APPLICATIONS_LIST_FILE_NAME_TMP);
            final File real_file = new File(filesDir + AppConstants.APPLICATIONS_LIST_FILE_NAME);

            if (tmp_file.length() != real_file.length()) {
                tmp_file.renameTo(real_file);
            } else {
                tmp_file.delete();
            }
        } catch (IOException exception) {
            Timber.e(exception, "caught exception: can't write app list in file");
            return false;
        }
        return true;
    }
}