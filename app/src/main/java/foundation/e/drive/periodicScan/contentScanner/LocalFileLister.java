/*
 * Copyright © ECORP SAS 2022.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */
package foundation.e.drive.periodicScan.contentScanner;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

import java.io.File;
import java.io.FileFilter;
import java.util.Arrays;
import java.util.List;

import foundation.e.drive.database.DbHelper;
import foundation.e.drive.fileFilters.FileFilterFactory;
import foundation.e.drive.models.SyncedFolder;
import timber.log.Timber;

/**
 * Implementation of AbstractFileLister with method adapted to local content
 * @author vincent  Bourgmayer
 */
public class LocalFileLister extends AbstractFileLister<File> {

    public LocalFileLister(@NonNull List<SyncedFolder> directories) {
        super(directories);
        super.diveIntoSkippedDir = true;
    }

    @Override
    protected boolean skipDirectory(@NonNull File currentDirectory, @NonNull SyncedFolder syncedFolder, @NonNull Context context) {
        return currentDirectory.lastModified() == syncedFolder.getLastModified()
                && !DbHelper.syncedFolderHasContentToUpload(syncedFolder.getId(), context);
    }

    @Override
    protected boolean skipSyncedFolder(@NonNull SyncedFolder syncedFolder) {
        final File folder = new File(syncedFolder.getLocalFolder());
        return (syncedFolder.isMediaType() && folder.isHidden()) || !syncedFolder.isScanLocal();
    }

    @Override
    protected boolean skipFile(@NonNull File file) {
        return file.isHidden();
    }

    @Override
    protected boolean isDirectory(@NonNull File file) {
        return file.isDirectory();
    }

    @Override
    @Nullable
    protected String getFileName(@NonNull File file) {
        return file.getName();
    }

    @Override
    @NonNull
    protected LocalFolderLoader createFolderLoader() {
        return new LocalFolderLoader();
    }

    @Override
    protected void updateSyncedFolder(@NonNull SyncedFolder syncedFolder, @NonNull File folder) {
        syncedFolder.setLastModified(folder.lastModified());
    }

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    public static class LocalFolderLoader implements FolderLoader<File> {
        private FolderWrapper<File> folder;

        @Override
        @NonNull
        public FolderWrapper<File> getFolderWrapper() {
            return folder;
        }


        /**
         * "Load" a directory to check if it exist, and list its content
         * It is expected to always return true. Its remote equivalent (in RemoteFileLister)
         * Might return false because of network, & RemoteFile classes. But that's not the case
         * here.
         * @param syncedFolder SyncedFolder instance with data about the folder to load
         * @return always true because it's about "File" instance.
         */
        @Override
        public boolean load(@NonNull SyncedFolder syncedFolder) {
            final File dir = new File(syncedFolder.getLocalFolder());
            Timber.v("Local Folder (last modified / exists): %s, %s", dir.lastModified(), dir.exists());

            if (!dir.exists()) {
                folder = new FolderWrapper(); //missing Folder Wrapper
                return true;
            } 

            folder = new FolderWrapper(dir);
            final String category = syncedFolder.isMediaType() ? "media" : syncedFolder.getLibelle();

            final FileFilter filter = FileFilterFactory.INSTANCE.buildFileFilter(category);
            final File[] files = dir.listFiles(filter);
            if (files != null) {
                folder.addContent(Arrays.asList(files));
            }
            
            return true;
        }
    }
}
