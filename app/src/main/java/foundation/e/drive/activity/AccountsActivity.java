/*
 * Copyright © ECORP SAS 2022.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */

package foundation.e.drive.activity;

import static foundation.e.drive.utils.AppConstants.ACCOUNT_DATA_ALIAS_KEY;
import static foundation.e.drive.utils.AppConstants.ACCOUNT_DATA_EMAIL;
import static foundation.e.drive.utils.AppConstants.ACCOUNT_DATA_NAME;
import static foundation.e.drive.utils.AppConstants.ACCOUNT_DATA_TOTAL_QUOTA_KEY;
import static foundation.e.drive.utils.AppConstants.ACCOUNT_DATA_USED_QUOTA_KEY;
import static foundation.e.drive.widgets.EDriveWidget.buildIntent;
import static foundation.e.drive.widgets.EDriveWidget.convertIntoMB;
import static foundation.e.drive.widgets.EDriveWidget.dataForWeb;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.ComponentName;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.owncloud.android.lib.common.OwnCloudClient;

import foundation.e.drive.R;
import foundation.e.drive.account.AccountUtils;
import foundation.e.drive.databinding.ActivityAccountsBinding;
import foundation.e.drive.utils.CommonUtils;
import foundation.e.drive.utils.DavClientProvider;
import foundation.e.drive.utils.AccessTokenProvider;
import foundation.e.drive.widgets.EDriveWidget;
import timber.log.Timber;

public class AccountsActivity extends AppCompatActivity {

    public static final String NON_OFFICIAL_AVATAR_PATH = "/index.php/avatar/";
    public static final String ACCOUNT_MANAGER_PACKAGE_NAME = "foundation.e.accountmanager";
    private static final String ACCOUNT_SETTINGS = "at.bitfire.davdroid.ui.AccountsActivity";

    private ActivityAccountsBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Timber.tag(AccountsActivity.class.getSimpleName());
        binding = ActivityAccountsBinding.inflate(getLayoutInflater());

        setContentView(binding.getRoot());
        setSupportActionBar(binding.toolbar);

        binding.toolbar.setNavigationOnClickListener(v -> onBackPressed());

        binding.settings.setOnClickListener(v -> {
            final Intent settingsIntent = buildIntent("", "")
                    .setComponent(new ComponentName(
                            ACCOUNT_MANAGER_PACKAGE_NAME, ACCOUNT_SETTINGS));
            startActivity(settingsIntent);
        });
        showDetails();
    }

    private void showDetails() {
        final AccountManager accountManager = AccountManager.get(this);
        final Account account = CommonUtils.getAccount(getString(R.string.eelo_account_type),
                accountManager);


        final String usedQuota = accountManager.getUserData(account, ACCOUNT_DATA_USED_QUOTA_KEY);
        final String totalQuota = accountManager.getUserData(account, ACCOUNT_DATA_TOTAL_QUOTA_KEY);
        final String email = accountManager.getUserData(account, ACCOUNT_DATA_EMAIL);
        String name = accountManager.getUserData(account, ACCOUNT_DATA_NAME);
        final String token = AccessTokenProvider.getToken(accountManager, account);

        // For some reason if we cant get name use email as name
        if (name == null || name.isEmpty()) {
            name = email;
        }

        binding.name.setText(name);
        binding.email.setText(email);

        binding.progress.setMax(convertIntoMB(totalQuota));
        binding.progress.setProgress(convertIntoMB(usedQuota));
        binding.progress.setVisibility(View.VISIBLE);

        String totalShownQuota = "?";
        String usedShownQuota = "?";
        try {
            final long totalQuotaLong = Long.parseLong(totalQuota);
            if (totalQuotaLong >= 0) {
                totalShownQuota = CommonUtils.humanReadableByteCountBin(totalQuotaLong);
            }
        } catch (NumberFormatException ignored) {
            Timber.i("Bad totalQuotaLong %s", totalQuota);
        }

        try {
            final long usedQuotaLong = Long.parseLong(usedQuota);
            if (usedQuotaLong >= 0) {
                usedShownQuota = CommonUtils.humanReadableByteCountBin(usedQuotaLong);
            }
        } catch (NumberFormatException ignore) {
            Timber.i("Bad usedQuotaLong %s", usedQuota);
        }

        binding.plan.setText(getString(R.string.free_plan, totalShownQuota));

         final String premiumPlan = AccountUtils.getPremiumPlan(accountManager, account);
         if (premiumPlan != null) {
             binding.plan.setText(getString(R.string.premium_plan, premiumPlan));
         }

        binding.myPlan.setVisibility(View.VISIBLE);
        binding.plan.setVisibility(View.VISIBLE);

        binding.status.setText(getString(R.string.progress_status, usedShownQuota, totalShownQuota));

        String aliases = accountManager.getUserData(account, ACCOUNT_DATA_ALIAS_KEY);
        if (aliases != null && !aliases.isEmpty()) {
            binding.alias.setVisibility(View.VISIBLE);
            binding.alias1.setText(aliases.split(",")[0]);
            binding.aliasDivider.setVisibility(View.VISIBLE);
        } else {
            binding.alias.setVisibility(View.GONE);
            binding.alias1Container.setVisibility(View.GONE);
        }

        binding.alias1Clipboard.setOnClickListener(v ->
                CommonUtils.copyToClipboard(Uri.parse(binding.alias1.getText().toString()),
                        v.getContext(), v.getContext().getString(R.string.alias)));

        binding.upgrade.setVisibility(View.VISIBLE);
        binding.upgrade.setOnClickListener(v -> {
            final Intent upgradeIntent = buildIntent(Intent.ACTION_VIEW,
                    String.format(EDriveWidget.WEBPAGE, email, token,
                            dataForWeb(totalQuota)));
            startActivity(upgradeIntent);
        });

        binding.alias.setOnClickListener(v -> {
            if (binding.alias1Container.getVisibility() == View.VISIBLE) {
                binding.alias1Container.setVisibility(View.GONE);
                binding.alias.setCompoundDrawablesWithIntrinsicBounds(null, null,
                        ContextCompat.getDrawable(v.getContext(), R.drawable.ic_expand_more), null);
            } else {
                binding.alias1Container.setVisibility(View.VISIBLE);
                binding.alias.setCompoundDrawablesWithIntrinsicBounds(null, null,
                        ContextCompat.getDrawable(v.getContext(), R.drawable.ic_expand_less), null);
            }
        });

        final OwnCloudClient client = DavClientProvider.getInstance().getClientInstance(account, this);
        if (client != null) {
            Glide.with(this)
                    .load(client.getBaseUri() + NON_OFFICIAL_AVATAR_PATH
                            + client.getCredentials().getUsername() + "/" + 300)
                    .error(R.drawable.ic_murena_eel)
                    .into(binding.avatar);
            binding.avatar.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onDestroy() {
        DavClientProvider.getInstance().saveAccounts(this);
        super.onDestroy();
    }
}
