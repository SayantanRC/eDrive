/*
 * Copyright © MURENA SAS 2023-2024.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */
package foundation.e.drive.account.receivers

import android.accounts.AccountManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import foundation.e.drive.R
import foundation.e.drive.account.AccountUtils
import foundation.e.drive.utils.AppConstants
import foundation.e.drive.utils.DavClientProvider
import foundation.e.drive.work.WorkLauncher
import timber.log.Timber

/**
 * Entry point for eDrive
 * Triggered by AccountManager
 * @author Vincent Bourgmayer
 */
class AccountAddedReceiver() : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        Timber.d("\"Account added\" intent received")

        if (context == null || intent == null || intent.extras == null) return

        val extras = intent.extras!!
        val accountName = extras.getString(AccountManager.KEY_ACCOUNT_NAME, "")
        val accountType = extras.getString(AccountManager.KEY_ACCOUNT_TYPE, "")
        val prefs = context.getSharedPreferences(AppConstants.SHARED_PREFERENCE_NAME,
            Context.MODE_PRIVATE)

        Timber.d("AccountAddedReceiver.onReceive with name: %s and type: %s", accountName, accountType)

        if (!canStart(accountName, accountType, prefs, context)) return

        prefs.edit()
            .putString(AccountManager.KEY_ACCOUNT_NAME, accountName)
            .apply()

        val workLauncher = WorkLauncher.getInstance(context)
        if (workLauncher.enqueueSetupWorkers(context)) {
            DavClientProvider.getInstance().cleanUp()
            workLauncher.enqueuePeriodicUserInfoFetching()
        }
}

    /**
     * Check that conditions to start are met:
     * - Setup has not already been done
     * - AccountName is not empty
     * - AccountType is /e/ account
     * - the account is effectively available through accountManager
     */
    private fun canStart(
        accountName: String,
        accountType: String,
        prefs: SharedPreferences,
        context: Context
    ): Boolean {
        if (isSetupAlreadyDone(prefs)) {
            return false
        }

        if (accountName.isEmpty()) {
            return false
        }

        if (isInvalidAccountType(accountType, context)) {
            return false
        }

        if (!isExistingAccount(accountName, context)) {
            Timber.w("No account exist for username: %s ", accountType, accountName)
            return false
        }
        return true
    }

    private fun isInvalidAccountType(accountType: String, context: Context): Boolean {
        val validAccountType = context.getString(R.string.eelo_account_type)
        return accountType != validAccountType
    }

    private fun isSetupAlreadyDone(prefs: SharedPreferences): Boolean {
        return prefs.getBoolean(AppConstants.SETUP_COMPLETED, false)
    }

    private fun isExistingAccount(accountName: String, context: Context): Boolean {
        return AccountUtils.getAccount(accountName, context) != null
    }
}
