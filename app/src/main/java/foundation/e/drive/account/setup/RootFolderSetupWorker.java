/*
 * Copyright © MURENA SAS 2022-2023.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */

package foundation.e.drive.account.setup;

import android.accounts.Account;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.work.Data;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.owncloud.android.lib.common.OwnCloudClient;
import com.owncloud.android.lib.common.operations.RemoteOperationResult;
import com.owncloud.android.lib.resources.files.CreateFolderRemoteOperation;

import java.io.File;

import foundation.e.drive.account.AccountUtils;
import foundation.e.drive.database.DbHelper;
import foundation.e.drive.models.SyncedFolder;
import foundation.e.drive.utils.CommonUtils;
import foundation.e.drive.utils.DavClientProvider;
import timber.log.Timber;

/**
 * Create folder on ecloud for a given local folder
 * Then register it in the database
 *
 *  /i\ Doesn't require NextcloudClient yet
 * @author Vincent Bourgmayer
 */
public class RootFolderSetupWorker extends Worker {
    public static final String DATA_KEY_ID="id";
    public static final String DATA_KEY_LIBELLE="libelle";
    public static final String DATA_KEY_LOCAL_PATH="localPath";
    public static final String DATA_KEY_REMOTE_PATH="remotePath";
    public static final String DATA_KEY_LAST_ETAG="etag";
    public static final String DATA_KEY_LAST_MODIFIED="lModified";
    public static final String DATA_KEY_SCAN_LOCAL="scanLocal";
    public static final String DATA_KEY_SCAN_REMOTE="scanRemote";
    public static final String DATA_KEY_ENABLE="enable";
    public static final String DATA_KEY_MEDIATYPE="mediaType";

    public RootFolderSetupWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
        Timber.tag(RootFolderSetupWorker.class.getSimpleName());
    }

    @NonNull
    @Override
    public Result doWork() {
        try {
            final Context context = getApplicationContext();
            final Account account = AccountUtils.getAccount(getApplicationContext());
            if (account == null) {
                Timber.d("doWork(): Can't get valid account");
                return Result.failure();
            }

            final boolean meteredNetworkAllowed = CommonUtils.isMeteredNetworkAllowed(account);
            if (!CommonUtils.haveNetworkConnection(context, meteredNetworkAllowed)) {
                Timber.d("doWork(): no usable connection");
                return Result.retry();
            }

            final SyncedFolder syncedFolder = getSyncedFolderFromData();
            if (syncedFolder == null) return Result.failure();

            Timber.v("doWork() for : %s", syncedFolder.getLocalFolder());
            final File folder = new File(syncedFolder.getLocalFolder());
            if (!folder.exists()) {
                folder.mkdirs();
                syncedFolder.setLastModified(folder.lastModified());
            }

            final OwnCloudClient client = DavClientProvider.getInstance().getClientInstance(account, context);
            if (client == null) {
                Timber.d("doWork(): Can't get OwnCloudClient");
                return Result.retry();
            }

            final CreateFolderRemoteOperation mkcolRequest =
                    new CreateFolderRemoteOperation(syncedFolder.getRemoteFolder(), true);

            @SuppressWarnings("deprecation") final RemoteOperationResult result = mkcolRequest.execute(client);

            DavClientProvider.getInstance().saveAccounts(context);

            if (result.isSuccess() || result.getCode() == RemoteOperationResult.ResultCode.FOLDER_ALREADY_EXISTS) {
                DbHelper.insertSyncedFolder(syncedFolder, context);
                return Result.success();
            }
        } catch (Exception exception) {
            Timber.e(exception);
        }
        return Result.retry();
    }

    @Nullable
    private SyncedFolder getSyncedFolderFromData() {
        final Data data = getInputData();
        final String category = data.getString(DATA_KEY_LIBELLE);
        final String remotePath = data.getString(DATA_KEY_REMOTE_PATH);
        final String localPath = data.getString(DATA_KEY_LOCAL_PATH);

        if( category == null || remotePath == null || localPath == null) {
            return null;
        }
        final SyncedFolder result = new SyncedFolder(
                category,
                localPath,
                remotePath,
                data.getBoolean(DATA_KEY_SCAN_LOCAL, true),
                data.getBoolean(DATA_KEY_SCAN_REMOTE, true),
                data.getBoolean(DATA_KEY_ENABLE, true),
                data.getBoolean(DATA_KEY_MEDIATYPE, true));

        result.setLastModified(data.getLong(DATA_KEY_LAST_MODIFIED, 0L));
        result.setLastEtag(data.getString(DATA_KEY_LAST_ETAG));
        result.setId(data.getInt(DATA_KEY_ID, -1));

        return result;
    }
}
