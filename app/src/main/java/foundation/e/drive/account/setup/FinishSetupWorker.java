/*
 * Copyright © MURENA SAS 2022-2023.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */

package foundation.e.drive.account.setup;

import static android.content.Context.MODE_PRIVATE;
import static foundation.e.drive.utils.AppConstants.INITIAL_FOLDER_NUMBER;
import static foundation.e.drive.utils.AppConstants.SHARED_PREFERENCE_NAME;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import foundation.e.drive.utils.AppConstants;
import foundation.e.drive.work.WorkLauncher;
import timber.log.Timber;

/**
 * This class perform last task of setup
 * It contains job to start FileObserver, periodic fullScan and Synchronization service
 * for the first time
 * @author Vincent Bourgmayer
 */
public class FinishSetupWorker extends Worker {
    public FinishSetupWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
    }

    @NonNull
    @Override
    public Result doWork() {
        Timber.v("FinishSetupWorker.doWork()");
        try {
            final Context appContext = getApplicationContext();
            enqueueWorkers(appContext);
            updateSharedPreferences(appContext);

            return Result.success();

        } catch (Exception exception) {
            Timber.e(exception);
        }
        return Result.retry();
    }

    private void updateSharedPreferences(Context appContext) {
        appContext.getSharedPreferences(SHARED_PREFERENCE_NAME, MODE_PRIVATE)
                .edit()
                .putBoolean(AppConstants.SETUP_COMPLETED, true)
                .putInt(INITIAL_FOLDER_NUMBER, 9)
                .apply();
    }

    private void enqueueWorkers(@NonNull final Context appContext) {
        final WorkLauncher launcher = WorkLauncher.getInstance(appContext);
        launcher.enqueueOneTimeAppListGenerator();
        launcher.enqueueOneTimeFullScan(false);
        launcher.enqueuePeriodicFullScan();
    }
}