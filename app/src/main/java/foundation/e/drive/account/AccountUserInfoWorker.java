/*
 * Copyright © ECORP SAS 2022.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */
package foundation.e.drive.account;

import static foundation.e.drive.utils.AppConstants.ACCOUNT_DATA_ALIAS_KEY;
import static foundation.e.drive.utils.AppConstants.ACCOUNT_DATA_EMAIL;
import static foundation.e.drive.utils.AppConstants.ACCOUNT_DATA_GROUPS;
import static foundation.e.drive.utils.AppConstants.ACCOUNT_DATA_NAME;
import static foundation.e.drive.utils.AppConstants.ACCOUNT_DATA_RELATIVE_QUOTA_KEY;
import static foundation.e.drive.utils.AppConstants.ACCOUNT_DATA_TOTAL_QUOTA_KEY;
import static foundation.e.drive.utils.AppConstants.ACCOUNT_DATA_USED_QUOTA_KEY;
import static foundation.e.drive.utils.AppConstants.ACCOUNT_USER_ID_KEY;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.nextcloud.common.NextcloudClient;
import com.owncloud.android.lib.common.Quota;
import com.owncloud.android.lib.common.UserInfo;
import com.owncloud.android.lib.common.operations.RemoteOperationResult;
import com.owncloud.android.lib.resources.users.GetUserInfoRemoteOperation;

import java.util.ArrayList;

import foundation.e.drive.R;
import foundation.e.drive.activity.AccountsActivity;
import foundation.e.drive.utils.AppConstants;
import foundation.e.drive.utils.CommonUtils;
import foundation.e.drive.utils.DavClientProvider;
import foundation.e.drive.utils.ViewUtils;
import timber.log.Timber;

/**
 * @author vincent Bourgmayer
 * @author TheScarastic
 */
public class AccountUserInfoWorker extends Worker {
    private static final int QUOTA_NOTIFICATION_ID = 11;
    public static final String UNIQUE_WORK_NAME = "AccountUserInfoWorker";

    private final AccountManager accountManager;
    private final Context context;
    private Account account;

    public AccountUserInfoWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
        this.context = context;
        accountManager = AccountManager.get(context);
        account = CommonUtils.getAccount(context.getString(R.string.eelo_account_type), accountManager);
    }

    @NonNull
    @Override
    public Result doWork() {
        final NextcloudClient client = getNextcloudClient();
        if (client == null) {
            Timber.d("Cannot fetch user info & aliases becauses NC client is null");
            return Result.failure();
        }

        Result result = Result.failure();

        try {
            if (fetchUserInfo(client) && fetchAliases(client)) {
                Glide.with(context)
                        .load(client.getBaseUri() + AccountsActivity.NON_OFFICIAL_AVATAR_PATH
                                + client.getUserId() + "/" + 300)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .preload();
                ViewUtils.updateWidgetView(context);
                DavClientProvider.getInstance().saveAccounts(context);
                result = Result.success();
            } else {
                result = Result.retry();
            }
        } catch (Throwable exception) {
            Timber.e(exception, "Exception on retrieving accountUserInfo.");
        }

        return result;
    }

    @Nullable
    private NextcloudClient getNextcloudClient() {
        if (account == null) {
            return null;
        }

        return DavClientProvider.getInstance().getNcClientInstance(account, context);
    }

    private boolean fetchUserInfo(final NextcloudClient client) {
        final GetUserInfoRemoteOperation getUserInfoRemoteOperation = new GetUserInfoRemoteOperation();
        final RemoteOperationResult<UserInfo> ocsResult = getUserInfoRemoteOperation.execute(client);

        if (ocsResult.isSuccess()) {
            final UserInfo userInfo = ocsResult.getResultData();

            if (accountManager.getUserData(account, ACCOUNT_USER_ID_KEY) == null) {
                final String userId = userInfo.getId();

                if (userId != null) {
                    client.setUserId(userId);
                    AccountManager.get(context).setUserData(account, ACCOUNT_USER_ID_KEY, userId);
                    Timber.v("UserId %s saved for account", userId);
                }
            }

            updateQuotaInUserData(userInfo);

            final String groups = (userInfo.getGroups() != null) ? String.join(",", userInfo.getGroups()) : "";

            accountManager.setUserData(account, ACCOUNT_DATA_NAME, userInfo.getDisplayName());
            accountManager.setUserData(account, ACCOUNT_DATA_EMAIL, userInfo.getEmail());
            accountManager.setUserData(account, ACCOUNT_DATA_GROUPS, groups);

            Timber.d("fetchUserInfo(): success");
            return true;
        }
        Timber.d("fetchUserInfo(): failure");
        return false;
    }

    private void updateQuotaInUserData(UserInfo userInfo) {
        final Quota userQuota = userInfo.getQuota();
        long totalQuota = 0L;
        long usedQuota = 0L;
        double relativeQuota = 0.0D;

        if (userQuota != null) {
            totalQuota = userQuota.getTotal();
            if (totalQuota <= 0) {
                totalQuota = 0;
            }

            usedQuota = userQuota.getUsed();
            relativeQuota = userQuota.getRelative();
        }

        accountManager.setUserData(account, ACCOUNT_DATA_TOTAL_QUOTA_KEY, "" + totalQuota);
        accountManager.setUserData(account, ACCOUNT_DATA_RELATIVE_QUOTA_KEY, "" + relativeQuota);
        accountManager.setUserData(account, ACCOUNT_DATA_USED_QUOTA_KEY, "" + usedQuota);

        addNotifAboutQuota(relativeQuota);
    }

    private void addNotifAboutQuota(final double relativeQuota) {
        final Context context = getApplicationContext();
        boolean needToNotify = needToSendNotification(context);

        final NotificationManager manager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        if (relativeQuota >= 99.0) {
            addNotification(manager, context.getString(R.string.notif_quota_99Plus_title), context.getString(R.string.notif_quota_99Plus_text), true);
        } else if (relativeQuota >= 90.0 && needToNotify) {
            addNotification(manager, context.getString(R.string.notif_quota_80plus_title), context.getString(R.string.notif_quota_90Plus_text), false);
        } else if (relativeQuota >= 80.0 && needToNotify) {
            addNotification(manager, context.getString(R.string.notif_quota_80plus_title), context.getString(R.string.notif_quota_80Plus_text), false);
        } else {
            try {
                manager.cancel(QUOTA_NOTIFICATION_ID);
            } catch (Exception e) {
                Timber.e(e, "Failed to cancel quota notification");
            }
        }
    }

    private boolean needToSendNotification(final Context context) {
        final String LAST_NOTIFICATION_TIMESTAMP_KEY = "last_notification_timestamp";
        final long MILLI_SECONDS_IN_A_DAY = 24 * 60 * 60 * 1000;

        SharedPreferences sharedPreferences = context.getSharedPreferences(
                AppConstants.SHARED_PREFERENCE_NAME,
                Context.MODE_PRIVATE
        );

        long previousTimestamp = sharedPreferences.getLong(LAST_NOTIFICATION_TIMESTAMP_KEY, 0);
        long currentTimeStamp = System.currentTimeMillis();

        if (previousTimestamp == 0 ||
                currentTimeStamp - previousTimestamp > MILLI_SECONDS_IN_A_DAY) {
            sharedPreferences
                    .edit()
                    .putLong(LAST_NOTIFICATION_TIMESTAMP_KEY, currentTimeStamp)
                    .apply();
            return true;
        }
        return false;
    }

    /**
     * send notification to inform user that he lacks space on ecloud
     * Improvement idea:
     * - add translatable message & title
     * - make message & title to be a parameter of the method, so we could reuse the function somewhere
     * - else with different notification. File conflict for example.
     **/
    private void addNotification(NotificationManager manager, String title, String text, boolean isOnGoing) {
        NotificationChannel channel = manager.getNotificationChannel(AppConstants.notificationChannelID);
        if (channel == null) {
            CommonUtils.createNotificationChannel(getApplicationContext());
        }

        final NotificationCompat.Builder builder =
                new NotificationCompat.Builder(getApplicationContext(), AppConstants.notificationChannelID)
                        .setSmallIcon(android.R.drawable.stat_sys_warning)
                        .setContentTitle(title)
                        .setContentText(text)
                        .setOngoing(isOnGoing)
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(text));
        // Add as notification
        manager.notify(QUOTA_NOTIFICATION_ID, builder.build());
    }

    private boolean fetchAliases(NextcloudClient client) {
        final String userId = accountManager.getUserData(account, ACCOUNT_USER_ID_KEY);

        if (userId == null || userId.isEmpty()) {
            return false;
        }

        final GetAliasOperation getAliasOperation = new GetAliasOperation(userId);
        final RemoteOperationResult<ArrayList<String>> ocsResult = getAliasOperation.execute(client);
        String aliases = "";

        if (ocsResult.isSuccess()) {
            final ArrayList<String> aliasList = ocsResult.getResultData();
            if (aliasList != null && !aliasList.isEmpty()) {
                aliases = String.join(",", aliasList);
            }
        }
        accountManager.setUserData(account, ACCOUNT_DATA_ALIAS_KEY, aliases);
        Timber.d("fetchAliases(): success: %s", ocsResult.isSuccess());

        DavClientProvider.getInstance().saveAccounts(context);

        return true;
    }
}
