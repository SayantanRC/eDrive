/*
 * Copyright © ECORP SAS 2023.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */

package foundation.e.drive.account.receivers;

import static foundation.e.drive.utils.AppConstants.INITIAL_FOLDER_NUMBER;
import static foundation.e.drive.utils.AppConstants.SETUP_COMPLETED;

import android.accounts.AccountManager;
import android.annotation.SuppressLint;
import android.app.Application;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.work.WorkManager;

import java.io.File;

import foundation.e.drive.R;
import foundation.e.drive.database.DbHelper;
import foundation.e.drive.database.FailedSyncPrefsManager;
import foundation.e.drive.synchronization.SyncProxy;
import foundation.e.drive.utils.AppConstants;
import foundation.e.drive.utils.DavClientProvider;
import foundation.e.drive.utils.ViewUtils;
import timber.log.Timber;

public class AccountRemoveCallbackReceiver extends BroadcastReceiver {

    @SuppressLint("UnsafeProtectedBroadcastReceiver")
    @Override
    public void onReceive(@NonNull Context context, @NonNull Intent intent) {
        Timber.d("Received account remove broadcast request");

        final Context applicationContext = context.getApplicationContext();

        final SharedPreferences preferences = applicationContext.getSharedPreferences(AppConstants.SHARED_PREFERENCE_NAME, Context.MODE_PRIVATE);

        if (!shouldProceedWithRemoval(intent, preferences, applicationContext)) {
            return;
        }

        cancelWorkers(applicationContext);
        SyncProxy.INSTANCE.moveToIdle((Application) applicationContext);
        deleteDatabase(applicationContext);
        cleanSharedPreferences(applicationContext, preferences);
        removeCachedFiles(applicationContext);
        deleteNotificationChannels(applicationContext);

        DavClientProvider.getInstance().cleanUp();

        ViewUtils.updateWidgetView(applicationContext);
    }


    private void cancelWorkers(@NonNull Context context) {
        final WorkManager workManager = WorkManager.getInstance(context);
        workManager.cancelAllWorkByTag(AppConstants.WORK_GENERIC_TAG);
    }

    private void deleteDatabase(@NonNull Context applicationContext) {
        final boolean result = applicationContext.deleteDatabase(DbHelper.DATABASE_NAME);
        Timber.d("Remove Database: %s", result);
    }

    private boolean shouldProceedWithRemoval(@NonNull Intent intent, @NonNull SharedPreferences preferences, @NonNull Context context) {
        if (isInvalidAction(intent) || intent.getExtras() == null) {
            Timber.w("Invalid account removal request");
            return false;
        }

        String currentAccount = preferences.getString(AccountManager.KEY_ACCOUNT_NAME, "");
        String currentAccountType = context.getString(R.string.eelo_account_type);

        if (currentAccount.isEmpty()) {
            Timber.d("No account set up, ignoring account removal");
            return false;
        }

        final String accountType = intent.getExtras().getString(AccountManager.KEY_ACCOUNT_TYPE);
        final String accountName = intent.getExtras().getString(AccountManager.KEY_ACCOUNT_NAME);

        return (currentAccount.equals(accountName) && currentAccountType.equals(accountType));
    }

    private boolean isInvalidAction(@NonNull Intent intent) {
        return !"android.accounts.action.ACCOUNT_REMOVED".equals(intent.getAction());
    }

    private void cleanSharedPreferences(@NonNull Context applicationContext, @NonNull SharedPreferences prefs) {
        if (!applicationContext.deleteSharedPreferences(AppConstants.SHARED_PREFERENCE_NAME)) {
            //If removal failed, clear all data inside
            prefs.edit().remove(AccountManager.KEY_ACCOUNT_NAME)
                    .remove(AccountManager.KEY_ACCOUNT_TYPE)
                    .remove(SETUP_COMPLETED)
                    .remove(INITIAL_FOLDER_NUMBER)
                    .remove(AppConstants.KEY_LAST_SCAN_TIME)
                    .apply();
        }

        applicationContext.deleteSharedPreferences(FailedSyncPrefsManager.PREF_NAME);
    }

    private void removeCachedFiles(@NonNull Context applicationContext) {
        try {
            deleteDir(applicationContext.getExternalCacheDir());
        } catch (SecurityException e) {
            Timber.e(e, "failed to delete cached file on account removal call");
        }
    }

    private boolean deleteDir(@Nullable File dir) throws SecurityException {
        if (dir == null) {
            Timber.w("cache file returned null. preventing a NPE");
            return false;
        }

        if (dir.isDirectory()) {
            String[] children = dir.list();
            if (children == null) {
                return dir.delete();
            }

            for (String child : children) {
                boolean isSuccess = deleteDir(new File(dir, child));
                if (!isSuccess) {
                    Timber.w("Failed to remove the cached file: %s", child);
                }
            }
        }

        return dir.delete();
    }

    private void deleteNotificationChannels(Context context) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        try {
            notificationManager.cancelAll();
        } catch (Exception exception) {
            Timber.e(exception, "Cannot cancel all notifications");
        }
    }
}
