/*
 * Copyright © MURENA SAS 2023.
 * Copyright © ECORP SAS 2022.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */

package foundation.e.drive.fileObservers

import android.content.Context
import android.os.FileObserver
import com.owncloud.android.lib.resources.files.FileUtils.PATH_SEPARATOR
import foundation.e.drive.database.DbHelper
import foundation.e.drive.models.DO_NOT_SCAN
import foundation.e.drive.models.SCAN_ON_CLOUD
import foundation.e.drive.models.SCAN_ON_DEVICE
import foundation.e.drive.models.SyncRequest
import foundation.e.drive.models.SyncedFileState
import foundation.e.drive.models.SyncedFolder
import foundation.e.drive.synchronization.SyncProxy
import foundation.e.drive.utils.FileUtils
import foundation.e.drive.utils.FileUtils.getLocalPath
import timber.log.Timber
import java.io.File

/**
 * @author Narinder Rana
 * @author vincent Bourgmayer
 * @author Fahim Salam Chowdhury
 */
class FileEventHandler(private val context: Context) {

    fun onEvent(event: Int, file: File) {
        if (FileUtils.isPartFile(file)) {
            return
        }

        if (file.isDirectory) {
            handleDirectoryEvent(event, file)
            return
        }

        handleFileEvent(event, file)
    }

    /**
     * Handle some file event for a file which is not a directory
     * @param event the event mask. CLOSE_WRITE, DELETE & MOVE_SELF are handled
     * @param file the file concerned by the event
     */
    private fun handleFileEvent(event: Int, file: File) {
        when (event) {
            FileObserver.CLOSE_WRITE -> handleFileCloseWrite(file)
            FileObserver.MOVED_TO -> handleFileMoveTo(file)
            FileObserver.DELETE -> handleFileDelete(file)
        }
    }

    /**
     * Handle FileEvent for a directory
     * @param event FileEvent mask. CREATE, CLOSE_WRITE, DELETE, MOVE_SELF
     * @param directory directory concerned by file event
     */
    private fun handleDirectoryEvent(event: Int, directory: File) {
        when (event) {
            FileObserver.CREATE -> handleDirectoryCreate(directory)
            FileObserver.CLOSE_WRITE -> handleDirectoryCloseWrite(directory)
            FileObserver.DELETE, FileObserver.DELETE_SELF -> handleDirectoryDelete(directory)
        }
    }

    /**
     * Send syncRequest to SynchronizationService
     * @param request SyncRequest that should be executed asap
     */
    private fun sendSyncRequestToSynchronizationService(request: SyncRequest) {
        val requestAdded = SyncProxy.queueSyncRequest(request, context.applicationContext)
        if (requestAdded) {
            Timber.d("Sending a SyncRequest for ${request.syncedFileState.name}")
            SyncProxy.startSynchronization(context)
        }
    }

    /**
     * When a new directory is detected, it must be inserted in database
     * if it's parent directory is already in the database
     * @param directory Directory that has been created
     */
    private fun handleDirectoryCreate(directory: File) {
        Timber.d("handleDirectoryCreate( ${directory.absolutePath} )")
        val parentFile = directory.parentFile ?: return

        val parentPath = getLocalPath(parentFile)
        val parentFolder = DbHelper.getSyncedFolderByLocalPath(parentPath, context) ?: return
        val folder = SyncedFolder(
            parentFolder,
            directory.name + PATH_SEPARATOR,
            directory.lastModified(),
            ""
        )
        DbHelper.insertSyncedFolder(folder, context)
    }

    /**
     * Handle CLOSE_WRITE event for a directory
     * todo: check in which condition a directory can generate a close_write
     * @param directory Directory that has been modified
     */
    private fun handleDirectoryCloseWrite(directory: File) {
        val fileLocalPath = getLocalPath(directory)
        Timber.d("handleDirectoryCloseWrite( $fileLocalPath )")
        val folder = DbHelper.getSyncedFolderByLocalPath(fileLocalPath, context)
        if (folder == null) {
            handleDirectoryCreate(directory) //todo check if really relevant
        } else {  //It's a directory update
            folder.setLastModified(directory.lastModified())
            DbHelper.updateSyncedFolder(folder, context)
        }
    }

    /**
     * Handle a file deletion event for a directory
     * @param directory Directory that has been removed
     */
    private fun handleDirectoryDelete(directory: File) {
        val fileLocalPath = getLocalPath(directory)
        Timber.d("handleDirectoryDelete( $fileLocalPath )")
        val folder = DbHelper.getSyncedFolderByLocalPath(fileLocalPath, context)
        if (folder == null) {
            insertDirDeleteRecordInDB(directory)
        } else if (folder.isEnabled) {
            folder.setEnabled(false)
            DbHelper.updateSyncedFolder(folder, context)
        }
    }

    private fun insertDirDeleteRecordInDB(directory: File) {
        //look for parent
        val parentFile = directory.parentFile ?: return

        val parentPath = getLocalPath(parentFile)
        val parentFolder = DbHelper.getSyncedFolderByLocalPath(parentPath, context) ?: return

        val folder = SyncedFolder(
            parentFolder,
            directory.name + PATH_SEPARATOR,
            directory.lastModified(),
            ""
        )
        folder.setEnabled(false)
        DbHelper.insertSyncedFolder(folder, context)
    }

    /**
     * handle a file close_write event for a file which is not a directory
     * @param file File that has been modified
     */
    private fun handleFileCloseWrite(file: File) {
        val fileLocalPath = getLocalPath(file)
        Timber.d("handleFileCloseWrite( $fileLocalPath )")
        var request: SyncRequest? = null
        val fileState = DbHelper.loadSyncedFile(context, fileLocalPath, true)

        if (fileState == null) { //New file discovered
            request = handleNewFileCreation(file)
        } else { //File update
            val isWaitingForDownload = fileState.isLastEtagStored() && fileState.lastModified == 0L
            if (fileState.scanScope > SCAN_ON_CLOUD && !isWaitingForDownload) {
                request = SyncRequest(fileState, SyncRequest.Type.UPLOAD)
            }
        }

        request?.let {
            sendSyncRequestToSynchronizationService(it)
        }
    }

    private fun handleFileMoveTo(file: File) {
        val fileLocalPath = getLocalPath(file)
        Timber.d("handleFileMoveTo( $fileLocalPath )")
        var request: SyncRequest? = null
        val fileState = DbHelper.loadSyncedFile(context, fileLocalPath, true)

        if (fileState == null) { //New file discovered
            request = handleNewFileCreation(file)
        }

        request?.let {
            sendSyncRequestToSynchronizationService(it)
        }
    }

    private fun handleNewFileCreation(file: File): SyncRequest? {
        val parentFile = file.parentFile ?: return null

        val parentPath = getLocalPath(parentFile)
        val parentFolder = DbHelper.getSyncedFolderByLocalPath(parentPath, context)
        if (parentFolder == null || !parentFolder.isEnabled) {
            Timber.d("Won't send sync request: no parent are known for new file: %s", file.name)
            return null
        }

        var scanScope = DO_NOT_SCAN
        if (parentFolder.isEnabled) {
            if (parentFolder.isScanRemote) scanScope = SCAN_ON_CLOUD
            if (parentFolder.isScanLocal) scanScope += SCAN_ON_DEVICE
        }

        val remotePath = parentFolder.remoteFolder + file.name
        val fileState = SyncedFileState(
            -1, file.name, getLocalPath(file), remotePath, "", 0L,
            parentFolder.id.toLong(), parentFolder.isMediaType, scanScope
        )
        return insertNewFileStateIntoDB(fileState, file)
    }

    private fun insertNewFileStateIntoDB(
        fileState: SyncedFileState,
        file: File
    ): SyncRequest? {
        val storedId = DbHelper.manageSyncedFileStateDB(fileState, "INSERT", context)
        if (storedId > 0) {
            fileState.id = storedId
            return SyncRequest(fileState, SyncRequest.Type.UPLOAD)
        }

        Timber.d("New File ${file.name} observed but impossible to insert it in DB")
        return null
    }

    /**
     * Handle a file deletion event for a file which is not a directory
     * @param file File that has been removed
     */
    private fun handleFileDelete(file: File) {
        val fileLocalPath = getLocalPath(file)
        Timber.d("handleFileDelete( $fileLocalPath )")
        val fileState = DbHelper.loadSyncedFile(context, fileLocalPath, true) ?: return

        //If already in DB
        if (fileState.scanScope > DO_NOT_SCAN) {
            //todo: if file is already sync disabled, we should probably remove file from DB
            val disableSyncingRequest = SyncRequest(fileState, SyncRequest.Type.DISABLE_SYNCING)
            sendSyncRequestToSynchronizationService(disableSyncingRequest)
        }
    }
}
