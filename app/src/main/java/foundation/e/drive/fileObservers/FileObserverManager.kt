/*
 * Copyright © MURENA SAS 2023.
 * Copyright © ECORP SAS 2022.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */

package foundation.e.drive.fileObservers

import android.content.Context
import android.os.Build
import android.os.FileObserver
import foundation.e.drive.database.DbHelper
import foundation.e.drive.models.SyncedFolder
import foundation.e.drive.utils.FileUtils
import timber.log.Timber
import java.io.File
import java.io.FileFilter
import java.util.Stack

/**
 * @author Narinder Rana
 * @author Vincent Bourgmayer
 * @author Fahim Salam Chowdhury
 */
class FileObserverManager(private val context: Context) : FileEventListener {

    companion object {
        val WATCHABLE_DIRECTORIES_FILTER = FileFilter {
            it.isDirectory && !it.isHidden && FileUtils.isNotPartFile(it)
        }
    }

    private val observers = HashMap<String, FileObserver>()
    private val handler = FileEventHandler(context)

    private var watching = false

    override fun notify(event: Int, file: File, dirPath: String) {
        if (file.isHidden) {
            return
        }

        Timber.d("notified for the file ${file.absolutePath} with event: $event dirPath: $dirPath")

        when (event) {
            FileObserver.DELETE_SELF -> stopWatching(dirPath)
            FileObserver.CREATE -> startWatchingDirectory(file)
        }

        handler.onEvent(event, file)
    }

    fun initializeObserving() {
        if (watching) {
            return
        }

        stopObserving()

        val syncedFolders = DbHelper.getAllSyncedFolders(context)
        if (syncedFolders.isEmpty()) {
            return
        }

        val stack = Stack<String>()
        syncedFolders.map(SyncedFolder::getLocalFolder)
            .forEach(stack::push)

        watching = true

        // Recursively watch all child directories
        recursivelyWatchAllDirectories(stack)

        Timber.d("started observing all syncable directories")
    }

    private fun recursivelyWatchAllDirectories(stack: Stack<String>) {
        while (!stack.empty()) {
            val parent = stack.pop()
            startWatching(parent)

            val path = File(parent)
            val files = path.listFiles(WATCHABLE_DIRECTORIES_FILTER) ?: continue

            files.map(File::getAbsolutePath)
                .forEach(stack::push)
        }
    }

    private fun startWatchingDirectory(file: File) {
        if (WATCHABLE_DIRECTORIES_FILTER.accept(file)) {
            startWatching(file.absolutePath)
        }
    }

    /**
     * Start watching a single file
     */
    private fun startWatching(path: String) {
        synchronized(observers) {
            stopObservingFile(path)
            startObservingFile(path)
        }
    }

    private fun stopObservingFile(path: String) {
        observers.remove(path)?.let {
            it.stopWatching()
            Timber.d("stop observing file: $path")
        }
    }

    private fun startObservingFile(path: String) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.Q) {
            return
        }

        val observer = DirectoryObserver(path, this)
        observer.startWatching()
        observers[path] = observer
        Timber.d("start observing file: $path")
    }

    fun stopObserving() {
        observers.forEach {
            it.value.stopWatching()
        }
        observers.clear()
        watching = false

        Timber.d("stopped observing all syncable directories")
    }

    /**
     * Stop watching a single file
     */
    private fun stopWatching(path: String) {
        synchronized(observers) {
            stopObservingFile(path)
        }
    }
}
