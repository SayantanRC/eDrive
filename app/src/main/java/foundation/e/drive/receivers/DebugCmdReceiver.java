/*
 * Copyright © ECORP SAS 2021-2022.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */
package foundation.e.drive.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;

import androidx.annotation.NonNull;
import androidx.work.WorkManager;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import foundation.e.drive.EdriveApplication;
import foundation.e.drive.account.AccountUtils;
import foundation.e.drive.database.DbHelper;
import foundation.e.drive.fileObservers.FileObserverManager;
import foundation.e.drive.models.SyncRequest;
import foundation.e.drive.models.SyncedFileState;
import foundation.e.drive.models.SyncedFolder;
import foundation.e.drive.synchronization.SyncProxy;
import foundation.e.drive.synchronization.SyncRequestCollector;
import foundation.e.drive.utils.ReleaseTree;
import foundation.e.drive.work.WorkLauncher;
import timber.log.Timber;

/**
 * @author Jonathan Klee
 * @author Vincent Bourgmayer
 */
public class DebugCmdReceiver extends BroadcastReceiver {

    public static final String ACTION_TEST_SYNC ="foundation.e.drive.action.TEST_SYNC";
    public static final String ACTION_FORCE_SCAN = "foundation.e.drive.action.FORCE_SCAN";
    public static final String ACTION_DUMP_DATABASE = "foundation.e.drive.action.DUMP_DATABASE";
    public static final String ACTION_FULL_LOG_ON_PROD = "foundation.e.drive.action.FULL_LOG_ON_PROD";
    public static final String ACTION_ENABLE_FILE_OBSERVER = "foundation.e.drive.action.ENABLE_FILE_OBSERVER";

    private static final String FULL_LOG_ENABLE_KEY = "full_log_enable";
    private static final String FILE_OBSERVER_ENABLE_KEY = "file_observer_enable";

    @Override
    public void onReceive(@NonNull Context context, @NonNull Intent intent) {
        Timber.tag(DebugCmdReceiver.class.getSimpleName()).v("onReceive");
        final WorkManager workManager = WorkManager.getInstance(context);

        switch (intent.getAction()) {
            case ACTION_FORCE_SCAN:
                Timber.d("Force Sync intent received");
                WorkLauncher.getInstance(context).enqueueOneTimeFullScan(true);
                break;
            case ACTION_DUMP_DATABASE:
                Timber.d("Dump database intent received");
                DbHelper.dumpDatabase(context);
                break;
            case ACTION_FULL_LOG_ON_PROD:
                final boolean allow_full_log = intent.getBooleanExtra(FULL_LOG_ENABLE_KEY, false);
                ReleaseTree.allowDebugLogOnProd(allow_full_log);
                Timber.d("Allow full log on prod: %s", allow_full_log);
                break;
            case ACTION_ENABLE_FILE_OBSERVER:
                if (AccountUtils.getAccount(context) == null) {
                    Timber.d("Ignore intent: no account");
                }

                final EdriveApplication application = (EdriveApplication) context.getApplicationContext();
                final boolean enabled = intent.getBooleanExtra(FILE_OBSERVER_ENABLE_KEY, true);
                Timber.i("Intent received: enable FileObserver: %s", enabled);
                if (enabled) {
                    application.startRecursiveFileObserver();
                } else {
                    application.stopRecursiveFileObserver();
                }
                break;
            case ACTION_TEST_SYNC:
                Timber.d("Test SyncWorker.kt started");
                final SyncRequestCollector collector = (SyncRequestCollector) SyncProxy.INSTANCE;
                collector.onPeriodicScanStart((EdriveApplication) context.getApplicationContext());

                final File dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS);
                final List<SyncRequest> requests = generateDummyFileToSync(dir, context);

                if (requests.isEmpty()) return;

                collector.queueSyncRequests(requests, context.getApplicationContext());

                collector.startSynchronization(context.getApplicationContext());
                break;
            default:
                break;
        }
    }

    private List<SyncRequest> generateDummyFileToSync(File dir, Context context) {
        final List<SyncRequest> result = new ArrayList<>();
        final int fileAmount = 10;
        final SyncedFolder parentFolder = DbHelper.getSyncedFolderByLocalPath(dir.getAbsolutePath(), context);

        if (parentFolder == null || !parentFolder.isEnabled()) {
            Timber.d("Won't send sync request: no parent are known for n %s", dir.getAbsolutePath());
            return result;
        }

        for (int index = 0; index < fileAmount; index++) {
            final String fileName = "a" + System.currentTimeMillis()/1200 + ".txt";
            File file = new File(dir.getAbsolutePath(), fileName);
            try {
                file.createNewFile();
            } catch (IOException exception) {
                Timber.d("can't create file");
                continue;
            }

            final String remotePath = parentFolder.getRemoteFolder() + fileName;
            final SyncedFileState fileState = new SyncedFileState(-1, fileName, file.getAbsolutePath(), remotePath, "", 0L, parentFolder.getId(), true, 3);

            final SyncRequest request = new SyncRequest(fileState, SyncRequest.Type.UPLOAD);
            result.add(request);
        }
        return result;
    }
}