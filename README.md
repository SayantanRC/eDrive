# eDrive

eDrive is an application provided by default with /e/OS.
It synchronizes user's data files to /e/Cloud or a self-hosted cloud.

## Map of synced directories

| device                               | cloud                                  | category     |
| ------------------------------------ | -------------------------------------- | ------------ |
| /storage/emulated/0/DCIM             | /Photos                                | Images       |
| /storage/emulated/0/Pictures         | /Pictures                              | Images       |
| /storage/emulated/0/Movies           | /Movies                                | Movies       |
| /storage/emulated/0/Music            | /Music                                 | Music        |
| /storage/emulated/0/Ringtones        | /Ringtones                             | Ringtones    |
| /storage/emulated/0/Documents        | /Documents                             | Documents    |
| /storage/emulated/0/Podcasts         | /Podcasts                              | Podcasts     |
| /data/system/users/0/                | /Devices/\<id\>/rom_settings/          | ROM settings |
| /data/data/foundation.e.drive/files/ | /Devices/\<id\>/rom_settings/app_list/ | ROM settings |

All remote directories are created by eDrive if needed.

## Notes

### How it works

When you register an /e/account, the synchronization is enabled by default.
The synchronization is done on `ACTION_SCREEN_OFF` intent if the last
sync is greater than 15 min. Otherwise a sync is scheduled each 30 min.

### Settings

To enable/disable the synchronisation, go into your account settings:

* Application settings (ROM settings)
* Pictures and videos (Images, Movies, Music, Ringtones, Documents and Podcasts)

# Interoperability

eDrive can also receive some broadcast intent for different purpose: 

**Force the synchronization**

```bash
adb shell am broadcast -a foundation.e.drive.action.FORCE_SCAN --receiver-include-background
```

**Generate a database dump accessible by the user**

```bash
adb shell am broadcast -a foundation.e.drive.action.DUMP_DATABASE --receiver-include-background
```

or Download database directly:

```bash
adb pull /data/data/foundation.e.drive/databases
```

**Disable log limit on release build**

```bash
adb shell am broadcast -a foundation.e.drive.action.FULL_LOG_ON_PROD --receiver-include-background --ez full_log_enable true
```

**Limit log output on release build** *(after previous command)*

```bash
adb shell am broadcast -a foundation.e.drive.action.FULL_LOG_ON_PROD --receiver-include-background --ez full_log_enable false
```

**Test SyncWorker: upload 10 empty files**

Does not rely on FileObserver or FullScanWorker

```bash
adb shell am broadcast -a foundation.e.drive.action.TEST_SYNC --receiver-include-background
```

**Display logat**

```bash
adb logcat --pid=$(adb shell pidof -s foundation.e.drive)
```

You can also use the script `dev-tools.sh` to run those command. Use : `./dev-tools.sh -h` to display options


**Disable File Observer**


```bash
adb shell am broadcast -a foundation.e.drive.action.ENABLE_FILE_OBSERVER --receiver-include-background --ez file_observer_enable false
```

**Enable File Observer**


```bash
adb shell am broadcast -a foundation.e.drive.action.ENABLE_FILE_OBSERVER --receiver-include-background --ez file_observer_enable true
```

### local NC for testing

Use following documentation to set up a local NC instance

https://gitlab.e.foundation/internal/wiki/-/wikis/qa-team/how-to-run-local-nextcloud

`docker run -d -p 8080:80 -e NEXTCLOUD_TRUSTED_DOMAINS="<your computer IP>:8080" -e SQLITE_DATABASE=nc -e NEXTCLOUD_ADMIN_USER=admin -e NEXTCLOUD_ADMIN_PASSWORD=admin nextcloud:<NC version>
`

Then you can use Wireshark by example to collect network packet.
Example of filter for wireshark:
`(ip.src == <computer IP> && ip.dst == <Phone IP>) || (ip.dst == <computer IP> && ip.src == <Phone IP> ) && http`


*note: replace <computer IP>, <NC version> and <Phone IP> by values*
